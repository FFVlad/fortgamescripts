using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemUtils : MonoBehaviour
{
    private static Collider[] items = new Collider[100];
    private static LayerMask itemsLayer = 1 << ReferenceManager.ItemsLayer;
    public static int AmountOfItemsInRadius(Item item, float searchRadius, Collider[] results)
    {
        return Physics.OverlapSphereNonAlloc(item.transform.position, searchRadius, results, itemsLayer);
    }

    public static int AmountOfItemsInBoxCollider(BoxCollider collisionArea, Collider[] results)
    {
        LayerMask itemsLayer = 1 << ReferenceManager.ItemsLayer;

        Vector3 scalebound = collisionArea.transform.lossyScale;
        Vector3 sizebound = collisionArea.size;
        Vector3 extent = new Vector3(sizebound.x * scalebound.x / 2, sizebound.y * scalebound.y / 2, sizebound.z * scalebound.z / 2);

        return Physics.OverlapBoxNonAlloc(collisionArea.transform.position, extent, results, collisionArea.transform.rotation, itemsLayer);
    }

    public static int AmountOfUnattachedItemsInRadius(Item item, float searchRadius, ref List<Item> results)
    {
        LayerMask itemsLayer = 1 << ReferenceManager.DetachedItemsLayer;
        int numberOfItems = Physics.OverlapSphereNonAlloc(item.transform.position, searchRadius, items, itemsLayer);
        List<Item> itemList = new List<Item>();
        for (int i = 0; i < numberOfItems; i++)
        {
            Item foundItem = items[i].GetComponent<Item>();
            if (foundItem == null || foundItem.GetItemComponentOfType<PickupableComponent>() == null || (foundItem.Floating))
            {
                continue;
            }

            itemList.Add(foundItem);
        }

        results = itemList;
        return itemList.Count;
    }

    public static int AmountOfEnemiesInRadius(Item item, float searchRadius, Collider[] results)
    {
        LayerMask enemiesLayer = 1 << ReferenceManager.EnemiesLayer;
        return Physics.OverlapSphereNonAlloc(item.transform.position, searchRadius, results, enemiesLayer);
    }

    public static int AmountOfEnergyConsumersInRadius(Item item, float searchRadius, List<IAutomated> results)
    {
        Collider[] itemsInRadius = new Collider[100];
        int amountOfItemsInRadius = Physics.OverlapSphereNonAlloc(item.transform.position, searchRadius, itemsInRadius, itemsLayer);
        for (int i = 0; i < amountOfItemsInRadius; i++)
        {
            Item foundItem = itemsInRadius[i].GetComponent<Item>();
            if (foundItem != null && foundItem is IAutomated)
            {
                results.Add(foundItem as IAutomated);
            }
        }

        return results.Count;
    }




}
