﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementHelper : MonoBehaviour
{
   /*TODO: make items interact all the way down for stackable shenanigans*/
   public static void PlaceItem(Item item, SnappingStyle snappingStyle, Vector3 offset, IItemPlaceable itemToPlaceOn = null, bool attach = true)
   {
      Rigidbody rigidBody = item.GetComponent<Rigidbody>();
      if (snappingStyle != SnappingStyle.Drop)
      {
         rigidBody.isKinematic = true;
         if (attach)
         {
            itemToPlaceOn.AttachItem(item);
            item.attachedTo = itemToPlaceOn;
         }
      }
      else
      {
         rigidBody.isKinematic = false;
      }

      item.gameObject.layer = ReferenceManager.ItemsLayer;
        switch (snappingStyle)
        {

            case SnappingStyle.Center:

                item.transform.position = itemToPlaceOn.GetPlacingPosition() + offset;
                item.transform.rotation = itemToPlaceOn.GetTransform().rotation;
                item.transform.parent = itemToPlaceOn.GetTransform().parent;
                break;

            case SnappingStyle.NoRotation:

                item.transform.position = itemToPlaceOn.GetPlacingPosition() + offset;
                item.transform.parent = itemToPlaceOn.GetTransform().parent;

                break;

            case SnappingStyle.Drop:

                item.gameObject.layer = ReferenceManager.DetachedItemsLayer;
                if (itemToPlaceOn != null)
                {
                    item.transform.parent = itemToPlaceOn.GetTransform().parent;
                }
                break;

            case SnappingStyle.RotationOffset:

                item.transform.position = itemToPlaceOn.GetPlacingPosition() + offset;
                item.transform.parent = itemToPlaceOn.GetTransform().parent;

                Vector3 dataEulers = DataManager.Instance.GetItemWithID(item.itemId).rotationAxis1;
                item.transform.rotation = Quaternion.Euler(new Vector3(dataEulers.x, dataEulers.y, dataEulers.z));

                break;
        }

      if (item is IShipItem)
      {
         /*TODO: possibly differentiate between ships*/
         VesselBase myShip = ReferenceManager.PlayerVessel;
         (item as IShipItem).AddToShip(myShip);
      }
   
   }}
