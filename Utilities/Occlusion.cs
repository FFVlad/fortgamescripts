﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Occlusion : MonoBehaviour
{

    public Transform target;

	public Material invisibleMaterial;
	// Update is called once per frame
	void Update ()
	{
	    float rayWidth = 2;
	    float distToCam = Vector3.Distance(this.target.position, transform.position);
	    Vector3 rayDir = this.transform.position - this.target.position;
        LayerMask mask = 1 << 0;//ReferenceManager.GroundLayer;
		Vector3 startPos = this.target.position + rayDir.normalized * (distToCam * 0.25f);
		
		RaycastHit[] rcarr = Physics.SphereCastAll(startPos, rayWidth, rayDir, distToCam,mask);
		
		Debug.DrawLine(startPos, startPos +  (rayDir*distToCam), Color.blue);
	
		if (rcarr.Length > 0)
	    {
		    foreach (RaycastHit rhit in rcarr)
		    {
			    if (!rhit.transform.gameObject.GetComponent<AutoTransparent>() && rhit.transform.GetComponent<Renderer>() != null && rhit.point.y > target.position.y + 10)
			    {
				    if (rhit.collider.bounds.size.y > Mathf.Abs(10) || transform.position.y < target.position.y)
				    {
					    rhit.transform.gameObject.AddComponent<AutoTransparent>();
					    rhit.transform.gameObject.GetComponent<AutoTransparent>().BeTransparent(this.invisibleMaterial);
				    }
			    }
			    else if (rhit.transform.gameObject.GetComponent<AutoTransparent>())
			    {
				    rhit.transform.gameObject.GetComponent<AutoTransparent>().resetTimer();
			    }
		    }
	    }
	}
	
}
