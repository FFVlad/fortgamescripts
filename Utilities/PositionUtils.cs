﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionUtils : MonoBehaviour {

	public static Vector3 RandomPositionInArea(Vector3 area)
	{
		float randomX = Random.Range(0, area.x);
		float randomY = Random.Range(0, area.y);
		float randomZ = Random.Range(0, area.z);
        
		return new Vector3(randomX,randomY,randomZ);
	}

    public static Vector3 RandomPositionInArea(float maxRadius, float minRadius = 0)
    {
        float randomX = Random.Range(0f, 1f) > 0.5f ? Random.Range(-maxRadius, -minRadius) : Random.Range(minRadius, maxRadius);
        float randomY = Random.Range(0f, 1f) > 0.5f ? Random.Range(-maxRadius, -minRadius) : Random.Range(minRadius, maxRadius);
        float randomZ = Random.Range(0f, 1f) > 0.5f ? Random.Range(-maxRadius, -minRadius) : Random.Range(minRadius, maxRadius);
        
        return new Vector3(randomX,randomY,randomZ);
    }
    
    public static Vector3 RandomPositionInCircle(float maxRadius, float minRadius = 0)
    {
        Vector3 randomPosition = RandomPositionInArea(maxRadius, minRadius);
        randomPosition.y = 0;

        return randomPosition;
    }

    public static Vector3 GetClosestPositionFromVectors(List<Vector3> compare, Vector3 pointToCompare)
    {
        int closestIndex = 0;
        float distance = 1000000;
        int currentIndex = 0;

        foreach (Vector3 vector in compare)
        {
            float compareDistance = Vector3.Distance(pointToCompare, vector);

            if (compareDistance < distance)
            {
                closestIndex = currentIndex;
                distance = compareDistance;
            }

            currentIndex++;
        }

        return compare[closestIndex];
    }

    /// <summary>
    /// IS A BIT SCARY, USE AT OWN RISK
    /// </summary>
    private static Collider[] colliders = new Collider[200];
    public static List<MonoBehaviour> GetEnemiesInRadius(Vector3 position, float radius, bool getDead = false)
    {
        LayerMask layerMask = 1 << 12; //Sort out all colliders not tagged enemy
        Physics.OverlapSphereNonAlloc(position, radius, colliders, layerMask);
        List<MonoBehaviour> enemies = new List<MonoBehaviour>();
        
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i] == null)
            {
                break; //First empty index in array, no need to loop empty ones
            }
            if (colliders[i] && colliders[i].CompareTag("Enemy"))
            {
                MonoBehaviour hitAI = colliders[i].GetComponent<MonoBehaviour>();

                if (hitAI)
                {
                    if (!enemies.Contains(hitAI))
                    {
                        enemies.Add(hitAI);
                    }
                }
            }
        }

        return enemies;
    }
    
    public static bool ClampRotation(ref Quaternion rotation, Vector3 axis, float limit, Quaternion originalRot = default(Quaternion))
    {
        Vector3 clampedRotation = rotation.eulerAngles;
        float minLimit = -limit;
        float maxLimit = limit;
        bool clamped = false;
        if (axis.x != 0)
        {
            maxLimit += originalRot.eulerAngles.x;
            minLimit += originalRot.eulerAngles.x;
            clamped = ClampAngle(ref clampedRotation.x, minLimit, maxLimit);
        }
        else if (axis.y != 0)
        {
            maxLimit += originalRot.eulerAngles.y;
            minLimit += originalRot.eulerAngles.y;
            clamped = ClampAngle(ref clampedRotation.y, minLimit, maxLimit);
        }
        else if (axis.z != 0)
        {
            maxLimit += originalRot.eulerAngles.z;
            minLimit += originalRot.eulerAngles.z;
            clamped = ClampAngle(ref clampedRotation.z, minLimit, maxLimit);
        }

        rotation = Quaternion.Euler(clampedRotation);
        return clamped;
    }

    private static bool ClampAngle(ref float angle, float min, float max)
    {
        if (angle > 180 && min < 180)
        {
            angle -= 360f;
        }

        /*if (min > 180)
        {
            min -= 360f;
            max -= 360f;
        }*/
        return Clamp(ref angle, min, max);
    }
    
    public static bool Clamp(ref float value, float min, float max)
    {
        if ((double) value < (double) min)
        {
            value = min;
            return true;
        }
        else if ((double) value > (double) max)
        {
            value = max;
            return true;
        }

        return false;
    }


    public static float CalculateBallisticForce(Vector3 direction, float angle)
    {
        float h = direction.y;
        direction.y = 0;

        float distance = direction.magnitude;

        float a = angle * Mathf.Deg2Rad;

        direction.y = distance * Mathf.Tan(a);

        distance += h / Mathf.Tan(a);

        float velocity = Mathf.Sqrt(Mathf.Abs(distance * Physics.gravity.magnitude / Mathf.Sin(2 * a)));
        return velocity;
    }

    public static Vector3 CalculateBallisticTrajectory(Vector3 target, Vector3 startingPoint, float initialAngle)
    {
        float angle = initialAngle * Mathf.Deg2Rad;
 
        Vector3 planarTarget = new Vector3(target.x, 0, target.z);
        Vector3 planarPostion = new Vector3(startingPoint.x, 0, startingPoint.z);
 
        float planarDistance = Vector3.Distance(planarTarget, planarPostion);
        
        float yOffset = startingPoint.y - target.y;
 
        float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * Mathf.Pow(planarDistance, 2)) / (planarDistance * Mathf.Tan(angle) + yOffset));
 
        Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));
 
        float angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPostion);
        Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;
        return finalVelocity;
    }
}
