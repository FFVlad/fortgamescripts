﻿using UnityEngine;
using System.Collections;

public class AutoTransparent : MonoBehaviour
{
    private Material _originalMaterial;
    private Material _invisibleMaterial;
    private float _mTransparency = 0f;
    private float _mT = 0f;

    bool _shouldBeTransparent = true;
    bool justKillRenderer = false;
    private Renderer _myRenderer;
    
    public void BeTransparent(Material invisibleMaterial)
    {
        if (this._myRenderer == null)
        {
            this._myRenderer = GetComponent<Renderer>();
        }

        this._shouldBeTransparent = true;
        if (justKillRenderer && this._myRenderer != null)
        {
            this._myRenderer.enabled = false;
        }

        this._originalMaterial = this._myRenderer.material;
        this._myRenderer.material= this._invisibleMaterial = invisibleMaterial;
    }
    public void resetTimer()
    {
        this._shouldBeTransparent = true;
        this._mT = 0f;
    }

    void OnDestroy()
    {
        if (justKillRenderer)
        {
            this._myRenderer.enabled = true;
        }
    }

    void Update()
    {
        if (!this._shouldBeTransparent && this._mT >= 1.0f)
        {
            Destroy(this);
        }

        if (!this._shouldBeTransparent)
        {
            this._myRenderer.material = this._originalMaterial;
        }
        else
        {
            this._myRenderer.material = this._invisibleMaterial;
        }
       
        this._shouldBeTransparent = false;
    }
}