﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "LIB/Inventory/PlayerInventory", order = 1)]
public class InventoryData : ScriptableObject
{
    public int inventoryID;
    public List<InventoryResource> inventoryResources = new List<InventoryResource>();
    public List<int> blocksInInventory = new List<int>();
    public List<int> itemBlueprintsInInventory = new List<int>();
    public List<InventoryItemData> itemsInInventory = new List<InventoryItemData>();
}

[System.Serializable]
public class InventoryItemData
{
    public int itemID;
    public bool hasInfinite = false;
    public int amount;

    internal InventoryItemData shallowCopy()
    {
        return (InventoryItemData)MemberwiseClone();
    }

}

[System.Serializable]
public class Blueprint
{
    public int itemID;
}

[System.Serializable]
public class InventoryResource
{
    public RawResource rawResource;
    public float amount;
}
