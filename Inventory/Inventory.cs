﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory
{
    internal Action inventoryUpdated;

    internal StructureBlock equippedStructureBlock;
    internal ItemData equippedItem;

    internal Dictionary<RawResource, float> inventoryResources = new Dictionary<RawResource, float>();
    internal List<int> blocksInInventory = new List<int>();
    internal List<int> itemBlueprintsInInventory = new List<int>();
    internal List<InventoryItemData> itemsInInventory = new List<InventoryItemData>();

    private int equippedBlockIndex = 0;
    private int equippedItemIndex = 0;

    internal void InitializeInventory(InventoryData data)
    {
        this.blocksInInventory = new List<int>(data.blocksInInventory);
        this.itemBlueprintsInInventory = new List<int>(data.itemBlueprintsInInventory);
        this.itemsInInventory = new List<InventoryItemData>();

        foreach (InventoryItemData itemData in data.itemsInInventory)
        {
            this.itemsInInventory.Add(itemData.shallowCopy());
        }

        this.inventoryResources = new Dictionary<RawResource, float>();

        foreach (InventoryResource resource in data.inventoryResources)
        {
            ChangeRawResource(resource.rawResource, resource.amount);
        }

        this.equippedBlockIndex = 0;
        this.equippedItemIndex = 0;

        SetCurrentlyEquippedBlock();
        SetCurrentlyEquippedItem();
    }

    #region Raw resources functions

    internal float GetRawResourceAmount(RawResource resource)
    {
        if (this.inventoryResources.ContainsKey(resource))
        {
            return this.inventoryResources[resource];
        }

        return 0;
    }

    internal void ChangeRawResource(RawResource resource, float amount)
    {
        if (amount < 0 && DebugManager.Instance.infiniteResources)
        {
            return;
        }

        if (!this.inventoryResources.ContainsKey(resource))
        {
            this.inventoryResources.Add(resource, 0);
        }
        this.inventoryResources[resource] += amount;
    }

    #endregion

    #region Block related functions

    internal void EquipNextBlock()
    {
        this.equippedBlockIndex++;

        if (this.equippedBlockIndex >= this.blocksInInventory.Count)
        {
            this.equippedBlockIndex = 0;
        }

        SetCurrentlyEquippedBlock();
    }

    internal void EquipPreviousBlock()
    {
        this.equippedBlockIndex--;

        if (this.equippedBlockIndex < 0)
        {
            this.equippedBlockIndex = this.blocksInInventory.Count - 1;
        }

        SetCurrentlyEquippedBlock();
    }

    internal bool CanBuildCurrentBlock()
    {
        if (this.equippedStructureBlock == null)
        {
            return false;
        }

        if (DebugManager.Instance.infiniteResources)
        {
            return true;
        }

        return this.equippedStructureBlock.HasEnoughResourcesToBuild(this);
    }

    internal void BuiltCurrentBlock()
    {
        foreach (InventoryResource resource in this.equippedStructureBlock.resourcesRequired)
        {
            ChangeRawResource(resource.rawResource, -resource.amount);
        }

        if (inventoryUpdated != null)
        {
            inventoryUpdated.Invoke();
        }
    }

    internal void DeletedBlock(int blockID)
    {
        StructureBlock block = DataManager.Instance.GetBlockWithID(blockID);

        if (block != null)
        {
            foreach (InventoryResource resource in block.resourcesRequired)
            {
                ChangeRawResource(resource.rawResource, resource.amount);
            }
        }

        if (inventoryUpdated != null)
        {
            inventoryUpdated.Invoke();
        }
    }

    private void SetCurrentlyEquippedBlock()
    {
        if (this.blocksInInventory.Count > this.equippedBlockIndex)
        {
            this.equippedStructureBlock = DataManager.Instance.GetBlockWithID(this.blocksInInventory[this.equippedBlockIndex]);

            if (inventoryUpdated != null)
            {
                inventoryUpdated.Invoke();
            }
        }
    }

    #endregion

    #region Item related functions

    internal string GetCurrentItemUIText()
    {
        if (this.equippedItem == null)
        {
            return "";
        }

        string uiText = this.equippedItem.VisibleName;
        InventoryItemData data = this.itemsInInventory[this.equippedItemIndex];

        if (!data.hasInfinite)
        {
            uiText += " x" + data.amount;
        }

        return uiText;
    }

    internal void PlacedCurrentItem()
    {
        ChangeCurrentItemAmount(-1);
    }

    internal void PlacedItem(int itemID)
    {
        ChangeItemAmount(itemID, -1);
    }

    internal bool CanCraftItem(int itemID)
    {
        ItemData itemData = DataManager.Instance.GetItemWithID(itemID);

        if (itemData && itemData.HasEnoughResourcesToCraft(this))
        {
            return true;
        }

        return false;
    }

    internal void TryToCraftItem(int itemID)
    {
        ItemData itemData = DataManager.Instance.GetItemWithID(itemID);

        if (itemData && itemData.HasEnoughResourcesToCraft(this))
        {
            foreach (InventoryResource resource in itemData.resourcesRequired)
            {
                ChangeRawResource(resource.rawResource, -resource.amount);
            }

            ChangeItemAmount(itemID, 1);
        }
    }

    internal void ChangeCurrentItemAmount(int changeBy)
    {
        ChangeItemAmount(this.itemsInInventory[this.equippedItemIndex], changeBy);
    }

    internal InventoryItemData GetItemDataForItem(int itemID)
    {
        foreach (InventoryItemData item in this.itemsInInventory)
        {
            if (item.itemID == itemID)
            {                
                return item;
            }
        }

        return null;
    }

    internal void ChangeItemAmount(int itemID, int changeAmount)
    {
        if (!DataManager.Instance.CheckIfItemExists(itemID))
        {
            return;
        }

        foreach (InventoryItemData item in this.itemsInInventory)
        {
            if (item.itemID == itemID)
            {
                ChangeItemAmount(item, changeAmount);
                return;
            }
        }

        InventoryItemData newItem = new InventoryItemData()
        {
            itemID = itemID,
            amount = changeAmount
        };

        this.itemsInInventory.Add(newItem);

        if (this.equippedItem == null)
        {
            EquipNextItem();
        }

        if (inventoryUpdated != null)
        {
            inventoryUpdated.Invoke();
        }
    }

    internal void ChangeItemAmount(InventoryItemData data, int changeAmount)
    {
        if (data.hasInfinite)
        {
            return;
        }

        data.amount += changeAmount;

        if (data.amount <= 0)
        {
            this.itemsInInventory.Remove(data);

            if (this.equippedItem.itemID == data.itemID)
            {
                EquipNextItem();
            }
        }

        if (inventoryUpdated != null)
        {
            inventoryUpdated.Invoke();
        }
    }

    internal void EquipItemWithID(int id)
    {
        for (int i = 0; i < itemsInInventory.Count; i++)
        {
            if (itemsInInventory[i].itemID == id)
            {
                equippedItemIndex = i;
                SetCurrentlyEquippedItem();
                break;
            }
        }
    }

    internal void EquipNextItem()
    {
        this.equippedItemIndex++;

        if (this.equippedItemIndex >= this.itemsInInventory.Count)
        {
            this.equippedItemIndex = 0;
        }

        SetCurrentlyEquippedItem();
    }

    internal void EquipPreviousItem()
    {
        this.equippedItemIndex--;

        if (this.equippedItemIndex < 0)
        {
            this.equippedItemIndex = this.itemsInInventory.Count - 1;
        }

        SetCurrentlyEquippedItem();
    }

    private void SetCurrentlyEquippedItem()
    {
        if (this.itemsInInventory.Count > this.equippedItemIndex)
        {
            this.equippedItem = DataManager.Instance.GetItemWithID(this.itemsInInventory[this.equippedItemIndex].itemID);

            if (inventoryUpdated != null)
            {
                inventoryUpdated.Invoke();
            }
        }
    }

    #endregion

}

public enum RawResource
{
    Wood,
    Metal,
    Plastic,
    Components
}