﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayUI : MonoBehaviour
{
    public static PlayUI Instance;

    public GameObject itemDataParent;
    public GameObject snappingParent;

    public TextMeshProUGUI buildingModeText;
    public Text currentlySelectedBlockText;
    public TextMeshProUGUI snappingModeText;
    public InventoryView inventoryView;
    public CraftingView craftingView;

    public List<BuildingModeUI> buildingModeUIs;

    private void Awake()
    {
        InitializeUI();
    }

    protected virtual void InitializeUI()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    internal void SetSnappingParent(bool setTo)
    {
        if (this.snappingParent)
        {
            this.snappingParent.SetActive(setTo);
        }
    }

    internal void SetItemDataParent(bool setTo)
    {
        if (this.itemDataParent)
        {
            this.itemDataParent.SetActive(setTo);
        }
    }

    internal void ChangeSelectedThingText(string textToInput)
    {
        if (this.currentlySelectedBlockText)
        {
            this.currentlySelectedBlockText.text = textToInput;
        }
    }

    internal void ChangeSelectedBuildModeText(string buildModeName)
    {
        if (this.buildingModeText)
        {
            this.buildingModeText.text = buildModeName;
        }
    }

    internal void ChangeSnappingMode(string text)
    {
        if (this.snappingModeText)
        {
            this.snappingModeText.text = text;
        }
    }

    internal void SetBuildingMode(BuildMode buildMode)
    {
        switch (buildMode)
        {
            case BuildMode.StructureMode:
                ChangeSelectedBuildModeText("Structure Mode");
                break;
            case BuildMode.ItemMode:
                ChangeSelectedBuildModeText("Item Mode");
                break;
            case BuildMode.DeleteMode:
                ChangeSelectedBuildModeText("Delete Mode");
                break;
            case BuildMode.InspectionMode:
                ChangeSelectedBuildModeText("Inspection Mode");
                break;
        }

        foreach (BuildingModeUI modeUI in this.buildingModeUIs)
        {
            modeUI.SetSelectedStatus(modeUI.buildMode == buildMode);
        }
    }

    internal void SetPlayerInventoryUI(Inventory inventory, bool setTo)
    {
        if (this.inventoryView)
        {
            if (setTo)
            {
                this.inventoryView.gameObject.SetActive(true);
                this.inventoryView.InitializeWithItems(inventory);                
            }
            else
            {
                this.inventoryView.gameObject.SetActive(false);
            }
        }
    }


    internal void TogglePlayerInventory(Inventory inventory)
    {
        if (this.inventoryView)
        {
            SetPlayerInventoryUI(inventory, !this.inventoryView.gameObject.activeInHierarchy);
        }
    }

    internal void ToggleCraftingWindow(Inventory inventory)
    {
        if (this.craftingView)
        {
            if (this.craftingView.gameObject.activeInHierarchy)
            {
                this.craftingView.gameObject.SetActive(false);
            }
            else
            {
                this.craftingView.gameObject.SetActive(true);
                this.craftingView.InitializeWithItems(inventory);
            }
        }
    }

}
