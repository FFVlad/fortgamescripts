﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SailingUI : PlayUI
{
    public static SailingUI SailingInstance;

    protected override void InitializeUI()
    {
        base.InitializeUI();

        if (SailingInstance == null)
        {
            SailingInstance = this;
        }
    }

}
