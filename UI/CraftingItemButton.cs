﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CraftingItemButton : InventoryItemButton
{

    public override void SelectThisItem()
    {
        if (this.parentView.playerInventoryBeingShown.CanCraftItem(this.itemBeingDisplayed.itemID))
        {
            this.parentView.playerInventoryBeingShown.TryToCraftItem(this.itemBeingDisplayed.itemID);
            this.parentView.UpdateInventoryView();
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        this.parentView.SetSelectedButton(this.itemInfo.itemID);
    }   

}
