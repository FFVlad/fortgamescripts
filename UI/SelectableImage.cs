﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectableImage : MonoBehaviour
{
    public Image selectedImage;

    public Color normalColor;
    public Color selectedColor;

    public virtual void SetThisToActive()
    {
        
    }

    internal void SetSelectedStatus(bool setTo)
    {
        if (selectedImage)
        {
            if (setTo)
            {
                selectedImage.color = selectedColor;
            }
            else
            {
                selectedImage.color = normalColor;
            }
        }
    }
}
