﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingModeUI : SelectableImage
{
    public BuildMode buildMode;

    public override void SetThisToActive()
    {
        Builder.Instance.ChangeBuildMode(buildMode);
    }

}
