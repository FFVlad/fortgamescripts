﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class InventoryItemButton : MonoBehaviour, IPointerEnterHandler
{
    public Image backgroundImage;
    public Image itemImage;
    public TextMeshProUGUI itemNameText;
    public TextMeshProUGUI itemAmountText;

    internal InventoryItemData itemBeingDisplayed;
    internal ItemData itemInfo;

    protected InventoryView parentView;

    internal virtual void InitializeButton(InventoryView viewParentedTo, InventoryItemData data)
    {
        this.parentView = viewParentedTo;
        this.itemBeingDisplayed = data;
        this.itemInfo = DataManager.Instance.GetItemWithID(data.itemID);
        UpdateButtonData();
    }

    public virtual void SelectThisItem()
    {
        this.parentView.playerInventoryBeingShown.EquipItemWithID(this.itemBeingDisplayed.itemID);
        this.parentView.SetSelectedButton();
    }

    internal void UpdateButtonData()
    {
        if (this.itemInfo == null || this.itemBeingDisplayed == null)
        {
            Debug.Log("call to update item button with null data!");
            return;
        }

        if (this.itemImage)
        {
            this.itemImage.sprite = this.itemInfo.itemSprite;
        }

        if (this.itemNameText)
        {
            this.itemNameText.text = this.itemInfo.VisibleName;
        }

        if (this.itemAmountText)
        {
            if (this.itemBeingDisplayed.hasInfinite)
            {
                this.itemAmountText.text = "";
            }
            else
            {
                this.itemAmountText.text = this.itemBeingDisplayed.amount + "x";
            }
        }
    }

    internal virtual void ChangeSelectionToThis(Color setTo)
    {
        if (this.backgroundImage)
        {
            this.backgroundImage.color = setTo;
        }
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {

    }

}
