﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportItemButton : InventoryItemButton
{

    public override void SelectThisItem()
    {
        IInteractable interactable = ReferenceManager.playerController?.CurrentInteraction;

        if (interactable != null && interactable.GetType() == typeof(InventoryMachine))
        {
            InventoryMachine machine = interactable as InventoryMachine;
            machine.CreateItem(this.parentView.playerInventoryBeingShown, itemInfo);
            this.parentView.UpdateInventoryView();
        }
    }

}
