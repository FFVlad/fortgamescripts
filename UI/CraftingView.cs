﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingView : InventoryView
{
    public List<RawResourceView> resourceViews = new List<RawResourceView>();

    private int lastButtonID = -1;

    internal override void InitializeWithItems(Inventory inventory)
    {
        base.InitializeWithItems(inventory);
        UpdateResourcesChangeAmounts(new List<InventoryResource>());
    }

    internal override void UpdateInventoryView()
    {
        ClearItemButtons();

        foreach (int itemID in this.playerInventoryBeingShown.itemBlueprintsInInventory)
        {
            GameObject newButton = Instantiate(this.itemButtonPrefab, this.buttonParent.transform);
            InventoryItemButton itemScript = newButton.GetComponent<InventoryItemButton>();            

            if (itemScript)
            {
                InventoryItemData data = playerInventoryBeingShown.GetItemDataForItem(itemID);

                if (data == null)
                {
                    data = new InventoryItemData() { itemID = itemID };
                }

                itemScript.InitializeButton(this, data);
                this.buttonList.Add(itemScript);
            }
            else
            {
                Debug.LogError("Something weird happened in Crafting view when creating buttons!");
                Destroy(newButton);
            }
        }

        UpdateResources(this.playerInventoryBeingShown);

        if (BuildingUI.BuildingInstance)
        {
            BuildingUI.BuildingInstance.UpdateResources(playerInventoryBeingShown);
        }
    }

    internal override void SetSelectedButton()
    {
        SetSelectedButton(this.lastButtonID);
    }

    internal override void SetSelectedButton(int itemID)
    {
        base.SetSelectedButton(itemID);

        ItemData data = DataManager.Instance.GetItemWithID(itemID);

        if (data)
        {
            UpdateResourcesChangeAmounts(data.resourcesRequired);
        }
        else
        {
            UpdateResourcesChangeAmounts(new List<InventoryResource>());
        }

        this.lastButtonID = itemID;
    }

    internal void UpdateResources(Inventory inventory)
    {
        foreach (RawResourceView view in this.resourceViews)
        {
            view.UpdateResourceView(inventory);
        }
    }

    internal void UpdateResourcesChangeAmounts(List<InventoryResource> resourcesNeeded, bool negativeValues = true)
    {
        foreach (RawResourceView view in this.resourceViews)
        {
            view.ShowRequiredResourceChange(0);
        }

        foreach (InventoryResource resource in resourcesNeeded)
        {
            foreach (RawResourceView view in this.resourceViews)
            {
                if (view.rawResource == resource.rawResource)
                {
                    int amount = (int)resource.amount;

                    if (negativeValues)
                    {
                        amount = -amount;
                    }

                    view.ShowRequiredResourceChange(amount);
                    break;
                }
            }
        }
    }

}
