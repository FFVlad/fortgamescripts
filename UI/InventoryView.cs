﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryView : MonoBehaviour
{
    public GameObject itemButtonPrefab;
    public GameObject buttonParent;

    public Color ItemNormalColor;
    public Color ItemSelectedColor;

    internal Inventory playerInventoryBeingShown;

    protected List<InventoryItemButton> buttonList = new List<InventoryItemButton>();

    private void OnDisable()
    {
        if (this.playerInventoryBeingShown != null)
        {
            this.playerInventoryBeingShown.inventoryUpdated -= UpdateInventoryView;
        }
    }

    internal virtual void InitializeWithItems(Inventory inventory)
    {
        this.playerInventoryBeingShown = inventory;
        this.playerInventoryBeingShown.inventoryUpdated += UpdateInventoryView;
        UpdateInventoryView();
    }

    internal virtual void UpdateInventoryView()
    {
        ClearItemButtons();

        foreach (InventoryItemData data in this.playerInventoryBeingShown.itemsInInventory)
        {
            GameObject newButton = Instantiate(this.itemButtonPrefab, this.buttonParent.transform);
            InventoryItemButton itemScript = newButton.GetComponent<InventoryItemButton>();

            if (itemScript)
            {
                itemScript.InitializeButton(this, data);
                this.buttonList.Add(itemScript);
            }
        }

        SetSelectedButton();
    }

    protected void ClearItemButtons()
    {
        foreach (InventoryItemButton button in this.buttonList)
        {
            Destroy(button.gameObject);
        }

        this.buttonList.Clear();
    }

    internal virtual void SetSelectedButton()
    {
        ItemData dataToSearchFor = this.playerInventoryBeingShown.equippedItem;

        foreach (InventoryItemButton button in this.buttonList)
        {
            if (button.itemInfo.itemID == dataToSearchFor.itemID)
            {
                button.ChangeSelectionToThis(this.ItemSelectedColor);
            }
            else
            {
                button.ChangeSelectionToThis(this.ItemNormalColor);
            }
        }
    }

    internal virtual void SetSelectedButton(int itemID)
    {
        ItemData dataToSearchFor = DataManager.Instance.GetItemWithID(itemID);

        foreach (InventoryItemButton button in this.buttonList)
        {
            if (button.itemInfo.itemID == dataToSearchFor.itemID)
            {
                button.ChangeSelectionToThis(this.ItemSelectedColor);
            }
            else
            {
                button.ChangeSelectionToThis(this.ItemNormalColor);
            }
        }
    }

}
