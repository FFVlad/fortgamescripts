﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraModeUI : SelectableImage
{
    public CameraMode cameraMode;

    public override void SetThisToActive()
    {
        BuildingManager.BuilderSceneInstance.ChangeCameraMode(cameraMode);
    }

}
