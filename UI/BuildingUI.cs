﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildingUI : PlayUI
{
    public static BuildingUI BuildingInstance;

    public List<RawResourceView> resourceViews = new List<RawResourceView>();
    public List<CameraModeUI> cameraModeUIs;

    private void Start()
    {

    }

    protected override void InitializeUI()
    {
        base.InitializeUI();

        if (BuildingInstance == null)
        {
            
            BuildingInstance = this;
        }
    }

    private void OnDisable()
    {
        
    }

    internal void UpdateResources(Inventory inventory)
    {
        foreach (RawResourceView view in this.resourceViews)
        {
            view.UpdateResourceView(inventory);
        }
    }

    internal void UpdateResourcesChangeAmounts(List<InventoryResource> resourcesNeeded, bool negativeValues = true)
    {
        foreach (RawResourceView view in this.resourceViews)
        {
            view.ShowRequiredResourceChange(0);
        }

        foreach (InventoryResource resource in resourcesNeeded)
        {
            foreach (RawResourceView view in this.resourceViews)
            {
                if (view.rawResource == resource.rawResource)
                {
                    int amount = (int)resource.amount;

                    if (negativeValues)
                    {
                        amount = -amount;
                    }

                    view.ShowRequiredResourceChange(amount);
                    break;
                }
            }
        }
    }

    internal void SetCameraMode(CameraMode cameraMode)
    {
        foreach (CameraModeUI modeUI in this.cameraModeUIs)
        {
            modeUI.SetSelectedStatus(modeUI.cameraMode == cameraMode);
        }
    }

    public void GoToSeaWithThisShip()
    {
        GameManager.Instance.ChangePlayMode(PlayMode.PlayMode);
    }

    public void GoToSeaWithDefaultShip()
    {
        GameManager.Instance.ChangeToDefaultBoat();
        GameManager.Instance.ChangePlayMode(PlayMode.PlayMode);
    }

}
