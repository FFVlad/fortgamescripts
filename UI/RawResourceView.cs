﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RawResourceView : MonoBehaviour
{
    public RawResource rawResource;
    public TextMeshProUGUI resourceAmountText;
    public TextMeshProUGUI resourceChangeAmountText;
    public Image resourceImage;

    internal void UpdateResourceView(Inventory inventory)
    {
        if (this.resourceAmountText)
        {
            this.resourceAmountText.text = Mathf.RoundToInt(inventory.GetRawResourceAmount(this.rawResource)).ToString();
        }
    }

    internal void ShowRequiredResourceChange(int amountChange)
    {
        if (this.resourceChangeAmountText)
        {
            if (amountChange == 0)
            {
                this.resourceChangeAmountText.text = "";
            }
            else
            {
                this.resourceChangeAmountText.text = amountChange.ToString();
            }
        }
    }

}
