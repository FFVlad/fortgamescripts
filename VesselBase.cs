﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VesselBase : MonoBehaviour
{
    //For technical reasons I suggest we never go over 10.
    public float maxVesselVelocity = 8f;
    private GameObject player;
    internal GameObject Player
    {
        get
        {
            if (player == null)
            {
                player = FindObjectOfType<PlayerController>().gameObject;
            }
            return player;

        }
    }

    internal bool isPlayerInShip = true;

    internal float shipSpeed;
    internal float maxVelocity;
    private float _minMaxVelocity = 1f;
    internal float shipSteerAngle;
    private bool _isGoingMaxSpeed;
    private List<Engine> engines = new List<Engine>();
    private List<Anchor> anchors = new List<Anchor>();
    private List<SteeringItem> steeringItems = new List<SteeringItem>();
    internal Rigidbody rb;
    public Vector3 MovementVector
    {
        get { return transform.forward; }
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        return;
        engines = new List<Engine>();
        steeringItems = new List<SteeringItem>();

        List<GameObject> children = LogicUtils.GetAllChildrenOf(transform);
        foreach (GameObject gameObj in children)
        {
            if (gameObj.activeSelf == false)
            {
                continue;
            }

            Engine engine = gameObj.GetComponent<Item>() as Engine;
            if (engine != null)
            {
                engines.Add(engine);
            }
            else
            {
                SteeringItem steerItem = gameObj.GetComponent<Item>() as SteeringItem;
                if (steerItem != null)
                {
                    steeringItems.Add(steerItem);
                }
            }
        }
    }

    public bool AddALotOfForce;
    public float HowMuchIsALot = 50000f;
    private void Update()
    {

        HandleShipInventory();
    }

    private void FixedUpdate()
    {
        //Clamp for speed limit 
        Vector3 _actualForce = rb.velocity;
        
        if(rb.velocity.z > maxVelocity + _minMaxVelocity)
        {
            _isGoingMaxSpeed = true;
            _actualForce.z = maxVelocity + _minMaxVelocity;
        }
        else
        {
            _isGoingMaxSpeed = false;
        }

        Vector3 originalVelocity = rb.velocity;
        //Clamp for technical limit
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVesselVelocity);
        if (originalVelocity != rb.velocity)
        {
            //This can be later used to tell player that their ship can't go any fast
            Debug.LogWarning("Ship is going too fast! If game seems OK (not buggy) go to VesselBase and increase maxVesselVelocity! (Distance from limited to original velocity: " + Vector3.Distance(originalVelocity, rb.velocity) + ")");
        }

        if (AddALotOfForce)
        {
            rb.AddForce(transform.forward * HowMuchIsALot * Time.fixedDeltaTime, ForceMode.Impulse);
        }

        rb.AddForce(transform.forward * shipSpeed * 1000f * Time.fixedDeltaTime, ForceMode.Impulse);
        SteerShip();
    }

    #region Ship handling

    [ContextMenu("HandleShipInventory")]
    private void HandleShipInventory()
    {
        CalculateSpeed();
        CalculateSteer();
    }

    private void CalculateSpeed()
    {
        shipSpeed = 0f;
        foreach (Engine engine in engines)
        {
            if (engine)
            {
                shipSpeed += engine.GetCurrentSpeed();
            }
        }

        foreach(Anchor anchor in anchors)
        {
            if (anchor)
            {
                shipSpeed -= anchor.GetCurrentSpeed();
            }
        }
        
        shipSpeed = Mathf.Clamp(shipSpeed, 0, float.MaxValue);
        /*TODO: Calculate maxvelocity: lastframepos-thisframepos/time.deltatime roughly*/
        maxVelocity = shipSpeed * 0.1f;
}

    private void CalculateSteer()
    {
        shipSteerAngle = 0f;
        foreach (SteeringItem steerItem in steeringItems)
        {
            shipSteerAngle += steerItem.GetCurrentSteer();
        }
    }

    private void SteerShip()
    {
        float steerSpeed = Mathf.Clamp(shipSpeed, 10f, float.MaxValue);
        Vector3 change = new Vector3(0, (shipSteerAngle*Time.fixedDeltaTime * (steerSpeed * 0.05f)), 0);
        transform.eulerAngles += change;
    }

    #endregion

    //Do we even want these?
    private void OnTriggerStay(Collider other)
    {
        if (LogicUtils.IsPlayer(other.transform))
        {
            isPlayerInShip = true;
            CancelInvoke("RemovePlayerFromShip");
            //other.transform.SetParent(this.transform);
            //this.transform.SetParent(null);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (LogicUtils.IsPlayer(other.transform))
        {
            Invoke("RemovePlayerFromShip", 0.5f);

            //this.transform.SetParent(sea);
        }
    }

    private void RemovePlayerFromShip()
    {
        isPlayerInShip = false;
    }

    public void AddToInventory(Item item)
    {
       Engine engineItem = item as Engine;
       if (engineItem != null)
       {
           AddEngine(engineItem);
       }
       else
       {
           SteeringItem steeringItem = item as SteeringItem;
           if (steeringItem)
           {
               AddSteering(steeringItem);
           }
       }
    } 
    
    private void AddEngine(Engine engineItem)
    {
        if (this.engines.Contains(engineItem) == false)
        {
            this.engines.Add(engineItem);
        }
    }

    private void AddSteering(SteeringItem steeringItem)
    {
        if (this.steeringItems.Contains(steeringItem) == false)
        {
            this.steeringItems.Add(steeringItem);
        }
    }

    public void RemoveFromInventory(Item item)
    {
        Engine engineItem = item as Engine;
        if (engineItem != null)
        {
            RemoveEngine(engineItem);
        }
        else
        {
            SteeringItem steeringItem = item as SteeringItem;
            if (steeringItem)
            {
                RemoveSteering(steeringItem);
            }
        }
    }

    private void RemoveEngine(Engine engineItem)
    {
        if (this.engines.Contains(engineItem))
        {
            this.engines.Remove(engineItem);
        }
    }
    
    
    private void RemoveSteering(SteeringItem steeringItem)
    {
        if (this.steeringItems.Contains(steeringItem))
        {
            this.steeringItems.Remove(steeringItem);
        }
    }

    internal void ResetShip()
    {
        this.transform.eulerAngles = Vector3.zero;
        GameManager.Instance.SetWaveWeight(0.05f);
        this.transform.position = new Vector3(this.transform.position.x, 1f, this.transform.position.z);
    }

}
