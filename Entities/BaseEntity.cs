﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEntity : MonoBehaviour
{
    public EntityEvent onEntityDeath;
    public EntityDamageTaken onEntityDamageTaken;
    
    public delegate void EntityEvent();
    public delegate void EntityDamageTaken(BaseEntity damageSource, float damageDealt);
    private bool dead;
    private bool _isInvulnerable;
    private float currentHealth = -1;
    private float maxHealth;

    public float CurrentHealth
    {
        get { return this.currentHealth; }
    }

    /// <summary>
    /// Use this to modify the health instead of modifying it directly.
    /// </summary>
    /// <param name="changeAmount">+ or - change to health.</param>
    /// <param name="changeReason">what object caused the change</param>
    public virtual void ChangeHealth<T>(float changeAmount, T changeReason)
    {
        if (this.dead)
        {
            return;
        }

        if (changeAmount < 0 && this._isInvulnerable)
        {
            return;
        }

        if (changeAmount < 0)
        {
            if (changeReason.GetType() == typeof(BaseEntity))
            {
                BaseEntity dmgSource = (BaseEntity) (object) changeReason;
                this.onEntityDamageTaken(dmgSource, changeAmount);
            }
        }

        this.currentHealth += changeAmount;

        if (this.currentHealth > this.maxHealth)
        {
            this.currentHealth = this.maxHealth;
        }
        else if (this.currentHealth <= 0)
        {
            EntityDeath((object) changeReason);
        }
    }

    protected virtual void EntityDeath(object causeOfDeath = null)
    {
        this.dead = true;

        if (this.onEntityDeath != null)
        {
            this.onEntityDeath();
        }
    }
}
