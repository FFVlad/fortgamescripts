﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyShip : Enemy
{
    private List<EnemyCannon> _cannons = new List<EnemyCannon>();
    
    private Transform _target;
    private int _moveSpeed = 5;
    private NavMeshAgent _navMeshAgent;
    
    public Transform Target
    {
        get { return this._target;}
        set
        {
            UpdateCannonTarget(value);
            this._target = value;
        }

    }

    private void UpdateCannonTarget(Transform value)
    {
        foreach (var cannon in this._cannons)
        {
            if (cannon != null)
            {
                cannon.Target = value;
            }
        }
    }
    
    private void UpdateCannonVisualDistance(float value)
    {
        foreach (var cannon in this._cannons)
        {
            if (cannon != null)
            {
                cannon.VisualDistance = value;
            }
        }
    }

    private void Start()
    {
        this._cannons.AddRange(GetComponentsInChildren<EnemyCannon>());

        this._navMeshAgent = GetComponent<NavMeshAgent>();
        destination = this.startPosition = transform.position;
        UpdateCannonVisualDistance(this.visualDistance);
    }

    private void Update()
    {
        if (LookForPlayer() == false)
        {
            Roam();
        }
        else
        {
            FollowPlayer();
        }
       // RotateToTarget();
    }
    
    private bool LookForPlayer()
    {
        if (Vector3.Distance(ReferenceManager.PlayerVessel.transform.position, transform.position) < this.visualDistance)
        {
            Target = ReferenceManager.PlayerVessel.transform;
            return true;
        }

        return false;
    }
  
    private void Roam()
    {
        Vector3 positionToCheck = transform.position;
        positionToCheck.y = this.destination.y;
        if (Vector3.Distance(positionToCheck, this.destination) < 3)
        {
            FindNewMovePoint();
        }

        //transform.position = Vector3.MoveTowards(transform.position, this.destination, this._moveSpeed * Time.deltaTime);
    }
    
    private void FollowPlayer()
    {
        if (Vector3.Distance(transform.position, this.destination) > this.visualDistance * 1.2f)
        {
            Target = null;
        }
        else
        {
            this.destination = Target.position;
            this._navMeshAgent.SetDestination(Target.position);

        }
    }
    
    private void FindNewMovePoint()
    {
        float numOfTries = this.visualDistance;
        while (true)
        {
            if (numOfTries <= 0)
            {
                this.destination = this.startPosition;
                return;
            }

            Vector3 randomDirection = Random.insideUnitSphere * 2 * numOfTries;

            randomDirection += this.startPosition;

            UnityEngine.AI.NavMeshHit hit;
            UnityEngine.AI.NavMesh.SamplePosition(randomDirection, out hit, 20, 1);

            if (float.IsInfinity(hit.position.magnitude))
            {
                numOfTries--;
                continue;
            }

            Vector3 finalPosition = hit.position;
            this.destination = finalPosition;
            this._navMeshAgent.SetDestination(this.destination);
            break;
        }
    }
    
    private void RotateToTarget()
    {
        Vector3 shootPosition = this.destination + Vector3.up;
        Quaternion desiredRotation = Quaternion.LookRotation(shootPosition - transform.position);
        Quaternion newRotation = Quaternion.RotateTowards(transform.rotation, desiredRotation, 10 * Time.deltaTime);
        Vector3 newRotationEulerAngles = newRotation.eulerAngles;
        newRotationEulerAngles.z = 0;
        newRotation.eulerAngles = newRotationEulerAngles;
        transform.rotation = newRotation;
    }
}
