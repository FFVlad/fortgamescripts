﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using Crest;
using KinematicCharacterController;
public class PlayerController : MonoBehaviour
{
    private Vector3 _lastParentLocation;
    private Quaternion _lastParentRotation;

    private Vector3 _parentLocationChange;
    private Quaternion _parentRotationChange;
    //General stuff
    private KinematicCharacterMotor _KCMotor;
    private KinematicCharacterController.Walkthrough.WallJumping.MyCharacterController _myCharacterController;
    private KinematicCharacterController.Walkthrough.WallJumping.MyPlayer _myPlayer;
    private float _originalStableSpeed, _originalJumpSpeed;
    private PlayerAnimator _playerAnimator;

    internal GameObject VesselLocalPositionObject;


    private void OnEnable()
    {
        if (playerCamera && playerCamera.gameObject)
        {
            playerCamera.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        if (playerCamera && playerCamera.gameObject)
        {
            playerCamera.gameObject.SetActive(false);
        }
    }

    internal GameObject playerCamera;


    private VesselBase vessel;
    public VesselBase Vessel
    {
        get
        {
            if (this.vessel == null)
            {
                this.vessel = FindObjectOfType<VesselBase>();
            }
            return this.vessel;
        }
    }

    //Interaction
    private Item _itemCurrentlyLookedAt;
    private IInteractable _currentInteraction;
    private IInteractable _currentlyPickedUpItem;
    internal bool _interactingWithSomething;
    internal bool _didAttemptInteractLastFrame = false;
    private float _pickUpProgress = 0f;

    internal IInteractable CurrentInteraction
    {
        get
        {
            return _currentInteraction;
        }
    }

    private Inventory playerInventory;
    internal Inventory CurrentPlayerInventory
    {
        get
        {
            if (playerInventory == null)
            {
                playerInventory = InventoryManager.Instance.GetPlayerInventory(playerID);
            }

            return playerInventory;
        }
    }

    private int playerID = 0; // edit this to have different inventories if we get multiplayer later
    
    //Ocean
    OceanRenderer oceanRenderer;
    private bool _isSwimming
    {
        get
        {
            return oceanRenderer.ViewerHeightAboveWater < 0f;
        }
    }
    #region Setup
    private void Awake()
    {
        SetupCamera();

        oceanRenderer = FindObjectOfType<OceanRenderer>();
        if (oceanRenderer)
        {
            oceanRenderer.Viewpoint = playerCamera.transform;
        }
    }

    private void SetupCamera()
    {
        if (Camera.main && Camera.main.gameObject)
        {
            playerCamera = Camera.main.gameObject;
            _playerAnimator = playerCamera.GetComponentInChildren<PlayerAnimator>();
        }
        
        if(playerCamera == null)
        {
            //Make animations work in build scene
            playerCamera = BuildingManager.BuilderSceneInstance.PlayerCameraParent;
            if (playerCamera)
            {
                _playerAnimator = playerCamera.GetComponentInChildren<PlayerAnimator>();
            }
        }
    }

    private void Start()
    {        
        _KCMotor = this.GetComponent<KinematicCharacterMotor>();
        _myCharacterController = this.GetComponent<KinematicCharacterController.Walkthrough.WallJumping.MyCharacterController>();
        _myPlayer = FindObjectOfType<KinematicCharacterController.Walkthrough.WallJumping.MyPlayer>();
        _originalStableSpeed = _myCharacterController.MaxStableMoveSpeed;
        _originalJumpSpeed = _myCharacterController.JumpSpeed;
        vp_Respawn();
    }


    #endregion

    #region Updates
    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            vp_Respawn();
        }
    }

    private void Update()
    {
        CheckIfPlayerFellOffShip();

        if (_interactingWithSomething)
        {
            
            if (this._currentInteraction != null && this._currentInteraction is IContinuousInteractable && (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.E)))
            {
                this._currentInteraction.SingleInteract(this);
            }
        }

        HandleInteraction();
    }
    #endregion

    #region Interaction
    private void HandleInteraction()
    {
        SearchForInteraction();

        ResetPickUpProgress();

        HandleSingleAndContinuousInteractionAttempts();

        HandlePickUpProcess();

    }

    private void HandleSingleAndContinuousInteractionAttempts()
    {
        if (_itemCurrentlyLookedAt != null && _currentInteraction == null && !Input.GetKey(KeyCode.E) && _didAttemptInteractLastFrame)
        {
            //Debug.LogError("This is where we START interacting");

            AttemptInteract();
        }
        else if (_currentInteraction != null && Input.GetKey(KeyCode.Q))
        {
            //Debug.LogError("This is where we STOP interacting");
            AttemptCancelInteract();
            _myPlayer._freezeCamera = false;
            _myCharacterController.JumpSpeed = _originalJumpSpeed;
            _myCharacterController.MaxStableMoveSpeed = _originalStableSpeed;
            ResetInteraction();
        }
    }

    #region Pick Up process

    private void HandlePickUpProcess()
    {
        if (_interactingWithSomething)
        {
            return;
        }

        if (_itemCurrentlyLookedAt != null && Input.GetKey(KeyCode.E) && _currentlyPickedUpItem == null)
        {
            IncreasePickUpProgress();
        }
        else if (!Input.GetKey(KeyCode.E) && _currentlyPickedUpItem != null)
        {
            AttemptDropItem();
        }

        if (Input.GetKey(KeyCode.E) && _pickUpProgress > 1f && _currentlyPickedUpItem == null)
        {
            AttempPickUpItem();
        }

        _didAttemptInteractLastFrame = Input.GetKey(KeyCode.E);
    }

    private void AttemptDropItem()
    {
        AttemptInteract(true, true);
    }

    private void AttempPickUpItem()
    {
        AttemptInteract(true);
    }

    private void IncreasePickUpProgress()
    {
        _pickUpProgress += 0.05f;
    }

    private void ResetPickUpProgress()
    {
        if (!_didAttemptInteractLastFrame)
        {
            _pickUpProgress = 0f;
        }
    }
    #endregion

    private void SearchForInteraction()
    {
        //If we are currently holding an item, no need to look for interactions
        if (_currentlyPickedUpItem != null)
        {
            return;
        }

        RaycastHit hit;
        Ray ray = new Ray(this.playerCamera.transform.position, playerCamera.transform.forward);
        Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward * 3f, Color.green, 0.01f);
        if (Physics.Raycast(ray, out hit, 3f))
        {
            if (LogicUtils.IsInteractable(hit.collider.transform))
            {
                GameObject hitObject = hit.collider.transform.gameObject;
                if (hitObject && this._itemCurrentlyLookedAt && hitObject == this._itemCurrentlyLookedAt.gameObject)
                {
                    return;
                }
                if (_itemCurrentlyLookedAt)
                {
                    //Won't make changes to itembase before merge, this is just for testing make these both ITEMS and put methods in itembase
                    HandleColoring(false);
                }

                if (hitObject)
                {
                    _itemCurrentlyLookedAt = hitObject.GetComponent<Item>();

                    if (_itemCurrentlyLookedAt != null)
                    {
                        HandleCurrentlyLookedAtItem(true);
                    }
                }
            }
            else
            {
                HandleCurrentlyLookedAtItem(false);

            }
        }
        else
        {
            HandleCurrentlyLookedAtItem(false);
        }
    }

    private void HandleCurrentlyLookedAtItem(bool hasItem)
    {
        if (hasItem)
        {
            HandleColoring(true);
        }
        else
        {
            HandleColoring(false);

            _itemCurrentlyLookedAt = null;
        }
    }

    private void AttemptInteract(bool isPickup = false, bool hadItemInHand = false)
    {
        if (_itemCurrentlyLookedAt)
        {
            Item item = _itemCurrentlyLookedAt.GetComponent<Item>();

            _currentInteraction = item;

            if (_currentInteraction != null)
            {
                if (!isPickup)
                {
                    //Player always interacts with the item he drops, is this intended?
                    //At least doesn't look good with animation, this skips the animation
                    if (_currentlyPickedUpItem == null)
                    {
                        _playerAnimator.Interact();
                    }

                    _currentInteraction.SingleInteract(this);

                    //Check to see if we want to hold reference to _currentInteraction
                    CheckIfCurrentIsContinuous();

                }
                else
                {
                    HandlePickupItem(hadItemInHand);

                }
            }
            else
            {
                _currentInteraction = null;
                return;
            }
        }
    }

    private void AttemptCancelInteract()
    {
        if (_currentInteraction != null && _currentInteraction is Item)
        {
            Item item = _currentInteraction as Item;
            item.CancelInteract();
            _currentInteraction = null;
        }
    }

    private void CheckIfCurrentIsContinuous()
    {
        IContinuousInteractable continuousCheck = _currentInteraction as IContinuousInteractable;
        if (continuousCheck == null)
        {
            //Debug.LogError("This is a single use item");
            _currentInteraction = null;
        }
        else
        {
            //Debug.LogError("Started interacting with a continuous use item!");
            _myCharacterController.MaxStableMoveSpeed = 0;
            _myCharacterController.JumpSpeed = 0;
            _myPlayer._freezeCamera = true;
            HandleColoring(false);
        }
    }

    private void ResetInteraction()
    {
        _currentInteraction = null;
        _interactingWithSomething = false;
    }

    private void HandlePickupItem(bool hadItemInHand)
    {
        if (!hadItemInHand)
        {
            _playerAnimator.Grab();
            StartHoldingItem();
        }
        else
        {
            _playerAnimator.Drop();
            DropHeldItem();
        }
    }

    private void StartHoldingItem()
    {

        _currentInteraction.Hold(true, this);

        _currentlyPickedUpItem = _currentInteraction;

    }

    private void DropHeldItem()
    {
        HandleColoring(false);

        _currentInteraction.Hold(false, this);

        _currentlyPickedUpItem = null;

    }

    public void StopInteractingWith(Item item)
    {
        if (this._itemCurrentlyLookedAt == item)
        {
            this._itemCurrentlyLookedAt = null;
            ResetInteraction();
        }
    }

    private void HandleColoring(bool active)
    {
        if (_itemCurrentlyLookedAt)
        {
            if (_interactingWithSomething)
            {
                active = false;
            }

            _itemCurrentlyLookedAt.SwitchInteractionMaterial(active);
        }
        else if (_currentInteraction != null)
        {
            (_currentInteraction as Item).SwitchInteractionMaterial(false);
        }
    }

    #endregion

    private void vp_Respawn()
    {
        ResetInteraction();
        _KCMotor.SetPosition(GameManager.Instance.currentBoat.GetComponent<BoatFrameDataContainer>().playerSpawnPoint.transform.position + Vector3.up);
        _KCMotor.BaseVelocity = Vector3.zero;
        Vessel.isPlayerInShip = true;
    }

    private void CheckIfPlayerFellOffShip()
    {
        if(this.transform.position.y + 5 < Vessel.transform.position.y)
        {
            vp_Respawn();
            vessel.ResetShip();
        }
    }


}