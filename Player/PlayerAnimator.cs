﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{

    private Animator _animator;

    private int GRAB;
    private int DROP;
    private int INTERACT;
    void Start()
    {
        _animator = GetComponent<Animator>();

        this.GRAB = Animator.StringToHash("Grab");
        this.DROP = Animator.StringToHash("Drop");
        this.INTERACT = Animator.StringToHash("Interact");
    }

    internal void Interact()
    {
        _animator.SetTrigger(INTERACT);
    }

    internal void Grab()
    {
        _animator.SetTrigger(GRAB);
    }

    internal void Drop()
    {
        _animator.SetTrigger(DROP);
    }
}

