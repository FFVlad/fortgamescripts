﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDeletableObject
{
    void DeletableHilighted();
    void DeletableUnhighlighted();
    void DeleteItem(Inventory playerWhoRemovedIt);
}
