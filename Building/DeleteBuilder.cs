﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DeleteBuilder : BuilderModule
{
    private IDeletableObject currentlySelectedDeletable;

    internal override void Raycasting(Ray ray)
    {
        base.Raycasting(ray);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, ignoreRaycastLayer) && !EventSystem.current.IsPointerOverGameObject())
        {
            GameObject objectHit = hit.collider.transform.gameObject;

            if (objectHit != null)
            {
                IDeletableObject deletable = objectHit.GetComponent<IDeletableObject>();

                if (deletable != null)
                {
                    if (deletable != this.currentlySelectedDeletable)
                    {
                        ClearCurrentDeletable();
                        this.currentlySelectedDeletable = deletable;
                        this.currentlySelectedDeletable.DeletableHilighted();
                    }
                }
                else
                {
                    ClearCurrentDeletable();
                }
            }
            else
            {
                ClearCurrentDeletable();
            }
        }
        else
        {
            ClearCurrentDeletable();
        }
    }

    internal override void ModuleUnSelected()
    {
        base.ModuleUnSelected();
        ClearCurrentDeletable();
    }

    internal override void TryToDoAction()
    {
        base.TryToDoAction();
        TryToDeleteCurrentItem();
    }

    private void ClearCurrentDeletable()
    {
        if (this.currentlySelectedDeletable != null)
        {
            this.currentlySelectedDeletable.DeletableUnhighlighted();
            this.currentlySelectedDeletable = null;
        }
    }

    private void TryToDeleteCurrentItem()
    {
        if (this.currentlySelectedDeletable != null)
        {
            this.currentlySelectedDeletable.DeleteItem(this.currentPlayerInventory);
            this.currentlySelectedDeletable = null;

            if (BuildingUI.BuildingInstance)
            {
                BuildingUI.BuildingInstance.UpdateResources(this.currentPlayerInventory);
            }
        }
    }

}
