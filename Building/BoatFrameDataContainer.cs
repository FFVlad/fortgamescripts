﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatFrameDataContainer : MonoBehaviour
{
    public List<BlockAttachPoint> permanentBlocks = new List<BlockAttachPoint>();
    public Rigidbody boatRigidBody;
    public BoatProbes boatProbes;
    public Transform shipPieceParent;
    public Transform emptyAttachPointsParent;
    public Transform playerSpawnPoint;

    private int blocksAttachedToThis = 0;

    internal void CheckShipIntegrity()
    {
        List<BlockAttachPoint> allBlocksInShip = new List<BlockAttachPoint>(GetComponentsInChildren<BlockAttachPoint>());
        List<BlockAttachPoint> attachedToShipBlocks = new List<BlockAttachPoint>();

        foreach (BlockAttachPoint blockAttach in permanentBlocks)
        {
            attachedToShipBlocks.Add(blockAttach);

            blockAttach.CalculateAttachPoints();
            
            foreach (BlockAttachPoint attachedPoint in blockAttach.attachedToPoints)
            {
                if (!attachedToShipBlocks.Contains(attachedPoint))
                {
                    attachedToShipBlocks.Add(attachedPoint);
                }
            }
        }

        foreach (BlockAttachPoint blockAttach in allBlocksInShip)
        {
            if (blockAttach.placedOnTheFrame && !attachedToShipBlocks.Contains(blockAttach))
            {
                attachedToShipBlocks.Add(blockAttach);
            }
        }

        CalculateAllBlocksAttached(attachedToShipBlocks);
        List<BlockAttachPoint> notAttachedToShip = new List<BlockAttachPoint>();

        foreach (BlockAttachPoint attachPoint in allBlocksInShip)
        {
            if (!attachedToShipBlocks.Contains(attachPoint))
            {
                notAttachedToShip.Add(attachPoint);
            }

            attachPoint.calculatedAttachPoints = false;
        }

        if (notAttachedToShip.Count > 0)
        {
            GameObject orphanShipPrefab = GameManager.Instance.orphanShipPrefab;
            int orphanIndex = 1;

            while (notAttachedToShip.Count > 0)
            {
                BlockAttachPoint newShipThing = notAttachedToShip[0];
                List<BlockAttachPoint> shipParts = new List<BlockAttachPoint>();
                shipParts.Add(newShipThing);

                CalculateAllBlocksAttached(shipParts);

                GameObject orphanChildrenParent = Instantiate(orphanShipPrefab, newShipThing.transform.position, newShipThing.transform.rotation);
                orphanChildrenParent.name = "OrphanShip " + orphanIndex++;

                Rigidbody newBoatBody = orphanChildrenParent.GetComponent<Rigidbody>();          

                BoatFrameDataContainer frameDataContainer = orphanChildrenParent.GetComponent<BoatFrameDataContainer>();

                foreach (BlockAttachPoint attachPoint in shipParts)
                {
                    attachPoint.transform.parent = orphanChildrenParent.transform;                    
                    notAttachedToShip.Remove(attachPoint);
                }

                frameDataContainer.RecalculateForcePoints();
            }
        }        

        if (attachedToShipBlocks.Count != blocksAttachedToThis)
        {
            blocksAttachedToThis = attachedToShipBlocks.Count;
            RecalculateForcePoints();
        }
    }

    private void CalculateAllBlocksAttached(List<BlockAttachPoint> attachedToShipPoints)
    {
        bool foundSomethingNew = true;

        while (foundSomethingNew)
        {
            bool somethingFound = false;

            List<BlockAttachPoint> newBlocks = new List<BlockAttachPoint>();

            foreach (BlockAttachPoint attachedPoint in attachedToShipPoints)
            {
                if (!attachedPoint.calculatedAttachPoints)
                {
                    attachedPoint.CalculateAttachPoints();                    
                }

                foreach (BlockAttachPoint attachPoint in attachedPoint.attachedToPoints)
                {
                    if (!attachedToShipPoints.Contains(attachPoint) && !newBlocks.Contains(attachPoint))
                    {
                        newBlocks.Add(attachPoint);
                        somethingFound = true;
                    }
                }
            }

            attachedToShipPoints.AddRange(newBlocks);
            foundSomethingNew = somethingFound;
        }
    }

    internal void RecalculateForcePoints()
    {
        if (boatProbes)
        {
            BlockAttachPoint[] blocks = GetComponentsInChildren<BlockAttachPoint>();
            EmptyAttachPoint[] frames = GetComponentsInChildren<EmptyAttachPoint>();

            boatProbes._forcePoints = new FloaterForcePoints[blocks.Length + frames.Length];

            Vector3 middlePoint = new Vector3();

            for (int i = 0; i < blocks.Length; i++)
            {
                Vector3 blockPos = blocks[i].transform.localPosition;
                middlePoint += blockPos;
                boatProbes._forcePoints[i] = new FloaterForcePoints() { _weight = 1, _offsetPosition = blockPos };
            }

            for (int i = 0; i < frames.Length; i++)
            {
                Vector3 blockPos = frames[i].transform.localPosition;
                middlePoint += blockPos;
                boatProbes._forcePoints[blocks.Length + i] = new FloaterForcePoints() { _weight = 0.5f, _offsetPosition = blockPos };
            }

            middlePoint /= blocks.Length + frames.Length;
            boatProbes._centerOfMass = middlePoint;

            boatProbes.enabled = true;
        }
    }

    internal void TimedCheckShipIntegrity()
    {
        CancelInvoke("CheckShipIntegrity");
        Invoke("CheckShipIntegrity", 0.1f);
    }

    internal void TimedRecalculateForcePoints()
    {
        CancelInvoke("RecalculateForcePoints");
        Invoke("RecalculateForcePoints", 0.1f);
    }

}
