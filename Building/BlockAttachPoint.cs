﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockAttachPoint : AttachPoint, IDeletableObject, IItemPlaceable
{
    public int blockID = -1;
    public BoxCollider boxCollider;

    [Header("Deletion variables")]
    public List<Renderer> renderersToChangeMaterialFor;
    public Material normalMaterial;
    public Material deleteMaterial;

    public bool Indestructible = false;
    public bool HasSeaHole = false;
    public bool placedOnTheFrame = false;

    private List<Item> _attachedItems = new List<Item>();

    public struct ObjectSize
    {
        public bool initialised;
        public float xSize;
        public float ySize;
        public float zSize;
    }

    public ObjectSize size;
    public ObjectSize Size
    {
        get
        {
            if (this.size.initialised == false)
            {
                this.size.xSize = this.boxCollider.size.x / 2 * transform.localScale.x;
                this.size.ySize = this.boxCollider.size.y / 2 * transform.localScale.y + 0.035f;
                this.size.zSize = this.boxCollider.size.z / 2 * transform.localScale.z;

            }
            return this.size;
        }
    }
    
    public List<PlacementDirection> legalDirections = new List<PlacementDirection>()
    {
        PlacementDirection.Forward,
        PlacementDirection.Back,
        PlacementDirection.Left,
        PlacementDirection.Right
    };

    private PlacementDirection direction = PlacementDirection.Forward;

    internal List<BlockAttachPoint> attachedToPoints;
    internal bool calculatedAttachPoints = false;

    internal override bool IsLegalAttach(StructureBlock blockBeingAttached, RaycastHit raycastHit)
    {
        this.direction = CalculateRaycastHitDirection(raycastHit);
        return this.legalDirections.Contains(this.direction);
    }

    internal override void PlaceGhostBlockOnThis(GameObject ghostBlock, RaycastHit raycastHit)
    {
        base.PlaceGhostBlockOnThis(ghostBlock, raycastHit);

        Vector3 positionDiff = GetObjectOnSideDiff();

        ghostBlock.transform.position = transform.position + positionDiff;
        ghostBlock.transform.rotation = transform.rotation;
    }

    internal override void PlaceBlockOnThis(GameObject blockToPlace)
    {
        base.PlaceBlockOnThis(blockToPlace);

        Vector3 positionDiff = GetObjectOnSideDiff();

        blockToPlace.transform.position = transform.position + positionDiff;
        blockToPlace.transform.rotation = transform.rotation;
    }

    internal override bool IsLegalItemPlacement(RaycastHit raycastHit)
    {
        this.direction = CalculateRaycastHitDirection(raycastHit);
        return this.direction == PlacementDirection.Top;
    }

    internal override Vector3 GetClosestSnapPoint(ItemData data, RaycastHit raycastHit)
    {
        Vector3 diffFromCenterToTop = transform.up * (Size.ySize);

        if (data.snappingStyle == SnappingStyle.Wall)
        {
            List<Vector3> comparisonVectors = new List<Vector3>();
          
            Vector3 diffFromCenterToRight = transform.right * (Size.xSize  - data.itemSize / 2);
            Vector3 diffFromCenterToForward = transform.forward * (Size.zSize  - data.itemSize / 2);
            
            Vector3 leftPoint = transform.position - diffFromCenterToRight + diffFromCenterToTop;
            comparisonVectors.Add(leftPoint);

            Vector3 rightPoint = transform.position + diffFromCenterToRight + diffFromCenterToTop;
            comparisonVectors.Add(rightPoint);

            Vector3 topPoint = transform.position + diffFromCenterToForward + diffFromCenterToTop;
            comparisonVectors.Add(topPoint);

            Vector3 bottomPoint = transform.position - diffFromCenterToForward + diffFromCenterToTop;
            comparisonVectors.Add(bottomPoint);

            return PositionUtils.GetClosestPositionFromVectors(comparisonVectors, raycastHit.point);
        }
        else
        {
            return transform.position + diffFromCenterToTop;
        }
    }

    internal PlacementDirection CalculateRaycastHitDirection(RaycastHit raycastHit)
    {
        Vector3 localPoint = raycastHit.collider.transform.InverseTransformPoint(raycastHit.point);
        Vector3 localDir = localPoint.normalized;

        float upDot = Vector3.Dot(localDir, Vector3.up);
        float fwdDot = Vector3.Dot(localDir, Vector3.forward);
        float rightDot = Vector3.Dot(localDir, Vector3.right);

        float upPower = Mathf.Abs(upDot);
        float fwdPower = Mathf.Abs(fwdDot);
        float rightPower = Mathf.Abs(rightDot);

        if (upPower > fwdPower && upPower > rightPower)
        {
            if (upDot > 0)
            {
                return PlacementDirection.Top;
            }
            else
            {
                return PlacementDirection.Bottom;
            }
        }
        else if (fwdPower > upPower && fwdPower > rightPower)
        {
            if (fwdDot > 0)
            {
                return PlacementDirection.Forward;

            }
            else
            {
                return PlacementDirection.Back;
            }
        }
        else if (rightPower > upPower && rightPower > fwdPower)
        {
            if (rightDot > 0)
            {
                return PlacementDirection.Right;
            }
            else
            {
                return PlacementDirection.Left;
            }
        }

        return PlacementDirection.Bottom;
    }

    private Vector3 GetObjectOnSideDiff()
    {
        switch (this.direction)
        {
            case PlacementDirection.Back:
                return -transform.forward * transform.lossyScale.z;
            case PlacementDirection.Forward:
                return transform.forward * transform.lossyScale.z;
            case PlacementDirection.Bottom:
                return new Vector3(0, -transform.lossyScale.y, 0);
            case PlacementDirection.Top:
                return new Vector3(0, transform.lossyScale.y, 0);
            case PlacementDirection.Right:
                return transform.right * transform.lossyScale.x;
            case PlacementDirection.Left:
                return -transform.right * transform.lossyScale.x;
        }

        return new Vector3();
    }

    public void DeletableHilighted()
    {
        if (!this.Indestructible)
        {
            foreach (Renderer renderer in this.renderersToChangeMaterialFor)
            {
                renderer.material = this.deleteMaterial;
            }
        }
    }

    public void DeletableUnhighlighted()
    {
        if (!this.Indestructible)
        {
            foreach (Renderer renderer in this.renderersToChangeMaterialFor)
            {
                renderer.material = this.normalMaterial;
            }
        }
    }

    public void DeleteItem(Inventory playerWhoRemovedIt)
    {
        if (this.Indestructible)
        {
            return;
        }

        if (this.placedOnTheFrame)
        {
            Builder.Instance.ReplaceDeletedStructureWithEmptyFrameObject(this);
        }

        playerWhoRemovedIt.DeletedBlock(this.blockID);

        if (GameManager.Instance.currentPlayMode == PlayMode.PlayMode)
        {
            BoatFrameDataContainer data = transform.root.GetComponent<BoatFrameDataContainer>();

            if (data)
            {
                data.TimedRecalculateForcePoints();
            }
        }

        Destroy(gameObject);
    }

    public bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle)
    {
        Vector3 offset = new Vector3();
        ItemData itemData = DataManager.Instance.GetItemWithID(item.itemId);

        if (itemData != null)
        {
            offset = itemData.itemHeightDiff;
        }
        else
        {
            Collider col = item.GetComponent<Collider>();
            if(col is BoxCollider)
            {
                offset.y = (col as BoxCollider).size.y;
            }
            else
            {
                offset.y = col.bounds.size.y;
            }
            
        }

        if (this._attachedItems.Count > 0)
        {
            foreach (var attachedItem in this._attachedItems)
            {
                IItemPlaceable itemPlaceable = attachedItem as IItemPlaceable;
                if (itemPlaceable != null && itemPlaceable.TryToPlaceItemOnThis(item, snappingStyle))
                {
                    return true;
                }
            }

            snappingStyle = SnappingStyle.Drop;
        }

        PlacementHelper.PlaceItem(item, snappingStyle, offset, this);
        item.isInWater = this.HasSeaHole;

        return true;
    }

    public void UnattachItem(Item item)
    {
        if (this._attachedItems.Contains(item))
        {
            this._attachedItems.Remove(item);
            item.attachedTo = null;
        }
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public Vector3 GetPlacingPosition()
    {
        return transform.position + transform.up * Size.ySize;
    }

    public List<Item> GetAttachedItems()
    {
        return this._attachedItems;
    }

    public void AttachItem(Item itemToAttach)
    {
        if (this._attachedItems.Contains(itemToAttach) == false)
        {
            this._attachedItems.Add(itemToAttach);
        }
    }

    internal void CalculateAttachPoints()
    {
        attachedToPoints = new List<BlockAttachPoint>();
        ObjectSize size = Size;
        Vector3 startingPosition = transform.position;
        startingPosition.y += size.ySize;

        LayerMask ignoreRaycastLayer = ~(1 << ReferenceManager.KCCLayer);

        // for debugging this if necessary.
        //Debug.DrawRay(startingPosition, transform.forward * size.xSize * 1.5f, Color.red, 100f);
        //Debug.DrawRay(startingPosition, -transform.forward * size.xSize * 1.5f, Color.red, 100f);
        //Debug.DrawRay(startingPosition, transform.right * size.zSize * 1.5f , Color.red, 100f);
        //Debug.DrawRay(startingPosition, -transform.right * size.zSize * 1.5f, Color.red, 100f);

        Ray testRay = new Ray(startingPosition, transform.forward * size.xSize * 1.5f);
        CheckRayHit(testRay, size.xSize * 1.5f, ignoreRaycastLayer);

        testRay = new Ray(startingPosition, -transform.forward * size.xSize * 1.5f);
        CheckRayHit(testRay, size.xSize * 1.5f, ignoreRaycastLayer);

        testRay = new Ray(startingPosition, transform.right * size.zSize * 1.5f);
        CheckRayHit(testRay, size.zSize * 1.5f, ignoreRaycastLayer);

        testRay = new Ray(startingPosition, -transform.right * size.zSize * 1.5f);
        CheckRayHit(testRay, size.zSize * 1.5f, ignoreRaycastLayer);

        calculatedAttachPoints = true;
    }

    private void CheckRayHit(Ray ray, float distance, LayerMask layers)
    {
        RaycastHit raycastHit;

        if (Physics.Raycast(ray, out raycastHit, distance, layers))
        {
            BlockAttachPoint blockAttach = raycastHit.collider.GetComponent<BlockAttachPoint>();
            if (blockAttach != null)
            {
                attachedToPoints.Add(blockAttach);
            }
        }
    }
}