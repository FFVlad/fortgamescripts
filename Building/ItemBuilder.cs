﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemBuilder : BuilderModule
{
    private bool snappingEnabled = false;
    private RaycastHit lastHit;
    private Vector3 defaultRotation = new Vector3();
    private Vector3 rotationDiff = new Vector3();

    private ItemData currentItem
    {
        get
        {
            return this.currentPlayerInventory.equippedItem;
        }
    }

    internal override void Raycasting(Ray ray)
    {
        base.Raycasting(ray);

        if (this.currentPlayerInventory.equippedItem == null && !EventSystem.current.IsPointerOverGameObject())
        {
            if (this.activeGhostPreviewObject != null)
            {
                GameObject.Destroy(this.activeGhostPreviewObject);
                this.activeGhostPreviewObject = null;
            }

            return;
        }

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, ignoreRaycastLayer) && !EventSystem.current.IsPointerOverGameObject())
        {
            GameObject objectHit = hit.collider.transform.gameObject;

            if (objectHit != null)
            {
                this.currentlyActiveAttachPoint = objectHit.GetComponent<AttachPoint>();

                if (this.currentlyActiveAttachPoint != null && this.currentlyActiveAttachPoint.IsLegalItemPlacement(hit))
                {
                    Vector3 placement = PlaceGhostItem(this.currentlyActiveAttachPoint, hit);
                    Quaternion rotation = SetGhostItemRotation(placement, this.currentlyActiveAttachPoint);

                    if (this.activeGhostPreviewObject == null)
                    {
                        if (this.currentPlayerInventory.equippedItem.ghostPrefab == null)
                        {
                            Debug.LogError("KARRI PLEASE NO ITEMS WITH NULL DATA FOR NO REASONS, EMPTY STUFF BREAK THINGS 8(");
                            return;
                        }

                        this.activeGhostPreviewObject = GameObject.Instantiate(this.currentPlayerInventory.equippedItem.ghostPrefab, placement, rotation);
                        this.defaultRotation = this.activeGhostPreviewObject.transform.rotation.eulerAngles;
                    }
                    else
                    {
                        this.activeGhostPreviewObject.transform.position = placement;
                        this.activeGhostPreviewObject.transform.rotation = rotation;
                    }
                }
                else
                {
                    ClearCurrentGhostObject();
                }
            }
            else
            {
                ClearCurrentGhostObject();
            }
        }
        else
        {
            ClearCurrentGhostObject();
        }

        this.lastHit = hit;
    }

    internal override void TryToDoAction()
    {
        base.TryToDoAction();

        if (this.activeGhostPreviewObject != null)
        {
            GhostCollisionChecker check = this.activeGhostPreviewObject.GetComponent<GhostCollisionChecker>();

            if (check != null && check.CheckIfCurrentlyLegal())
            {
                if (this.currentPlayerInventory.equippedItem.itemPlacingPrefab == null)
                {
                    Debug.LogError("KARRI PLEASE NO ITEMS WITH NULL DATA FOR NO REASONS, EMPTY STUFF BREAK THINGS 8(");
                    return;
                }

                GameObject newItem = GameObject.Instantiate(this.currentPlayerInventory.equippedItem.itemPlacingPrefab);
                newItem.transform.position = this.activeGhostPreviewObject.transform.position;
                newItem.transform.rotation = this.activeGhostPreviewObject.transform.rotation;
                newItem.transform.parent = this.boatFrameData.shipPieceParent;

                Item[] itemScripts = newItem.GetComponentsInChildren<Item>();
                this.currentPlayerInventory.PlacedCurrentItem();

                if (itemScripts != null)
                {
                    foreach (var item in itemScripts)
                    {
                        PickupableComponent pickupable = item.GetItemComponentOfType<PickupableComponent>();

                        if (pickupable != null)
                        {
                            pickupable.TryToParentThis();
                        }
                    }
                }
            }
        }
    }

    internal override void ChangeSelectionForward()
    {
        base.ChangeSelectionForward();

        this.currentPlayerInventory.EquipNextItem();
        ClearCurrentGhostObject();
    }

    internal override void ChangeSelectionBackward()
    {
        base.ChangeSelectionBackward();

        this.currentPlayerInventory.EquipPreviousItem();
        ClearCurrentGhostObject();
    }

    internal override void ModuleSelected()
    {
        base.ModuleSelected();
        this.rotationDiff = new Vector3();

        if (PlayUI.Instance)
        {
            PlayUI.Instance.SetSnappingParent(true);
            PlayUI.Instance.SetItemDataParent(true);
        }
    }

    internal override void RotateItemForward(float speedVariable)
    {
        base.RotateItemForward(speedVariable);
        RotateCurrentGhostItem(speedVariable * this.currentPlayerInventory.equippedItem.rotationAxis1 * Time.deltaTime);
    }

    internal override void RotateItemBackward(float speedVariable)
    {
        base.RotateItemBackward(speedVariable);
        RotateCurrentGhostItem(-speedVariable * this.currentPlayerInventory.equippedItem.rotationAxis1 * Time.deltaTime);
    }

    protected override void UpdateRelevantUI()
    {
        base.UpdateRelevantUI();

        if (PlayUI.Instance && this.currentlyActiveModule)
        {
            PlayUI.Instance.ChangeSelectedThingText(this.currentPlayerInventory.GetCurrentItemUIText());
            SetSnappingText();
        }
    }

    internal void ToggleSnapping()
    {
        this.snappingEnabled = !this.snappingEnabled;
        SetSnappingText();
    }

    private void SetSnappingText()
    {
        if (this.snappingEnabled)
        {
            if (PlayUI.Instance)
            {
                PlayUI.Instance.ChangeSnappingMode("Snapping: enabled");
            }
        }
        else
        {
            if (PlayUI.Instance)
            {
                PlayUI.Instance.ChangeSnappingMode("Snapping: disabled");
            }
        }
    }

    private Vector3 PlaceGhostItem(AttachPoint attachPoint, RaycastHit hit)
    {
        if (this.snappingEnabled)
        {
            return this.currentlyActiveAttachPoint.GetClosestSnapPoint(currentItem, hit) + currentItem.itemHeightDiff;
        }
        else
        {
            return hit.point + hit.normal * this.currentPlayerInventory.equippedItem.itemHeightDiff.y;
        }
    }

    private Quaternion SetGhostItemRotation(Vector3 placementPosition, AttachPoint attachPoint)
    {
        if (this.snappingEnabled)
        {
            Vector3 midPoint = attachPoint.transform.position;
            midPoint.y = placementPosition.y;
            BlockAttachPoint attachBlock = attachPoint as BlockAttachPoint;

            if (attachBlock)
            {
                if (currentItem.snappingStyle != SnappingStyle.Wall)
                {
                    midPoint = AdjustMidPointToEdge(midPoint, attachBlock);
                }
            }

            if (this.debug)
            {
                if (this.midpointIndicator)
                {
                    this.midpointIndicator.transform.SetParent(attachPoint.transform);
                    this.midpointIndicator.transform.position = midPoint;
                    UnityEditor.Selection.activeGameObject = this.midpointIndicator;
                    this.midpointIndicator.name = attachPoint.transform.up.ToString();
                }

                else
                {
                    this.midpointIndicator = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    this.midpointIndicator.transform.localScale /= 5;
                }
            }

            if (currentItem.snappingStyle != SnappingStyle.Wall)
            {
                return attachPoint.transform.rotation;
            }
            else
            {
                return Quaternion.LookRotation(placementPosition - midPoint, attachPoint.transform.up);
            }
        }
        else
        {
            Quaternion desiredRotation = Quaternion.Euler(this.defaultRotation + this.rotationDiff); ;
            desiredRotation.z = attachPoint.transform.rotation.z;
            return desiredRotation;
        }
    }

    private Vector3 AdjustMidPointToEdge(Vector3 midPoint, BlockAttachPoint attachPoint)
    {
        midPoint += attachPoint.transform.forward * attachPoint.Size.zSize;
        return midPoint;
    }

    private void RotateCurrentGhostItem(Vector3 rotationSpeed)
    {
        if (this.activeGhostPreviewObject != null)
        {
            this.rotationDiff += rotationSpeed;
        }
    }

}