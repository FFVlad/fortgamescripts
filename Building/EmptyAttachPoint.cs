﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyAttachPoint : AttachPoint
{

    internal override bool IsLegalAttach(StructureBlock blockBeingAttached, RaycastHit raycastHit)
    {
        return blockBeingAttached.canBePlacedOnFrame;
    }

    internal override bool IsLegalItemPlacement(RaycastHit raycastHit)
    {
        return false;
    }

    internal override void PlaceGhostBlockOnThis(GameObject ghostBlock, RaycastHit raycastHit)
    {
        base.PlaceGhostBlockOnThis(ghostBlock, raycastHit);
        ghostBlock.transform.position = transform.position;
        ghostBlock.transform.rotation = transform.rotation;
    }

    internal override void PlaceBlockOnThis(GameObject blockToPlace)
    {
        base.PlaceBlockOnThis(blockToPlace);

        blockToPlace.transform.position = transform.position;
        blockToPlace.transform.rotation = transform.rotation;

        BlockAttachPoint blockAttach = blockToPlace.GetComponent<BlockAttachPoint>();

        if (blockAttach != null)
        {
            blockAttach.placedOnTheFrame = true;
        }

        Destroy(gameObject);
    }

}
