﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuilderModule
{
    internal Inventory currentPlayerInventory;
    internal BoatFrameDataContainer boatFrameData;

    protected GameObject activeGhostPreviewObject;
    protected AttachPoint currentlyActiveAttachPoint;
    protected bool debug = false;
    protected GameObject midpointIndicator;
    protected bool currentlyActiveModule = false;
    protected LayerMask ignoreRaycastLayer;

    internal virtual void InitializeModule(Inventory inventory, BoatFrameDataContainer frame)
    {
        this.currentPlayerInventory = inventory;
        this.currentPlayerInventory.inventoryUpdated += InventoryUpdated;
        this.boatFrameData = frame;
        ignoreRaycastLayer = ~((1 << ReferenceManager.IgnoreRaycastLayer) | (1 << ReferenceManager.KCCLayer));
    }

    internal virtual void ModuleSelected()
    {
        this.currentlyActiveModule = true;
        UpdateRelevantUI();
    }

    internal virtual void ModuleUnSelected()
    {
        this.currentlyActiveModule = false;
        ClearCurrentGhostObject();
    }

    internal virtual void DestroyModule()
    {
        if (this.currentPlayerInventory != null)
        {
            this.currentPlayerInventory.inventoryUpdated -= InventoryUpdated;
        }
    }

    internal virtual void Raycasting(Ray ray)
    {

    }

    internal virtual void ChangeSelectionForward()
    {

    }

    internal virtual void ChangeSelectionBackward()
    {

    }

    internal virtual void RotateItemForward(float speedVariable)
    {

    }

    internal virtual void RotateItemBackward(float speedVariable)
    {

    }

    internal virtual void ContinuousActionHeld()
    {

    }

    internal virtual void TryToDoAction()
    {

    }

    protected virtual void InventoryUpdated()
    {
        UpdateRelevantUI();
    }

    protected void ClearCurrentGhostObject()
    {
        if (this.activeGhostPreviewObject != null)
        {
            GameObject.Destroy(this.activeGhostPreviewObject);
            if (this.midpointIndicator)
            {
                GameObject.Destroy(this.midpointIndicator);
            }
        }
    }

    protected virtual void UpdateRelevantUI()
    {

    }

}
