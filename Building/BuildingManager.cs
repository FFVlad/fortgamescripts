﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingManager : Builder
{
    public static BuildingManager BuilderSceneInstance;

    public GameObject BuildingModeUIParent;
    public GameObject RTSCameraParent;
    public GameObject FPSCameraParent;
    public GameObject PlayerParent;
    public GameObject PlayerCameraParent;
    public GameObject framePrefab;
    private CameraMode currentCameraMode = CameraMode.FirstPerson;

    protected override void InitializeBuilder()
    {
        BuilderSceneInstance = this;

        if (GameManager.Instance.currentBoat == null)
        {
            CreateCurrentFrame(this.framePrefab);
            GameManager.Instance.currentBoat = this.currentFrameContainer.gameObject;
            DontDestroyOnLoad(GameManager.Instance.currentBoat);
        }
        else
        {
            this.currentFrameContainer = GameManager.Instance.currentBoat.GetComponent<BoatFrameDataContainer>();
        }

        base.InitializeBuilder();
        ChangeCameraMode(CameraMode.GodMode);
    }

    protected override void CheckForChangingBuildMode()
    {
        base.CheckForChangingBuildMode();

        if (Input.GetKeyDown(KeyCode.F9))
        {
            ChangeCameraMode(CameraMode.GodMode);
        }
        else if (Input.GetKeyDown(KeyCode.F10))
        {
            ChangeCameraMode(CameraMode.FirstPerson);
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            BuildingUI.BuildingInstance.ToggleCraftingWindow(this.currentPlayerInventory);
        }
    }

    internal void ChangeCameraMode(CameraMode cameraMode)
    {
        if (this.currentCameraMode == cameraMode)
        {
            return;
        }

        this.currentCameraMode = cameraMode;
        bool useRTSCamera = cameraMode == CameraMode.GodMode;

        this.RTSCameraParent.SetActive(useRTSCamera);
        if (this.FPSCameraParent != null)
        {
            this.FPSCameraParent.SetActive(!useRTSCamera);
            this.PlayerParent.SetActive(!useRTSCamera);
            this.PlayerCameraParent.SetActive(!useRTSCamera);
        }
        else
        {
            this.FPSCameraParent = FindObjectOfType<PlayerController>().gameObject;
            this.FPSCameraParent.SetActive(!useRTSCamera);
            this.PlayerParent.SetActive(!useRTSCamera);
            this.PlayerCameraParent.SetActive(!useRTSCamera);
        }

        if (useRTSCamera)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (BuildingUI.BuildingInstance)
        {
            BuildingUI.BuildingInstance.SetCameraMode(this.currentCameraMode);
        }
    }

}

public enum CameraMode
{
    GodMode,
    FirstPerson
}