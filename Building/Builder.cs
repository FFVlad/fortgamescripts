﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Builder : MonoBehaviour
{
    public static Builder Instance;

    public GameObject emptyStructurePrefab;
    public float rotationSpeed;

    public List<BuildMode> allowedBuildModes = new List<BuildMode>()
    {
        BuildMode.StructureMode, BuildMode.ItemMode, BuildMode.DeleteMode, BuildMode.InspectionMode
    };

    internal Inventory currentPlayerInventory;

    protected BuildMode currentBuildMode = BuildMode.StructureMode;
    protected BoatFrameDataContainer currentFrameContainer;

    protected Dictionary<BuildMode, BuilderModule> moduleDictionary = new Dictionary<BuildMode, BuilderModule>();

    protected BuilderModule currentModule;
    protected bool initialized = false;

    #region Unity base and setup functions

    // Start is called before the first frame update
    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            InitializeBuilder();
        }
    }

    private void Update()
    {
        if (initialized)
        {
            CheckForInputs();
        }
    }

    protected virtual void InitializeBuilder()
    {
        this.currentPlayerInventory = InventoryManager.Instance.GetPlayerInventory();

        if (GameManager.Instance.currentBoat != null)
        {
            this.currentFrameContainer = GameManager.Instance.currentBoat.GetComponent<BoatFrameDataContainer>();
        }

        if (this.currentFrameContainer == null)
        {
            Debug.LogError("No boat frame, can't initialize modules properly!");
            return;
        }

        this.moduleDictionary = new Dictionary<BuildMode, BuilderModule>();

        foreach (BuildMode buildMode in this.allowedBuildModes)
        {
            if (!this.moduleDictionary.ContainsKey(buildMode))
            {
                BuilderModule newModule = null;

                switch (buildMode)
                {
                    case BuildMode.StructureMode:
                        newModule = new StructureBuilder();
                        break;
                    case BuildMode.ItemMode:
                        newModule = new ItemBuilder();
                        break;
                    case BuildMode.DeleteMode:
                        newModule = new DeleteBuilder();
                        break;
                }

                if (newModule != null)
                {
                    newModule.InitializeModule(this.currentPlayerInventory, this.currentFrameContainer);
                }

                this.moduleDictionary.Add(buildMode, newModule);
            }
        }

        if (BuildingUI.BuildingInstance)
        {
            BuildingUI.BuildingInstance.UpdateResources(this.currentPlayerInventory);
        }

        ChangeBuildMode(this.allowedBuildModes[this.allowedBuildModes.Count - 1]);
        initialized = true;
    }

    // future proofing for when we have different frames and stuff
    protected virtual void CreateCurrentFrame(GameObject framePrefabToUse)
    {
        GameObject frameObject = Instantiate(framePrefabToUse);
        this.currentFrameContainer = frameObject.GetComponent<BoatFrameDataContainer>();
        Rigidbody rigidbody = frameObject.GetComponent<Rigidbody>();

        if (rigidbody)
        {
            rigidbody.isKinematic = true;
        }
    }

    #endregion

    #region Input handling

    protected virtual void CheckForInputs()
    {
        CheckForChangingBuildMode();

        if (PlayUI.Instance && Input.GetKeyDown(KeyCode.I))
        {
            PlayUI.Instance.TogglePlayerInventory(this.currentPlayerInventory);
        }

        if (this.currentModule == null)
        {
            return;
        }

        RaycastCheckForHits();
        CheckIfChangeSelection();
        CheckForItemRotation();
        CheckForSnapping();

        if (Input.GetMouseButton(0))
        {
            this.currentModule.ContinuousActionHeld();
        }

        if (Input.GetMouseButtonDown(0))
        {
            this.currentModule.TryToDoAction();
        }
    }

    protected virtual void RaycastCheckForHits()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        this.currentModule.Raycasting(ray);
    }

    protected virtual void CheckForItemRotation()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            this.currentModule.RotateItemForward(this.rotationSpeed);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            this.currentModule.RotateItemBackward(this.rotationSpeed);
        }
    }

    protected virtual void CheckIfChangeSelection()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            this.currentModule.ChangeSelectionForward();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            this.currentModule.ChangeSelectionBackward();
        }
    }

    protected virtual void CheckForChangingBuildMode()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            ChangeBuildMode(BuildMode.StructureMode);
        }
        else if (Input.GetKeyDown(KeyCode.F2))
        {
            ChangeBuildMode(BuildMode.ItemMode);
        }
        else if (Input.GetKeyDown(KeyCode.F3))
        {
            ChangeBuildMode(BuildMode.DeleteMode);
        }
        else if (Input.GetKeyDown(KeyCode.F4))
        {
            ChangeBuildMode(BuildMode.InspectionMode);
        }
    }

    protected virtual void CheckForSnapping()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (this.moduleDictionary.ContainsKey(BuildMode.ItemMode))
            {
                (this.moduleDictionary[BuildMode.ItemMode] as ItemBuilder).ToggleSnapping();
            }
        }
    }

    #endregion

    internal virtual void ChangeBuildMode(BuildMode changeTo)
    {
        if (this.currentBuildMode == changeTo || !this.allowedBuildModes.Contains(changeTo))
        {
            return;
        }

        this.currentBuildMode = changeTo;

        if (this.currentModule != null)
        {
            this.currentModule.ModuleUnSelected();
        }

        this.currentModule = this.moduleDictionary[this.currentBuildMode];

        if (PlayUI.Instance)
        {
            PlayUI.Instance.ChangeSelectedThingText("");
            PlayUI.Instance.SetSnappingParent(false);
            PlayUI.Instance.SetItemDataParent(false);
            PlayUI.Instance.ChangeSnappingMode("");
            PlayUI.Instance.SetBuildingMode(this.currentBuildMode);
        }

        if (BuildingUI.BuildingInstance)
        {
            BuildingUI.BuildingInstance.UpdateResourcesChangeAmounts(new List<InventoryResource>());
        }

        if (this.currentModule != null)
        {
            this.currentModule.ModuleSelected();
        }
    }

    internal virtual void ReplaceDeletedStructureWithEmptyFrameObject(BlockAttachPoint attachPoint)
    {
        GameObject newEmptyFramePart = Instantiate(this.emptyStructurePrefab);
        newEmptyFramePart.transform.position = attachPoint.transform.position;
        newEmptyFramePart.transform.rotation = attachPoint.transform.rotation;
        newEmptyFramePart.transform.parent = this.currentFrameContainer.emptyAttachPointsParent;
    }

}

public enum BuildMode
{
    StructureMode = 0,
    ItemMode = 1,
    InspectionMode = 2,
    DeleteMode = 10
}