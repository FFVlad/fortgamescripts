﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemPlaceable
{
    bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle);
    void UnattachItem(Item item);
    Transform GetTransform();
    Vector3 GetPlacingPosition();
    List<Item> GetAttachedItems();
    void AttachItem(Item itemToAttach);

}
