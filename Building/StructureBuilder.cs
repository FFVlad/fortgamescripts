﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StructureBuilder : BuilderModule
{
    private float lastPlacementTime = 0;

    internal override void ModuleSelected()
    {
        base.ModuleSelected();
        if (PlayUI.Instance)
        {
            PlayUI.Instance.SetItemDataParent(true);
        }
    }

    internal override void Raycasting(Ray ray)
    {
        base.Raycasting(ray);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, ignoreRaycastLayer) && !EventSystem.current.IsPointerOverGameObject())
        {
            GameObject objectHit = hit.collider.transform.gameObject;

            if (objectHit != null)
            {
                AttachPoint attachPoint = objectHit.GetComponent<AttachPoint>();

                if (attachPoint != null && attachPoint.IsLegalAttach(this.currentPlayerInventory.equippedStructureBlock, hit))
                {
                    AttachPointHoveredOver(attachPoint, hit);
                }
                else
                {
                    ClearCurrentAttachPoint();
                }
            }
            else
            {
                ClearCurrentAttachPoint();
            }
        }
        else
        {
            ClearCurrentAttachPoint();
        }
    }

    internal override void ContinuousActionHeld()
    {
        if (Time.timeSinceLevelLoad - this.lastPlacementTime < 0.2f)
        {
            return;
        }

        base.ContinuousActionHeld();

        if (this.currentlyActiveAttachPoint && this.currentPlayerInventory.CanBuildCurrentBlock())
        {
            GameObject newBlock = GameObject.Instantiate(this.currentPlayerInventory.equippedStructureBlock.blockPrefab);
            this.currentlyActiveAttachPoint.PlaceBlockOnThis(newBlock);
            newBlock.transform.parent = this.boatFrameData.shipPieceParent;
            this.currentlyActiveAttachPoint = null;
            this.currentPlayerInventory.BuiltCurrentBlock();
            this.lastPlacementTime = Time.timeSinceLevelLoad;
        }
    }

    internal override void ChangeSelectionForward()
    {
        base.ChangeSelectionForward();

        this.currentPlayerInventory.EquipNextBlock();
        ClearCurrentGhostObject();
    }

    internal override void ChangeSelectionBackward()
    {
        base.ChangeSelectionBackward();

        this.currentPlayerInventory.EquipPreviousBlock();
        ClearCurrentGhostObject();
    }

    protected override void UpdateRelevantUI()
    {
        base.UpdateRelevantUI();

        if (PlayUI.Instance && this.currentlyActiveModule)
        {
            PlayUI.Instance.ChangeSelectedThingText(this.currentPlayerInventory.equippedStructureBlock.VisibleName);
        }

        if (BuildingUI.BuildingInstance && this.currentlyActiveModule)
        {
            BuildingUI.BuildingInstance.UpdateResources(this.currentPlayerInventory);
            BuildingUI.BuildingInstance.UpdateResourcesChangeAmounts(this.currentPlayerInventory.equippedStructureBlock.resourcesRequired);
        }
    }

    private void AttachPointHoveredOver(AttachPoint attachPoint, RaycastHit raycastHit)
    {
        if (attachPoint != null)
        {
            if (attachPoint != this.currentlyActiveAttachPoint)
            {
                AttachPointNotTarget(this.currentlyActiveAttachPoint);
                this.currentlyActiveAttachPoint = attachPoint;

                if (this.activeGhostPreviewObject != null)
                {
                    GameObject.Destroy(this.activeGhostPreviewObject);
                }

                this.activeGhostPreviewObject = GameObject.Instantiate(this.currentPlayerInventory.equippedStructureBlock.ghostPrefab);

                this.currentlyActiveAttachPoint.PlaceGhostBlockOnThis(this.activeGhostPreviewObject, raycastHit);
            }
            else
            {
                if (this.activeGhostPreviewObject == null)
                {
                    this.activeGhostPreviewObject = GameObject.Instantiate(this.currentPlayerInventory.equippedStructureBlock.ghostPrefab);
                }

                this.currentlyActiveAttachPoint.PlaceGhostBlockOnThis(this.activeGhostPreviewObject, raycastHit);
            }
        }
    }

    private void AttachPointNotTarget(AttachPoint attachPoint)
    {
        if (this.currentlyActiveAttachPoint == attachPoint)
        {
            ClearCurrentAttachPoint();
        }
    }

    private void ClearCurrentAttachPoint()
    {
        this.currentlyActiveAttachPoint = null;
        ClearCurrentGhostObject();
    }

}
