﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostCollisionChecker : MonoBehaviour
{
    public List<Renderer> renderersToChangeMatsFor = new List<Renderer>();
    public Material normalMaterial;
    public Material failedMaterial;

    private List<Collider> collidingWith = new List<Collider>();

    private void OnDisable()
    {
        collidingWith.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        collidingWith.Add(other);
        CheckForMaterialChange();
    }

    private void OnTriggerExit(Collider other)
    {
        collidingWith.Remove(other);
        CheckForMaterialChange();
    }

    internal bool CheckIfCurrentlyLegal()
    {
        return collidingWith.Count == 0;
    }

    private void CheckForMaterialChange()
    {
        if (CheckIfCurrentlyLegal())
        {
            ChangeRenderers(normalMaterial);
        }
        else
        {
            ChangeRenderers(failedMaterial);
        }
    }

    private void ChangeRenderers(Material changeMat)
    {
        foreach (var rendererToChange in this.renderersToChangeMatsFor)
        {
            rendererToChange.material = changeMat;
        }
        
    }
}
