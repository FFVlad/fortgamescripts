﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachPoint : MonoBehaviour
{

    protected virtual void AttachPointSelected()
    {

    }

    protected virtual void AttachPointDeSelected()
    {

    }

    internal virtual bool IsLegalAttach(StructureBlock blockBeingAttached, RaycastHit raycastHit)
    {
        return true;
    }

    internal virtual bool IsLegalItemPlacement(RaycastHit raycastHit)
    {
        return true;
    }

    internal virtual Vector3 GetClosestSnapPoint(ItemData data, RaycastHit raycastHit)
    {
        return transform.position;
    }

    internal virtual void PlaceGhostBlockOnThis(GameObject ghostBlock, RaycastHit raycastHit)
    {

    }

    internal virtual void PlaceBlockOnThis(GameObject blockToPlace)
    {

    }

}

public enum PlacementDirection
{
    Top = 0,
    Bottom = 1,
    Right = 2,
    Left = 3,
    Forward = 4,
    Back = 5
}

public enum SnappingStyle
{
    Wall = 0,
    Center = 1,
    NoRotation = 2,
    Drop = 3,
    RotationOffset = 4
}
