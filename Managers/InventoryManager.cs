﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager Instance;

    public int defaultInventoryID = 0;

    private Dictionary<int, Inventory> playerInventories = new Dictionary<int, Inventory>();

    private Dictionary<int, InventoryData> _inventoryDictionary;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            this._inventoryDictionary = LogicUtils.FillDictionary("Inventories", (InventoryData data) => data.inventoryID);
        }
    }

    internal Inventory GetPlayerInventory(int playerID = 0)
    {
        if (!playerInventories.ContainsKey(playerID))
        {
            AddNewPlayerInventory(playerID);
        }

        return playerInventories[playerID];
    }

    internal InventoryData GetInventoryWithID(int id)
    {
        if (this._inventoryDictionary.ContainsKey(id))
        {
            return this._inventoryDictionary[id];
        }

        return null;
    }

    private void AddNewPlayerInventory(int playerID)
    {
        InventoryData data = null;

        if (DebugManager.Instance.everythingUnlocked)
        {
            data = DataManager.Instance.GetFullUnlockedInventory();
        }
        else if (DebugManager.Instance.useDebugInventory)
        {
            data = GetInventoryWithID(DebugManager.Instance.debugInventoryID);
        }
        else
        {
            data = GetInventoryWithID(defaultInventoryID);
        }

        Inventory inventory = new Inventory();
        inventory.InitializeInventory(data);

        playerInventories.Add(playerID, inventory);
    }

}
