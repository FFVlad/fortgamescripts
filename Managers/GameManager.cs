﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public string SeaSceneString;
    public string BuildModeString;
    public string[] IslandStrings;
    public PlayMode currentPlayMode = PlayMode.BuildMode;

    public GameObject defaultBoat;
    public GameObject currentBoat;
    public GameObject orphanShipPrefab;

    //Sea and Weather management
    public float seaAmount = 0.5f;
    private bool _isSeaCalming;
    private bool _isSeaMaddening;
    private bool _isPlayerCloseToAnIsland;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            SceneManager.sceneLoaded += NewSceneWhoDis;
        }
    }

    private void Update()
    {
        HandleWeather();
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            SceneManager.sceneLoaded -= NewSceneWhoDis;
        }
    }

    internal void ChangeToDefaultBoat()
    {
        if (currentBoat)
        {
            Destroy(currentBoat);
        }

        currentBoat = Instantiate(defaultBoat);
       
        DontDestroyOnLoad(currentBoat);
    }

    internal void ChangePlayMode(PlayMode changeTo)
    {
        if (currentPlayMode == changeTo)
        {
            return;
        }

        currentPlayMode = changeTo;

        switch (currentPlayMode)
        {
            case PlayMode.PlayMode:
                SceneManager.LoadScene(SeaSceneString, LoadSceneMode.Single);
                foreach(string scene in IslandStrings)
                {
                    SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
                    Debug.Log("Loaded scene " + scene);
                }

                StartCoroutine(BuildUpWaves());
                break;
        }
    }

    IEnumerator BuildUpWaves()
    {

        float waveIncreaseSpeed = 0.01f;
        WaitForSeconds wait = new WaitForSeconds(0.1f);
        Crest.ShapeGerstnerBatched seaShape = null;
        while (seaShape == null)
        {
            seaShape = GameObject.FindObjectOfType<Crest.ShapeGerstnerBatched>();
            if (seaShape == null)
            {
                yield return wait;
            }
        }

        float targetWaveAmount = seaAmount;
        float currentWaveAmount = 0f;
        seaShape._weight = 0f;
        if (DebugManager.Instance.wavesDisabled)
        {
            yield break;
        }
        //wait a while before starting the waves.
        yield return new WaitForSeconds(2f);

        while (GameManager.Instance.currentBoat == null)
        {
            yield return wait;
        }

        while (currentWaveAmount < targetWaveAmount)
        {
            currentWaveAmount += waveIncreaseSpeed;
            seaShape._weight = currentWaveAmount;
            yield return wait;
        }
        seaShape._weight = targetWaveAmount;
        Debug.Log("waves are now fully active, weight:" + seaShape._weight);
    }

    internal void NewSceneWhoDis(Scene scene, LoadSceneMode mode)
    {
        if (currentPlayMode == PlayMode.PlayMode)
        {
            Rigidbody rigidbody = currentBoat?.GetComponent<Rigidbody>();

            if (rigidbody)
            {
                rigidbody.position = new Vector3(rigidbody.position.x, 1f, rigidbody.position.z);
                rigidbody.isKinematic = false;
                rigidbody.velocity = Vector3.zero;
                rigidbody.angularVelocity = Vector3.zero;
            }

            BoatFrameDataContainer boatData = currentBoat?.GetComponent<BoatFrameDataContainer>();

            if (boatData)
            {
                boatData.CheckShipIntegrity();
            }
        }
    }


    /* TODO:
     * Do we want a weather manager?
     * Could do a weather manager.
     */
    private void HandleWeather()
    {
        //Checks if player IS in player, over 10 seconds have passed and player is NOT close to an island
        if (currentPlayMode == PlayMode.PlayMode && Time.timeSinceLevelLoad > 10f && !_isPlayerCloseToAnIsland)
        {
            SetWaveWeight(GetWaveWeight() + Random.Range(-0.05f, 0.05f) * Time.deltaTime);
            RandomlyMakeSeaCalmOrMad();
            CheckIfSeaCalmOrMad();
        }
    }

    private void RandomlyMakeSeaCalmOrMad()
    {
        if(!_isSeaCalming && !_isSeaMaddening)
        {
            if (Random.Range(0f, 100f) < 0.01f)
            {
                Debug.Log("Sea will be calmed down randomly");
                StartCoroutine(CalmSeaDown());
            }
            else if(Random.Range(0f, 100f) > 99.99f)
            {
                Debug.Log("Sea WILL GO MAD randomly");
                StartCoroutine(MakeSeaMad());
            }
        }
    }

    private void CheckIfSeaCalmOrMad()
    {
        if (GetWaveWeight() > 0.25f)
        {
            StartCoroutine(CalmSeaDown());
        }
        else if (GetWaveWeight() < 0.01f)
        {
            StartCoroutine(MakeSeaMad());
        }
    }
    internal float GetWaveWeight()
    {
        if (ReferenceManager.Waves)
        {
            return ReferenceManager.Waves._weight;
        }
        return 0;
    }

    internal void SetWaveWeight(float wantedWeight)
    {
        wantedWeight = Mathf.Clamp01(wantedWeight);
        if (ReferenceManager.Waves)
        {
            ReferenceManager.Waves._weight = wantedWeight;
        }
    }

    private IEnumerator CalmSeaDown()
    {
        if (_isSeaCalming)
        {
            yield break;
        }
        _isSeaCalming = true;
        while (GetWaveWeight() > 0.03f)
        {
            SetWaveWeight(GetWaveWeight() - 0.0005f);
            yield return new WaitForEndOfFrame();
        }
        _isSeaCalming = false;
    }

    private IEnumerator MakeSeaMad()
    {
        if (_isSeaMaddening)
        {
            yield break;
        }
        _isSeaMaddening = true;
        while (GetWaveWeight() < 0.20f && !_isPlayerCloseToAnIsland)
        {
            SetWaveWeight(GetWaveWeight() + 0.0005f);
            yield return new WaitForEndOfFrame();
        }
        _isSeaMaddening = false;
    }

    internal void SetPlayerEnterIslandProximity(bool enter)
    {
        _isPlayerCloseToAnIsland = enter;
        if (enter)
        {
            StartCoroutine(CalmSeaDown());
        }
    }

}

public enum PlayMode
{
    BuildMode = 0,
    PlayMode = 1,
    Menu = 2
}
