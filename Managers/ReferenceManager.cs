﻿using System.Collections;
using System.Collections.Generic;
using Crest;
using UnityEngine;

public class ReferenceManager : MonoBehaviour
{
    private static ReferenceManager _instance;
    internal static ReferenceManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        CreateSingleton();
    }

    private bool CreateSingleton()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return false;
        }
        if (!transform.parent)
        {
            DontDestroyOnLoad(gameObject);
        }

        _instance = this;
        return true;
    }

    private static PlayerController _playerController;
    internal static PlayerController playerController
    {
        get
        {
            if (_playerController == null)
            {
                _playerController = FindObjectOfType<PlayerController>();
            }
            return _playerController;
        }
    }

    private static VesselBase _playerVessel;
    internal static VesselBase PlayerVessel
    {
        get
        {
            if(_playerVessel == null)
            {
                _playerVessel = FindObjectOfType<VesselBase>();
            }
            return _playerVessel;
        }
    }

    private static GameObject _sea;
    internal static GameObject Sea
    {
        get
        {
            /*TODO: on a scale of toni to niko this is a solid arne, pls fix*/
            if (_sea == null)
            {
                _sea = FindObjectOfType<OceanRenderer>().transform.root.gameObject;
            }
            return _sea;
        }
    }

    private static ShapeGerstnerBatched _waves;
    internal static ShapeGerstnerBatched Waves
    {
        get
        {
            if (_waves == null)
            {
                _waves = FindObjectOfType<ShapeGerstnerBatched>();
            }
            return _waves;
        }
    }


    public Material interactionMaterial;
    public static int ItemsLayer => LayerMask.NameToLayer("Items");
    public static int KCCLayer => LayerMask.NameToLayer("KCCVessel");
    public static int IgnoreRaycastLayer => LayerMask.NameToLayer("Ignore Raycast");
    public static int DetachedItemsLayer => LayerMask.NameToLayer("ItemsNotPlaced");
    public static int EnemiesLayer => LayerMask.NameToLayer("Enemies");
}
