﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterestManager : MonoBehaviour
{
    public static PointOfInterestManager Instance;
    public List<string> POIPoolManagerReferences;
    public List<PointOfInterestBase> pointOfInterests = new List<PointOfInterestBase>();

    internal PointOfInterestBase currentGoal;

    public GameObject goalOfTheGame;
    [Tooltip("Percentage chance of spawning a Point Of Interest every POI Spawn Attempt Interval")]
    public int POISpawnChance = 5;

    [Tooltip("Maximum amount of POIs at any given time")]
    public int POIMax = 5;

    [Tooltip("Time in seconds between each spawn attempt")]
    public float POISpawnAttemptInterval = 5f;

    [Tooltip("Distance of how far POIs spawn in Unity units")]
    public float POISpawnDistance = 1500f;

    int currentSpawnIndex = 0;
    private float _currentSpawnTime;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        Invoke("SpawnGoal", 1f);
    }

    private void SpawnGoal()
    {
        goalOfTheGame = Instantiate(goalOfTheGame);
        currentGoal = goalOfTheGame.GetComponent<PointOfInterestBase>();
        InitializeSpawnedPOI(goalOfTheGame);
    }

    bool logged = false;
    private void Update()
    {
        if (ValidateSpawnConditions())
        {
            AttemptSpawn();
        }

        if(pointOfInterests.Count >= POIMax && !logged)
        {
            logged = true;
            //Debug.LogError("Max amount of POIs spawned in " + Time.timeSinceLevelLoad);
        }
    }

    private bool ValidateSpawnConditions()
    {
        if (pointOfInterests.Count < POIMax)
        {
            _currentSpawnTime += Time.deltaTime;
            if (_currentSpawnTime > POISpawnAttemptInterval)
            {
                _currentSpawnTime = 0f;
                return true;
            }
        }
        return false;
    }

    private void AttemptSpawn()
    {
        if (Random.Range(0f, 100f) < POISpawnChance)
        {
            CreateNewPOI();
        }
    }

    private void CreateNewPOI()
    {
        if (PoolManager.Instance == null)
        {
            return;
        }
        int islandToSpawnIndex = Random.Range(0, POIPoolManagerReferences.Count);
        GameObject POIObject = PoolManager.Instance.GetPooledObject(POIPoolManagerReferences[islandToSpawnIndex]);

        if(POIObject == null)
        {
            return;
        }
        InitializeSpawnedPOI(POIObject);

    }

    private void InitializeSpawnedPOI(GameObject POIToSpawn)
    {
        PointOfInterestBase POItoAdd = POIToSpawn.GetComponent<PointOfInterestBase>();
        pointOfInterests.Add(POItoAdd);

        //Randomize position 

        Vector3 POISpawnPosition = SelectSpawnLocation();
        POISpawnPosition.x += Random.Range(0, 200f);
        POISpawnPosition.z += Random.Range(0, 200f);
        POItoAdd.transform.position = POISpawnPosition;
    }

    private Vector3 SelectSpawnLocation()
    {

        Vector3 spawnPosition = Vector3.zero;
        if(currentSpawnIndex == 0)
        {
            spawnPosition = ReferenceManager.PlayerVessel.transform.forward * POISpawnDistance;

        }
        else if(currentSpawnIndex == 1)
        {
            spawnPosition = ReferenceManager.PlayerVessel.transform.right * POISpawnDistance;
        }
        else if(currentSpawnIndex == 2)
        {
            spawnPosition =  -ReferenceManager.PlayerVessel.transform.right * POISpawnDistance;
            currentSpawnIndex = -1;
        }
        currentSpawnIndex++;
        spawnPosition.y = 0;
        return spawnPosition;
    }

    internal void RemovePOI(PointOfInterestBase POItoRemove)
    {
        pointOfInterests.Remove(POItoRemove);

        PoolManager.Instance.ReturnObjectToPool(POItoRemove.POItype, POItoRemove.gameObject);
    }
}
