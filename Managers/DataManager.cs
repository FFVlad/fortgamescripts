﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;

    private Dictionary<int, StructureBlock> _blockDictionary;
    private Dictionary<int, ItemData> _itemDictionary;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            InitializeData();
        }
    }

    private void InitializeData()
    {
        this._blockDictionary = LogicUtils.FillDictionary("Blocks", (StructureBlock data) => data.blockID);
        this._itemDictionary = LogicUtils.FillDictionary("Items", (ItemData data) => data.itemID);
    }

    internal InventoryData GetFullUnlockedInventory()
    {
        InventoryData data = InventoryData.CreateInstance<InventoryData>();

        foreach (StructureBlock structureBlock in this._blockDictionary.Values)
        {
            data.blocksInInventory.Add(structureBlock.blockID);
        }

        foreach (ItemData item in this._itemDictionary.Values)
        {
            data.itemsInInventory.Add(new InventoryItemData() { itemID = item.itemID, hasInfinite = true });
        }

        data.inventoryResources.Add(new InventoryResource() { rawResource = RawResource.Wood, amount = 50000 });
        data.inventoryResources.Add(new InventoryResource() { rawResource = RawResource.Metal, amount = 50000 });
        data.inventoryResources.Add(new InventoryResource() { rawResource = RawResource.Plastic, amount = 50000 });
        data.inventoryResources.Add(new InventoryResource() { rawResource = RawResource.Components, amount = 50000 });

        return data;
    }

    internal StructureBlock GetBlockWithID(int id)
    {
        if (this._blockDictionary.ContainsKey(id))
        {
            return this._blockDictionary[id];
        }

        return null;
    }

    internal ItemData GetItemWithID(int id)
    {
        if (this._itemDictionary.ContainsKey(id))
        {
            return this._itemDictionary[id];
        }

        return null;
    }

    internal bool CheckIfItemExists(int id)
    {
        return this._itemDictionary.ContainsKey(id);
    }

}
