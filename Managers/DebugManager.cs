﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : MonoBehaviour
{
    public static DebugManager Instance;
    
    public bool infiniteResources = false;
    public bool everythingUnlocked = false;
    public bool wavesDisabled = false;

    public bool useDebugInventory = false;
    public int debugInventoryID = -1;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

}
