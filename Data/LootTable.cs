﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Lootz", menuName = "LIB/Inventory/Loot table", order = 1)]
public class LootTable : ScriptableObject
{
    public int lootTableID;
    
    [Header("FIRST INDEX IS CHANCE TO DROP 0 ITEMS")]
    public List<float> chanceToGetAmountOfItems = new List<float>();
    public List<LootItemData> lootTable = new List<LootItemData>();
    private List<LootItemData> _itemsNotToDrop = new List<LootItemData>();

    internal void GenerateLootDropsFromTable()
    {
        if (this.chanceToGetAmountOfItems.Count <= 1 || this.lootTable.Count == 0)
        {
            return;
        }

        float totalChance = 0f;
        foreach (float chance in this.chanceToGetAmountOfItems)
        {
            totalChance += chance;
        }

        if (totalChance <= 0)
        {
            return;
        }

        float randomNumber = Random.Range(0, totalChance);
        float currentlyAt = 0;
        int currentIndex = 0;
        int winnerIndex = -1;

        foreach (float chance in this.chanceToGetAmountOfItems)
        {
            float maxLimitOfGettingIt = currentlyAt + chance;

            if (randomNumber >= currentlyAt && randomNumber < maxLimitOfGettingIt)
            {
                winnerIndex = currentIndex;
                break;
            }

            currentIndex++;
            currentlyAt = maxLimitOfGettingIt;
        }

        if (winnerIndex <= 0) // the first chance is for no loot, so quit
        {
            return;
        }

        for (int i = 0; i < winnerIndex; i++)
        {
            LootItemData lootItem = GetRandomItemFromLootTable();

            if (lootItem != null)
            {
                lootItem.GetLootFromThis(this);
            }
        }
    }


    internal List<LootItemData> GenerateLootDropListFromTable()
    {
        if (this.chanceToGetAmountOfItems.Count <= 1 || this.lootTable.Count == 0)
        {
            return null;
        }

        this._itemsNotToDrop.Clear();

        float totalChance = 0f;
        foreach (float chance in this.chanceToGetAmountOfItems)
        {
            totalChance += chance;
        }

        if (totalChance <= 0)
        {
            return null;
        }

        float randomNumber = Random.Range(0, totalChance);
        float currentlyAt = 0;
        int currentIndex = 0;
        int winnerIndex = -1;

        foreach (float chance in this.chanceToGetAmountOfItems)
        {
            float maxLimitOfGettingIt = currentlyAt + chance;

            if (randomNumber >= currentlyAt && randomNumber < maxLimitOfGettingIt)
            {
                winnerIndex = currentIndex;
                break;
            }

            currentIndex++;
            currentlyAt = maxLimitOfGettingIt;
        }

        if (winnerIndex <= 0) // the first chance is for no loot, so quit
        {
            return null;
        }

        List<LootItemData> generatedLootItems = new List<LootItemData>();
        AddGuaranteedItems(generatedLootItems);

        for (int i = 0; i < winnerIndex; i++)
        {
            LootItemData generatedLootItem = GetRandomItemFromLootTable();

            if (generatedLootItem != null)
            {
                generatedLootItems.Add(generatedLootItem);
            }
        }

        return generatedLootItems;
    }

    private LootItemData GetRandomItemFromLootTable()
    {
        float totalChance = 0f;

        List<LootItemData> possibleLoot = new List<LootItemData>(this.lootTable);

        possibleLoot = DeleteDroppedUniqueItems(possibleLoot);
        
        foreach (LootItemData lootItem in possibleLoot)
        {
            totalChance += lootItem.chanceToGet;
        }

        if (totalChance <= 0)
        {
            return null;
        }

        float randomNumber = Random.Range(0, totalChance);
        float currentlyAt = 0;
        int currentIndex = 0;
        int winnerIndex = -1;

        foreach (LootItemData lootItem in possibleLoot)
        {
            float maxLimitOfGettingIt = currentlyAt + lootItem.chanceToGet;

            if (randomNumber >= currentlyAt && randomNumber < maxLimitOfGettingIt)
            {
                winnerIndex = currentIndex;
                break;
            }

            currentIndex++;
            currentlyAt = maxLimitOfGettingIt;
        }

        if (winnerIndex < 0)
        {
            Debug.LogError("Something went wrong when trying to get a random item from loot table!");
            return null;
        }

        LootItemData itemData = possibleLoot[winnerIndex];

        if (itemData.typeofLoot == LootType.Nothing)
        {
            return null;
        }

        return itemData;
    }

    private List<LootItemData> DeleteDroppedUniqueItems(List<LootItemData> possibleLoot)
    {
        return possibleLoot.Except(this._itemsNotToDrop).ToList();
    }

    private void AddGuaranteedItems(List<LootItemData> generatedLootItems)
    {
        for (int i = 0; i < lootTable.Count; i++)
        {
            if (this.lootTable[i].guaranteedDrop)
            {
                this.lootTable[i].AddToList(generatedLootItems, this);
            }
        }
    }

    internal void RemoveFromList(LootItemData itemData)
    {
        this._itemsNotToDrop.Add(itemData);
    }

}

[System.Serializable]
public class LootItemData
{
    public float chanceToGet = 0;
    public LootType typeofLoot = LootType.Nothing;
    public string objectName = string.Empty;

    public int itemIDToGet = 0;
    
    public int minAmount = 1;
    public int maxAmount = 1;
    
    public bool guaranteedDrop;
    public bool uniqueDrop;

    public bool HasObject
    {
        get { return false; }

    }

    public string PooledObjectName
    {
        get
        {
            if (!HasObject)
            {
                return null;
            }

            if (string.IsNullOrEmpty(this.ObjectName))
            {
                return "";
            }

            return this.ObjectName;

        }
    }

    public string ObjectName
    {
        get
        {
            return "";
            //UsableItem item = InventoryManager.instance.GetUsableItemWithID(this.itemIDToGet);
            
            //return item ? item.prefabName : this.objectName;
        }
        set { this.objectName = value; }
    }

    public void GetLootFromThis(LootTable parentTable = null)
    {
        //PlayerInventory playerInventory = ReferenceManager.GetPlayerInventory();

        //switch (this.typeofLoot)
        //{
        //    case LootType.Money:
        //        playerInventory.ChangeCurrencyInInventory(Mathf.RoundToInt(Random.Range(this.minAmount, this.maxAmount)));
        //        break;
        //    case LootType.ExecutionMoney:
        //        playerInventory.ChangeCurrencyInInventory(Mathf.RoundToInt(Random.Range(this.minAmount, this.maxAmount)));
        //        break;
        //    case LootType.Bullets:
        //        int value = 1;

        //        if (playerInventory.currentlyEquippedWeapon.IsWeaponWithClip())
        //        {
        //            value = playerInventory.currentlyEquippedWeapon.GetCurrentWeaponData().clipSize;
        //        }

        //        playerInventory.AddBullets(Mathf.RoundToInt(Random.Range(this.minAmount, this.maxAmount) * value));
        //        break;
        //    case LootType.Key:
        //        ReferenceManager.GetPlayer().Say("FoundKey",true);
        //        ReferenceManager.GetPlayerInventory().gotKey = true;
        //        break;
        //    case LootType.Item:
        //        playerInventory.GainUsableItem(itemIDToGet, Random.Range(this.minAmount, this.maxAmount));
        //        break;
        //}
    }

    public void AddToList(List<LootItemData> listToAddTo, LootTable parentTable)
    {
        listToAddTo.Add(this);
        if (this.uniqueDrop && parentTable != null)
        {
            parentTable.RemoveFromList(this);
        }
    }
    
    public LootItemData shallowCopy()
    {
        return (LootItemData)MemberwiseClone();
    }
}

public enum LootType
{
    Money = 0,
    ExecutionMoney = 1,
    Bullets = 2,
    Item = 3,
    Key = 4,
    Nothing = 999
}