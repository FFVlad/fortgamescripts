﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StructurePiece", menuName = "LIB/Building/StructurePiece", order = 1)]
public class StructureBlock : ScriptableObject
{
    public int blockID;
    public string VisibleName;
    public GameObject blockPrefab;
    public GameObject ghostPrefab;
    public bool canBePlacedOnFrame = true;
    public List<InventoryResource> resourcesRequired = new List<InventoryResource>();

    internal bool HasEnoughResourcesToBuild(Inventory inventory)
    {
        foreach (InventoryResource resource in this.resourcesRequired)
        {
            if (inventory.GetRawResourceAmount(resource.rawResource) < resource.amount)
            {
                return false;
            }
        }

        return true;
    }

}
