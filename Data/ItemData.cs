﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "LIB/Inventory/Item", order = 1)]
public class ItemData : ScriptableObject
{
    public int itemID;
    public string VisibleName;
    public Sprite itemSprite;
    public GameObject itemPlacingPrefab;
    public GameObject ghostPrefab;
    public SnappingStyle snappingStyle;
    public Vector3 itemHeightDiff;
    public float itemSize = 0;
    public float itemHeight;
    public Vector3 rotationAxis1;
    public Vector3 rotationAxis2;
    public List<InventoryResource> resourcesRequired = new List<InventoryResource>();

    internal bool HasEnoughResourcesToCraft(Inventory inventory)
    {
        foreach (InventoryResource resource in this.resourcesRequired)
        {
            if (inventory.GetRawResourceAmount(resource.rawResource) < resource.amount)
            {
                return false;
            }
        }

        return true;
    }

}
