﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandBase : MonoBehaviour
{
    private float _distanceToPlayer = 0;
    private PlayerController _player;
    private bool _isPlayerInProximity;
    // Update is called once per frame
    private void Start()
    {
        //No need to update distance every frame for far away islands, do this instead
        InvokeRepeating(nameof(UpdateDistance), 0, 2f);

        _player = ReferenceManager.playerController;
    }

    void Update()
    {
        if(_distanceToPlayer > 200)
        {
            if (_isPlayerInProximity)
            {
                UnloadStuffFromIsland();
                GameManager.Instance.SetPlayerEnterIslandProximity(false);
                _isPlayerInProximity = false;
                LoadStuffToIsland();
            }
            _isPlayerInProximity = false;
        }
        else
        {
            //Debug.Log("Player is close! " + _distanceToPlayer + "u away from island");
            _distanceToPlayer = (this.transform.position - _player.transform.position).magnitude;
            if (!_isPlayerInProximity)
            {
                GameManager.Instance.SetPlayerEnterIslandProximity(true);
                _isPlayerInProximity = true;
                LoadStuffToIsland();
            }
        }
    }

    private void UpdateDistance()
    {
        if (_distanceToPlayer > 200)
        {
            _distanceToPlayer = (this.transform.position - _player.transform.position).magnitude;
            //Debug.Log("Player is far. " + _distanceToPlayer + "u away from island");
        }
    }

    private void LoadStuffToIsland()
    {
        //Debug.LogError("Here we can start instantiating and creating shizles to the island we are approaching");
    }


    private void UnloadStuffFromIsland()
    {
        //Debug.LogError("Here we can start removing shizles from the island we are leaving");
    }
}
