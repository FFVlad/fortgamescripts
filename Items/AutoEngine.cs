﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoEngine : Engine, IAutomated
{
    private float energyLevel;

    public override float GetCurrentSpeed()
    {
        return base.GetCurrentSpeed() * Mathf.Clamp(GetEnergyLevel()/GetEnergyThreshold(), 0f, 1f);
    }

    public void AutomatedAction()
    {}
    
    public void ChangeEnergyLevel(float newEnergyLevel)
    {
        this.energyLevel = newEnergyLevel;
    }

    public float GetEnergyLevel()
    {
        return this.energyLevel;
    }

    public float GetEnergyThreshold()
    {
        return 1;
    }
}
