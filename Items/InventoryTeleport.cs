﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryTeleport : StandItem
{
    public BoxCollider collisionArea;
    public Transform teleportPosition;

    internal void TryToTeleportItem(Inventory playerInventory, ItemData itemData)
    {
        Collider[] things = new Collider[20];

        int amount = ItemUtils.AmountOfItemsInBoxCollider(collisionArea, things);

        if (amount == 0)
        {
            CreateItem(playerInventory, itemData);
        }
    }

    protected override bool CheckAttachedItems(Item item, SnappingStyle snappingStyle, out bool tryToPlaceItemOnThis)
    {
        if (item is IHolder)
        {
            if (this._attachedItems.Count > 0)
            {
                foreach (var attachedItem in this._attachedItems)
                {
                    IItemPlaceable itemPlaceable = attachedItem as IItemPlaceable;
                    if (itemPlaceable != null && itemPlaceable.TryToPlaceItemOnThis(item, snappingStyle))
                    {
                        tryToPlaceItemOnThis = true;
                        return true;
                    }
                }
            }

            PlacementHelper.PlaceItem(item, snappingStyle, teleportPosition.position - transform.position, this);
            tryToPlaceItemOnThis = true;
            return true;
        }

        tryToPlaceItemOnThis = false;
        return false;
    }

    public override bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle)
    {
        if (snappingStyle == SnappingStyle.Drop)
        {
            return false;
        }

        Vector3 offset = new Vector3();
        PlacementHelper.PlaceItem(item, snappingStyle, offset, this);

        return true;
    }

    public override Vector3 GetPlacingPosition()
    {
        return teleportPosition.position;
    }

    private void CreateItem(Inventory playerInventory, ItemData itemData)
    {
        GameObject newItem = Instantiate(itemData.itemPlacingPrefab);
        newItem.transform.position = this.teleportPosition.position;
        newItem.transform.rotation = this.teleportPosition.rotation;
        //newItem.transform.parent = this.boatFrameData.shipPieceParent;

        Item[] itemScripts = newItem.GetComponentsInChildren<Item>();
        playerInventory.PlacedCurrentItem();

        if (itemScripts != null)
        {
            foreach (var item in itemScripts)
            {
                PickupableComponent pickupable = item.GetItemComponentOfType<PickupableComponent>();

                if (pickupable != null)
                {
                    pickupable.TryToParentThis();
                }
            }
        }
    }

}
