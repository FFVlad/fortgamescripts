﻿using UnityEngine;

public class SteeringItem : LockingItem, IShipItem
{
    private DestructibleComponent _destructibleComponent;
    private PickupableComponent _pickUpComponent;
    private RotatingComponent _rotatingComponent;
    private ShootableComponent _shootableComponent;

    private SteeringComponent _steeringComponent;

    [Tooltip("How much is the maximum steer, 10 is quite ok, 20 is really good")]
    public float _steerLimit;

    [Tooltip("How fast the steering wheel rotates, 5 is quite ok, 10 is really good")]
    public float steerToChange;

    public void AddToShip(VesselBase ship)
    {
        ship.AddToInventory(this);
    }

    public void RemoveFromShip(VesselBase ship)
    {
        ship.RemoveFromInventory(this);
    }


    protected override void CreateComponents()
    {
        this._steeringComponent = AddItemComponent<SteeringComponent>(new SteeringComponent(this));
        this._rotatingComponent = AddItemComponent<RotatingComponent>(new RotatingComponent(this));
        this._pickUpComponent = AddItemComponent<PickupableComponent>(new PickupableComponent(this));
        this._shootableComponent = AddItemComponent<ShootableComponent>(new ShootableComponent(this));
        this._destructibleComponent = AddItemComponent<DestructibleComponent>(new DestructibleComponent(this));
    }

    public float GetCurrentSteer()
    {
        return this.attachedTo is SteeringStand ? this._steeringComponent.steerAngle : 0;
    }

    internal override void SetComponentValues()
    {
        base.SetComponentValues();
        RotatingComponent rotator = GetItemComponentOfType<RotatingComponent>();
        if (rotator != null)
        {
            rotator.rotationAxis = Vector3.forward;
            rotator._horizontalRotationLimit = this._steerLimit;
            rotator._originalRotation = transform.rotation;

            SteeringComponent steeringComponent = GetItemComponentOfType<SteeringComponent>();
            if (steeringComponent != null)
            {
                rotator.onIncreaseRotation += steeringComponent.IncreaseSteerAngle;
                rotator.onDecreaseRotation += steeringComponent.DecreaseSteerAngle;
            }
        }
    }
}