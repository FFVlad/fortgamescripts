﻿using System.Collections.Generic;
using UnityEngine;

public class AmmoHolder : Item,IHolder,IItemPlaceable
{
    private PickupableComponent _pickupComponent;
    private HolderComponent holderComponent;
    public Collider[] itemsInRadius = new Collider[20];
    private Item _attachedItem;

    protected override void CreateComponents()
    {
        this._pickupComponent = AddItemComponent<PickupableComponent>(new PickupableComponent(this));
        this.holderComponent = AddItemComponent<HolderComponent>(new HolderComponent(this));
    }

    public float GetContents()
    {
        return this.holderComponent.ammoLeft;
    }

    public void ChangeContents(float delta)
    {
        this.holderComponent.ammoLeft += (int)delta;
    }

    public bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle)
    {
        if (item is AmmoHolder == false)
        {
            return false;
        }
        Vector3 offset = new Vector3();

        offset += Vector3.up * GetComponent<Collider>().bounds.size.y / 2;

        if (snappingStyle == SnappingStyle.Drop)
        {
            return false;
        }
        if (this._attachedItem != null )
        {
            return this._attachedItem is IItemPlaceable ? (this._attachedItem as IItemPlaceable).TryToPlaceItemOnThis(item,snappingStyle) : false;
        }

        PlacementHelper.PlaceItem(item, snappingStyle, offset, this);

        return true;
    }

    public void UnattachItem(Item item)
    {
        if (this._attachedItem == item)
        {
            this._attachedItem = null;
            item.attachedTo = null;
        }
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public Vector3 GetPlacingPosition()
    {
        return transform.position + Vector3.up * GetComponent<Collider>().bounds.size.y / 2;;
    }

    public List<Item> GetAttachedItems()
    {
        List<Item> items = new List<Item>();
        items.Add(this._attachedItem);
        return items;
    }

    public void AttachItem(Item itemToAttach)
    {
        this._attachedItem = itemToAttach;
    }
}