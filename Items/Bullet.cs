﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Item
{
    private PickupableComponent _pickupComponent;
    private ShootableComponent _shootableComponent;

    protected override void CreateComponents()
    {
        this._pickupComponent = AddItemComponent<PickupableComponent>(new PickupableComponent(this));
        this._shootableComponent = AddItemComponent<ShootableComponent>(new ShootableComponent(this));
    }

}

 