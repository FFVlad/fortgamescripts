﻿using UnityEngine;

public class ItemMagnet : Item
{
    private ItemMagnetComponent _magnetComponent;
    public Transform spawnPointForitems;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void CreateComponents()
    {
        this._magnetComponent = AddItemComponent<ItemMagnetComponent>(new ItemMagnetComponent(this));
        AddItemComponent<PickupableComponent>(new PickupableComponent(this));

    }

    internal override void SetComponentValues()
    {
        base.SetComponentValues();
        if (this._magnetComponent != null)
        {
            this._magnetComponent.radius = 15;
            this._magnetComponent.net = this.spawnPointForitems;
        }
        else
        {
            Debug.LogError("Something went horribly wrong");
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 15);
    }
}