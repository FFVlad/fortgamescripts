﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : LockingItem
{
    internal virtual ForceMode ShootingForceMode
    {
        get { return ForceMode.Impulse; }
    } 

    public float force = 5;
    public float horizontalRotationLimit = 30f;
    public float verticalRotationLimit = 25f;
    
    private RotatingComponent _rotatingComponent;
    private PickupableComponent _pickUpComponent;
    private ShootingComponent _shootingComponent;
    private DestructibleComponent _destructibleComponent;

    protected override void CreateComponents()
    {
        this._rotatingComponent = AddItemComponent<RotatingComponent>(new RotatingComponent(this));
        this._pickUpComponent = AddItemComponent<PickupableComponent>(new PickupableComponent(this));
        this._shootingComponent = AddItemComponent<ShootingComponent>(new ShootingComponent(this));
       // this._destructibleComponent = AddItemComponent<DestructibleComponent>(new DestructibleComponent(this));
    }

    internal override void SetComponentValues()
    {
        base.SetComponentValues();
        
        SetForce(this.force);
        this._rotatingComponent.rotationAxis = Vector3.up;
        this._rotatingComponent.allowVerticalRotation = true;
        this._rotatingComponent._horizontalRotationLimit = horizontalRotationLimit;
        this._rotatingComponent.verticalRotationLimit = verticalRotationLimit;
        this._rotatingComponent._originalRotation = transform.rotation;
    }

    internal void SetForce(float force)
    {
        this._shootingComponent.ShootingForce = force;
    }
    
    internal void SetForce(Vector3 force)
    {
        this._shootingComponent.ShootingDirection = force;
    }
    
}
