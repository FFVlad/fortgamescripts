﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Item
{
    public string projectile;
    
    public Transform spawnPoint;
    
    public float attacksPerSecond;
    
    private FollowComponent _followComponent;
    private SpawnerComponent _spawnerComponent;
    
    private Cannon _cannon;
    private float visualDistance = 100;

    public Cannon Cannon
    {
        get
        {
            if (this._cannon == null)
            {
                _cannon = GetComponentInChildren<Cannon>();
            }
            return this._cannon;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        this.rigidBody.isKinematic = true;
    }

    protected override void Update()
    {
        base.Update();

        CalculateShootingForce();

        ShootingComponent shootingComponent = Cannon.GetItemComponentOfType<ShootingComponent>();
        if (shootingComponent != null && shootingComponent.ShootingDirection == Cannon.transform.forward)
        {
            shootingComponent.ShootingDirection = this.Cannon.transform.forward + this.Cannon.transform.up * 0.5f;
        }

    }

    private void CalculateShootingForce()
    {
        Vector3 target = this._followComponent.GetTargetPosition();

        Vector3 direction = target - Cannon.transform.position;

        Vector3 lookDirection = Cannon.transform.forward;

        float angle = Vector3.Angle(direction, lookDirection);

        float force = PositionUtils.CalculateBallisticForce(direction, angle);

        force = Mathf.Clamp(force, 5f, float.MaxValue);
        Cannon.SetForce(force);
    }

    

    protected override void CreateComponents()
    {
        this._followComponent = AddItemComponent<FollowComponent>(new FollowComponent(this));
        this._spawnerComponent = AddItemComponent<SpawnerComponent>(new SpawnerComponent(this));
    }

    internal override void SetComponentValues()
    {
        this._spawnerComponent.projectile = this.projectile;
        this._spawnerComponent.spawnPoint = this.spawnPoint;
        this._spawnerComponent.attacksPerSecond = this.attacksPerSecond;
        Cannon.gameObject.layer = 0;
    }
}
