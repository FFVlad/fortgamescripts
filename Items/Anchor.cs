﻿public class Anchor : Item
{
    private MoverComponent _moverComponent;
    private PickupableComponent _pickupComponent;
    private ShootableComponent _shootableComponent;
    
    public float speedDecrease;

    protected override void Start()
    {
        base.Start();
        this._moverComponent.speedAddition = this.speedDecrease;
    }

    protected override void CreateComponents()
    {
        this._moverComponent = AddItemComponent<MoverComponent>(new MoverComponent(this));
        this._pickupComponent = AddItemComponent<PickupableComponent>(new PickupableComponent(this));
        this._shootableComponent = AddItemComponent<ShootableComponent>(new ShootableComponent(this));
    }

    public float GetCurrentSpeed()
    {
        if (this.isInWater)
        {
            return _moverComponent.currentSpeedAddition;
        }

        return 0;
    }

    public void AddToShip(VesselBase ship)
    {
        ship.AddToInventory(this);
    }

    public void RemoveFromShip(VesselBase ship)
    {
        ship.RemoveFromInventory(this);
    }
}