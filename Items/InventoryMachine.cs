﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryMachine : LockingItem, IItemPlaceable
{
    public InventoryTeleport inventoryTeleport;

    private InventorySummoner inventorySummoner;
    private List<Item> _attachedItems = new List<Item>();

    protected override void CreateComponents()
    {
        AddItemComponent<PickupableComponent>(new PickupableComponent(this));
        this.inventorySummoner = AddItemComponent<InventorySummoner>(new InventorySummoner(this));
    }

    internal void CreateItem(Inventory inventory, ItemData itemData)
    {
        inventoryTeleport.TryToTeleportItem(inventory, itemData);
    }

    public bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle)
    {
        if (snappingStyle == SnappingStyle.Drop)
        {
            return false;
        }

        Vector3 offset = new Vector3();
        offset = item.transform.position - transform.position;

        if (this._attachedItems.Count > 0)
        {
            foreach (var attachedItem in this._attachedItems)
            {
                IItemPlaceable itemPlaceable = attachedItem as IItemPlaceable;
                if (itemPlaceable != null && itemPlaceable.TryToPlaceItemOnThis(item, snappingStyle))
                {
                    return true;
                }
            }
        }

        PlacementHelper.PlaceItem(item, snappingStyle, offset, this);

        return true;
    }


    public void UnattachItem(Item item)
    {
        if (this._attachedItems.Contains(item))
        {
            this._attachedItems.Remove(item);
            item.attachedTo = null;
        }
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public Vector3 GetPlacingPosition()
    {
        return transform.position;
    }

    public List<Item> GetAttachedItems()
    {
        return this._attachedItems;
    }

    public void AttachItem(Item itemToAttach)
    {
        if (this._attachedItems.Contains(itemToAttach) == false)
        {
            this._attachedItems.Add(itemToAttach);
        }
    }
}
