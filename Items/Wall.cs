﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : Item
{
    protected override void CreateComponents()
    {
        AddItemComponent<DestructibleComponent>(new DestructibleComponent(this));
        AddItemComponent<BlockerComponent>(new BlockerComponent(this));
    }

}
