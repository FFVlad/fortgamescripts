﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergySource : Item
{
    protected override void CreateComponents()
    {
        base.CreateComponents();
        AddItemComponent<EnergySourceComponent>(new EnergySourceComponent(this));
    }
}
