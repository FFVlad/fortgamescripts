﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyDestructibleItem : Item
{
    private DestructibleComponent _destructibleComponent;
    private ShipIntegrityCheckerOnDestroy _shipIntegrityChecker;

    protected override void Awake()
    {
    }

    protected override void CreateComponents()
    {
        _destructibleComponent = AddItemComponent<DestructibleComponent>(new DestructibleComponent(this));
        _shipIntegrityChecker = AddItemComponent<ShipIntegrityCheckerOnDestroy>(new ShipIntegrityCheckerOnDestroy(this));
    }

    internal override void SwitchInteractionMaterial(bool active)
    {
    }
}
