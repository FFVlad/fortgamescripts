﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prop : Item, IItemPlaceable
{
    private Item _attachedItem;
    public bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle)
    {
        if (item is Resource == false)
        {
            return false;
        }
        Vector3 offset = new Vector3();

        Collider collider;
        collider = GetComponent<Collider>();
        if (collider)
        {
            offset += Vector3.up * GetComponent<Collider>().bounds.size.y / 2;
        }

        if (snappingStyle == SnappingStyle.Drop)
        {
            return false;
        }
        if (this._attachedItem != null)
        {
            return this._attachedItem is IItemPlaceable ? (this._attachedItem as IItemPlaceable).TryToPlaceItemOnThis(item, snappingStyle) : false;
        }

        PlacementHelper.PlaceItem(item, snappingStyle, offset, this);

        return true;
    }

    public void UnattachItem(Item item)
    {
        if (this._attachedItem == item)
        {
            this._attachedItem = null;
            item.attachedTo = null;
        }
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public Vector3 GetPlacingPosition()
    {
        return transform.position + Vector3.up * GetComponent<Collider>().bounds.size.y / 2; ;
    }

    public List<Item> GetAttachedItems()
    {
        List<Item> items = new List<Item>();
        items.Add(this._attachedItem);
        return items;
    }

    public void AttachItem(Item itemToAttach)
    {
        this._attachedItem = itemToAttach;
    }

    protected override void CreateComponents()
    {
        Debug.LogError("added PickupableComponent");
        AddItemComponent<PickupableComponent>(new PickupableComponent(this));
    }
}
