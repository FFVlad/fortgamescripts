﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCannon : AutoCannon
{
    internal override ForceMode ShootingForceMode
    {
        get { return ForceMode.Impulse; }
    }

    protected override Vector3 CalculateShootPosition()
    {
        Vector3 shootPos = base.CalculateShootPosition();
        return shootPos;
    }

    protected override void CalculateShootingForce()
    {
        Vector3 target = this.Target.position;

        Vector3 direction = target - transform.position;

        Vector3 lookDirection = transform.forward;

        float angle = Vector3.Angle(direction, lookDirection);
        Vector3 force = PositionUtils.CalculateBallisticTrajectory(target, transform.position, angle);

        SetForce(1);
        SetForce(force);
    }

    protected override bool LookForTarget()
    {
        return Target != null;
    }

    public override float GetEnergyThreshold()
    {
        return -1;
    }
}
