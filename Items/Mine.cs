﻿using UnityEngine;

public class Mine : Item
{
    private ExplosiveComponent _explosiveComponent;
    private Rigidbody _rb;
    protected override void Update()
    {
        base.Update();
        CancelOutUpwardsForce();
    }

    private void CancelOutUpwardsForce()
    {
        if (this._rb == null)
        {
            this._rb = GetComponent<Rigidbody>();
        }

        if (this._rb.velocity.y > 7.5f)
        {
            this._rb.velocity *= -1;
        }
    }

    protected override void CreateComponents()
    {
        this._explosiveComponent = AddItemComponent<ExplosiveComponent>(new ExplosiveComponent(this));
    }

    protected override void OnCollisionEnter(Collision other)
    {
        SingleInteract();
    }
}