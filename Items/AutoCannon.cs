﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCannon : Cannon, IAutomated
{
    public Transform Target { get; set; } 

    public Collider[] enemiesInRadius = new Collider[20];
    
    private SpawnerComponent _spawnerComponent;
    public Transform spawnPoint;
    
    private float _autoRotationSpeed = 5;
    protected float visualDistance = 30;
    public float VisualDistance
    {
        get { return this.visualDistance; }
        set { this.visualDistance = value; }
    }
    public float attacksPerSecond = 0.5f;
    private float timeSinceLastAttack = 1;

    private float energyLevel;
    protected override void CreateComponents()
    {
        base.CreateComponents();
        this._spawnerComponent = AddItemComponent<SpawnerComponent>(new SpawnerComponent(this));
    }

    internal override void SetComponentValues()
    {
        base.SetComponentValues();

        PlaceSpawnerPoint();

        this._spawnerComponent.autoShoot = false;
        this._spawnerComponent.spawnPoint = this.spawnPoint;
    }
    
    private void PlaceSpawnerPoint()
    {
        if (this.spawnPoint == null)
        {
            Debug.Log("No spawnpoint on " + name);
            this.spawnPoint = new GameObject("CannonSpawnPoint").transform;
            this.spawnPoint.SetParent(transform);
        }
        int amountOfItemsInRadius = ItemUtils.AmountOfItemsInRadius(this, 5, this.enemiesInRadius);
        for (int i = 0; i < amountOfItemsInRadius; i++)
        {
            Item foundItem = this.enemiesInRadius[i].GetComponent<Item>();
            if (foundItem != null)
            {
                ShootingComponent shootingComponent = foundItem.GetItemComponentOfType<ShootingComponent>();
                if (shootingComponent != null)
                {
                    this.spawnPoint.transform.position = foundItem.transform.position + Vector3.up * 0.5f;
                    break;
                }
            }
        }
    }

    protected override void Update()
    {
        base.Update();
        if (GetEnergyLevel() >= GetEnergyThreshold())
        {
            AutomatedAction();
        }
    }

    public void AutomatedAction()
    {
        if (LookForTarget())
        {
            transform.rotation = RotateToTarget();
            if (CanSeeTarget(15))
            {
                this.timeSinceLastAttack += Time.deltaTime;
                if (this.timeSinceLastAttack > 1 / this.attacksPerSecond)
                {
                    CalculateShootingForce();
                    timeSinceLastAttack = 0;
                    SingleInteract(this);
                }
            }
        }
    }
    
  
    protected virtual bool LookForTarget()
    {
        int amountOfEnemies = ItemUtils.AmountOfEnemiesInRadius(this, VisualDistance, enemiesInRadius);
        for (int i = 0; i < amountOfEnemies; i++)
        {
            if (this.enemiesInRadius[i] != null)
            {
                Target = this.enemiesInRadius[i].transform;
                return true;
            }
        }

        return false;
    }
    
    protected virtual Quaternion RotateToTarget()
    {
        Vector3 shootPosition = CalculateShootPosition();
        Quaternion desiredRotation = Quaternion.LookRotation(shootPosition - transform.position);
        Quaternion newRotation = Quaternion.RotateTowards(transform.rotation, desiredRotation, this._autoRotationSpeed * Time.deltaTime);
        Vector3 newRotationEulerAngles = newRotation.eulerAngles;
        newRotationEulerAngles.z = 0;
        newRotation.eulerAngles = newRotationEulerAngles;
        return newRotation;
    }

    protected virtual Vector3 CalculateShootPosition()
    {
        Vector3 shootPosition = Target.position + Vector3.up;
        return shootPosition;
    }

    internal bool CanSeeTarget(float searchAngle)
    {
        if (Target == null)
        {
            return false;
        }
        
        if (Physics.Linecast(transform.position + transform.forward, Target.position) == false)
        {
            return false;
        }
	
        Vector3 targetDir = Target.position - transform.position;
        Vector3 cameraDir = transform.forward;

        float angle = Vector3.Angle(targetDir, cameraDir);
		
        if (angle > searchAngle)
        {
            return false;
        }

        if (Vector3.Distance(Target.position, transform.position) > this.VisualDistance)
        {
            return false;
        }	

        return true;
    }

    protected virtual void CalculateShootingForce()
    {
        Vector3 target = this.Target.position;

        Vector3 direction = target - transform.position;

        Vector3 lookDirection = transform.forward;

        float angle = Vector3.Angle(direction, lookDirection);

        float force = PositionUtils.CalculateBallisticForce(direction, angle);

        force = Mathf.Clamp(force, 5f, float.MaxValue);
        SetForce(force);
    }


    public void ChangeEnergyLevel(float newEnergyLevel)
    {
        this.energyLevel = newEnergyLevel;
    }
   
    public float GetEnergyLevel()
    {
        return this.energyLevel;
    }

    public virtual float GetEnergyThreshold()
    {
        return 1;
    }
}
