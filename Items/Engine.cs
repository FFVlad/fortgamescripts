﻿using UnityEngine;

public class Engine : Item, IShipItem
{
    private DestructibleComponent _destructibleComponent;
    private MoverComponent _moverComponent;
    private PickupableComponent _pickUpComponent;
    private ShootableComponent _shootableComponent;
    public float speedAddition;

    //Engine animation
    Transform meshTransform;
    bool isgrowing;

    
    public void AddToShip(VesselBase ship)
    {
        ship.AddToInventory(this);
    }

    public void RemoveFromShip(VesselBase ship)
    {
        ship.RemoveFromInventory(this);
    }

    protected override void Start()
    {
        base.Start();
        this._moverComponent.speedAddition = this.speedAddition;
        this.meshTransform = transform.GetChild(0);
    }

    protected override void CreateComponents()
    {
        this._moverComponent = AddItemComponent<MoverComponent>(new MoverComponent(this));
        this._pickUpComponent = AddItemComponent<PickupableComponent>(new PickupableComponent(this));
        this._shootableComponent = AddItemComponent<ShootableComponent>(new ShootableComponent(this));
        this._destructibleComponent = AddItemComponent<DestructibleComponent>(new DestructibleComponent(this));
        AddItemComponent<AudioRandomPitchComponent>(new AudioRandomPitchComponent(this));
    }

    public virtual float GetCurrentSpeed()
    {
        if (this.isInWater)
        {
            return this._moverComponent.currentSpeedAddition;
        }

        return 0;
    }
    
    /*TODO: POEM:
     * ROSES ARE RED
     * ARTIST ARE DEAD
     * IF THERE IS NO ART
     * PROGRAMMER MAKE ART
     * ART!
     * 
     * also add engine sounds
     */

    protected override void Update()
    {
        base.Update();
        PutPutPut();
    }

    private void PutPutPut()
    {
        if (_moverComponent._isPowered)
        {
            if(this.meshTransform.localScale.x > 1.1f)
            {
                this.isgrowing = false;
            }
            else if(this.meshTransform.localScale.x < 0.9f)
            {
                this.isgrowing = true;
            }

            if (isgrowing)
            {
                this.meshTransform.localScale += new Vector3(0.05f, 0.05f, 0.05f);

            }
            else
            {
                this.meshTransform.localScale -= new Vector3(0.05f, 0.05f, 0.05f);

            }
        }
    }
}
    
