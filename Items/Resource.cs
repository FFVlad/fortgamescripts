﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : Item, IItemPlaceable, IResource
{
    private Item _attachedItem;
    public RawResource resourceType;
    public float amount;
    public bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle)
    {
        if (item is Resource == false)
        {
            return false;
        }
        Vector3 offset = new Vector3();

        Collider collider;
        collider = GetComponent<Collider>();
        if (collider)
        {
            offset += Vector3.up * GetComponent<Collider>().bounds.size.y / 2;
        }

        if (snappingStyle == SnappingStyle.Drop)
        {
            return false;
        }
        if (this._attachedItem != null)
        {
            return this._attachedItem is IItemPlaceable ? (this._attachedItem as IItemPlaceable).TryToPlaceItemOnThis(item, snappingStyle) : false;
        }

        PlacementHelper.PlaceItem(item, snappingStyle, offset, this);

        return true;
    }

    public void UnattachItem(Item item)
    {
        if (this._attachedItem == item)
        {
            this._attachedItem = null;
            item.attachedTo = null;
        }
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public Vector3 GetPlacingPosition()
    {
        return transform.position + Vector3.up * GetComponent<Collider>().bounds.size.y / 2; ;
    }

    public List<Item> GetAttachedItems()
    {
        List<Item> items = new List<Item>();
        items.Add(this._attachedItem);
        return items;
    }

    public void AttachItem(Item itemToAttach)
    {
        this._attachedItem = itemToAttach;
    }

    protected override void CreateComponents()
    {
        AddItemComponent<PickupableComponent>(new PickupableComponent(this));
    }

    public void AddResources(RawResource type, float amount)
    {
        Debug.LogError("Adding "+amount+" resoure " + resourceType);
        InventoryManager.Instance.GetPlayerInventory().ChangeRawResource(type, amount);
        
    }

    public override void SingleInteract(params object[] arguments)
    {
        base.SingleInteract(arguments);

        PlayerController player = LogicUtils.FindObjectInArguments<PlayerController>(arguments);
        if (player)
        {
            Debug.LogError("Player collected this");
            AddResources(this.resourceType, this.amount);
            Destroy(this.gameObject);
        }
    }
}
