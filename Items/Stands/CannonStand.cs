﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonStand : StandItem
{
    protected override bool CheckAttachedItems(Item item, SnappingStyle snappingStyle, out bool tryToPlaceItemOnThis)
    {
        if (item is IHolder)
        {
            if (this._attachedItems.Count > 0)
            {
                foreach (var attachedItem in this._attachedItems)
                {
                    IItemPlaceable itemPlaceable = attachedItem as IItemPlaceable;
                    if (itemPlaceable != null && itemPlaceable.TryToPlaceItemOnThis(item, snappingStyle))
                    {
                        tryToPlaceItemOnThis = true;
                        return true;
                    }
                }
            }

            PlacementHelper.PlaceItem(item, snappingStyle, transform.right, this);
            tryToPlaceItemOnThis = true;
            return true;
        }

        tryToPlaceItemOnThis = false;
        return false;
    }
}
