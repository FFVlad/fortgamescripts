﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandItem :  Item, IItemPlaceable
{
    protected List<Item> _attachedItems = new List<Item>();

    protected override void CreateComponents()
    {
        AddItemComponent<PickupableComponent>(new PickupableComponent(this));
        AddItemComponent<DestructibleComponent>(new DestructibleComponent(this));
    }

    public virtual bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle)
    {
        Vector3 offset = new Vector3();

        offset += Vector3.up * GetComponent<Collider>().bounds.size.y / 2;

        if (snappingStyle == SnappingStyle.Drop)
        {
            return false;
        }

        bool tryToPlaceItemOnThis;
        if (CheckAttachedItems(item, snappingStyle, out tryToPlaceItemOnThis))
        {
            return tryToPlaceItemOnThis;
        }

        PlacementHelper.PlaceItem(item, snappingStyle, offset, this);

        return true;
    }

    protected virtual bool CheckAttachedItems(Item item, SnappingStyle snappingStyle, out bool tryToPlaceItemOnThis)
    {
        if (this._attachedItems.Count > 0)
        {
            bool attachedSuccess = false;
            foreach (var attachedItem in this._attachedItems)
            {
                IItemPlaceable itemPlaceable = attachedItem as IItemPlaceable;
                if (itemPlaceable != null && itemPlaceable.TryToPlaceItemOnThis(item, snappingStyle))
                {
                    attachedSuccess = true;
                    break;
                }
            }

            tryToPlaceItemOnThis = attachedSuccess;
            return true;
        }
        tryToPlaceItemOnThis = false;
        return false;
    }

    public void UnattachItem(Item item)
    {
        if (this._attachedItems.Contains(item))
        {
            this._attachedItems.Remove(item);
            item.attachedTo = null;
        }
    }
    
    public Transform GetTransform()
    {
        return transform;
    }

    public virtual Vector3 GetPlacingPosition()
    {
        return transform.position;
    }

    public List<Item> GetAttachedItems()
    {
        return this._attachedItems;
    }

    public void AttachItem(Item itemToAttach)
    {
        if (this._attachedItems.Contains(itemToAttach) == false)
        {
            this._attachedItems.Add(itemToAttach);
        }
    }
}
