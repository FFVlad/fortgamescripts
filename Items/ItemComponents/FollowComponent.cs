﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowComponent : ItemComponent
{
    private Transform target;

    public Transform Target
    {
        get { return this.target; }
        set { this.target = value; }
    }

    public FollowComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }

   

    protected override bool CheckActivationConditions()
    {
        return true;
    }

    protected override void Activation()
    {
        if (Target == null)
        {
            return;
        }
        Vector3 lookPos =  this.Target.position + Vector3.up * 15;
        this.itemAttachedTo.transform.LookAt(lookPos);
    }

    public Vector3 GetTargetPosition()
    {
        return Target != null ? Target.position : Vector3.zero;
    }
}
