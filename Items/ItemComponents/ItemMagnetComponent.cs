﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMagnetComponent : ItemComponent
{
    public float radius;

    public float collectionTime = 5;
    private float _timeSinceLastCollection;
    public int collectionSpeed = 3;

    private List<Item> itemsInNet = new List<Item>();
    private List<Item> _currentlyDraggedItems = new List<Item>();
    private int maxItemsInNet = 5;
    public Transform net;
    private float ChanceToGrabItem = 0.5f;

    public ItemMagnetComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }
    
    protected override bool CheckActivationConditions()
    {
        if (this.itemsInNet.Count >= this.maxItemsInNet)
        {
            return false;
        }
        
        this._timeSinceLastCollection += Time.deltaTime;
        if (this._timeSinceLastCollection >= collectionTime)
        {
            return true;
        }

        return false;
    }

    protected override void Activation()
    {
        List<Item> itemsInRadius = new List<Item>();
        int amountOfItemsInRadius = ItemUtils.AmountOfUnattachedItemsInRadius(this.itemAttachedTo, this.radius, ref itemsInRadius);
        for (int i = 0; i < amountOfItemsInRadius; i++)
        {
            if (itemsInRadius[i] == null)
            {
                continue;
            }

            itemAttachedTo.StartCoroutine(DragItemToShip(itemsInRadius[i]));
            this._timeSinceLastCollection = 0;
            break;
        }
    }

    private IEnumerator DragItemToShip(Item foundItem)
    {
        if (_currentlyDraggedItems.Contains(foundItem))
        {
            yield return null;
        }
        _currentlyDraggedItems.Add(foundItem);
        float distanceToShip = DistanceToShip(foundItem);
        while (distanceToShip > this.radius/3 && foundItem != null)
        {
            foundItem.transform.position = Vector3.MoveTowards(foundItem.transform.position, this.itemAttachedTo.transform.position, this.collectionSpeed*Time.deltaTime);
            distanceToShip = DistanceToShip(foundItem);
            yield return null;
        }
        _currentlyDraggedItems.Remove(foundItem);
        Collect(foundItem);
    }

    private void Collect(Item foundItem)
    {
        foundItem.GetItemComponentOfType<PickupableComponent>().onPickUp += () => this.itemsInNet.Remove(foundItem);
        //if (UnityEngine.Random.Range(0f, 1f) < this.ChanceToGrabItem)
        //{ Never gonna give you up, never gonna let you down
        if (foundItem && foundItem.transform)
        {
            foundItem.transform.SetParent(net);
            Vector3 randomDir = RandomizePosABit();
            Vector3 posToMoveTo = this.net.position + this.net.forward * 2 + randomDir - this.net.transform.up * 0.5f;
            itemAttachedTo.StartCoroutine(QuickMoveTo(foundItem.transform, this.net.transform));
            //foundItem.transform.position = this.net.position + this.net.forward * 2 + randomDir - this.net.transform.up*0.5f;
            foundItem.gameObject.layer = ReferenceManager.ItemsLayer;
            foundItem.rigidBody.isKinematic = true;
            CalculateNetItems();
        }
        //foundItem.transform.SetParent(this.itemAttachedTo.transform);
        //} 
        //else
        //{
        /*TODO: This should be a separate item
         * 
         */
        //    if (foundItem)
        //    {
        //        foundItem.transform.position = this.collectionPoint.position;
        //        foundItem.GetItemComponentOfType<PickupableComponent>().TryToParentThis();
        //    }
        //}
    }

    IEnumerator QuickMoveTo(Transform item, Transform toMoveTo)
    {
        float timer = 0f;
        Vector3 from = item.transform.position;
        Vector3 randomDir = RandomizePosABit();
        Vector3 offset = this.net.forward * 2 + randomDir - this.net.transform.up * 0.5f;
        while (timer < 1f)
        {
            item.position = Vector3.Lerp(from, this.net.position + offset, timer / 1f);
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private void CalculateNetItems()
    {
        itemsInNet = new List<Item>();
        foreach (Transform t in itemAttachedTo.transform)
        {
            Item current = t.GetComponent<Item>();
            if (current != null)
            {
                itemsInNet.Add(current);
            }
        }
    }

    private Vector3 RandomizePosABit()
    {
        Vector3 randomDir;
        float random = Random.Range(0f, 1f);
        if (random > 0.7f)
        {
            float random2 = Random.Range(0f, 1f);
            randomDir = this.net.right * random2;
        }
        else if (random < 0.3f)
        {
            float random2 = Random.Range(0f, 1f);
            randomDir = -this.net.right * random2;
        }
        else
        {
            float random2 = Random.Range(0f, 1f);
            randomDir = this.net.forward * random2;
        } 
        return randomDir;
    }

    private float DistanceToShip(Item foundItem)
    {
        float distanceToShip = Vector3.Distance(foundItem.transform.position, this.itemAttachedTo.transform.position);
        return distanceToShip;
    }
}
