﻿using System.Collections;
using System.Collections.Generic;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

public class Item : MonoBehaviour, IInteractable, IDeletableObject
{   
    public RenderMaterialDictionary renderersToChangeMaterialFor;
    public Material deleteMaterial;
    
    public int itemId = -1;

    public float damageOnImpact = 3;
    public float timeBetweenInteractions = 1;
    public float health = 10;

    public List<ItemComponent> itemComponents = new List<ItemComponent>();
    private List<Item> _recentlyInteractedItems = new List<Item>();

    internal Rigidbody rigidBody;

    private bool _disableInteractions;
    internal bool isInWater;
    public IItemPlaceable attachedTo;
    public SnappingStyle snappingStyle = SnappingStyle.NoRotation;
    
    private float lifeTime = 5;

    
    private BoatAlignNormal floatingScript;
    public BoatAlignNormal FloatingScript
    {
        get
        {
            if (this.floatingScript == null)
            {
                this.floatingScript = GetComponent<BoatAlignNormal>();
                if (this.floatingScript == null)
                {
                    Debug.Log(name + " cannot float");
                }
            }
            return this.floatingScript;
        }
    }

    public bool Floating
    {
        get
        {
            return FloatingScript != null && FloatingScript.enabled;
        }
    }

    protected virtual void Awake()
    {
        rigidBody = this.GetComponent<Rigidbody>();
        if (rigidBody == null)
        {
            rigidBody = this.gameObject.AddComponent<Rigidbody>();
            if (this.GetComponent<Collider>() == null || this.GetComponent<Collider>().isTrigger)
            {
                rigidBody.useGravity = false;
            }
        }
    }

    protected virtual void Start()
    {
        SetMaterial();
        InitializeComponents();
    }

    public void InitializeComponents()
    {
        CreateComponents();
        SetComponentValues();
    }

    protected virtual void CreateComponents()
    {
        AddItemComponent<PickupableComponent>(new PickupableComponent(this));
    }
    
    internal virtual void SetComponentValues()
    {
        DestructibleComponent destructibleComponent = GetItemComponentOfType<DestructibleComponent>();
        if (destructibleComponent != null)
        {
            destructibleComponent.health = this.health;
        }
        ShootableComponent shootableComponent = GetItemComponentOfType<ShootableComponent>();
        if (shootableComponent != null)
        {
            shootableComponent._damage = this.damageOnImpact;
        }
        PickupableComponent pickupableComponent = GetItemComponentOfType<PickupableComponent>();
        if (pickupableComponent != null)
        {
            pickupableComponent.TryToParentThis();
        }
    }

    protected virtual void Update()
    {
        foreach (var component in this.itemComponents)
        {
            component.Activate();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Item interactionItem = LogicUtils.IsInteractable(other);
        if (interactionItem)
        {
            SingleInteract(interactionItem);
        }
    }

    protected virtual void OnCollisionEnter(Collision other)
    {
        Item interactionItem = LogicUtils.IsInteractable(other.collider);
        if (interactionItem)
        {
            SingleInteract(interactionItem);
        }
    }

    public virtual void SingleInteract(params object[] arguments)
    {
        if (this._disableInteractions)
        {
            return;
        }

        Item itemInteracting = LogicUtils.FindObjectInArguments<Item>(arguments);
        if (itemInteracting != null)
        {
            if (ItemNotInteractedWithRecently(itemInteracting))
            {
                itemInteracting.SingleInteract(this);
            }
            else
            {
                return;
            }
        }

        foreach (var component in this.itemComponents)
        {
            if (component is PickupableComponent)
            {
                continue;
            }
            component.Interact(arguments);
        }
    }

    public virtual void CancelInteract(params object[] arguments)
    {
        foreach (var component in this.itemComponents)
        {
            if (component is PickupableComponent)
            {
                continue;
            }
            component.CancelInteraction(arguments);
        }
    }

    public Transform Allegiance()
    {
        return transform.root;
    }

    public virtual void Hold(bool holding, params object[] arguments)
    {
        _disableInteractions = holding;
        List<object> args = new List<object>();
        args.AddRange(arguments);
        args.Add(holding);
        arguments = args.ToArray();
        GetItemComponentOfType<PickupableComponent>()?.Interact(arguments);
    }

    private bool ItemNotInteractedWithRecently(Item itemInteracting)
    {
        if (this._recentlyInteractedItems.Contains(itemInteracting))
        {
            return false;
        }

        AddToInteractionList(itemInteracting);
        return true;
    }

    private void AddToInteractionList(Item itemInteracting)
    {
        this._recentlyInteractedItems.Add(itemInteracting);
        if (gameObject.activeSelf)
        {
            StartCoroutine(DeleteItemFromInteractionList(itemInteracting));
        }
    }

    private IEnumerator DeleteItemFromInteractionList(Item itemInteracting)
    {
        float time = 0;
        while (true)
        {
            time += Time.deltaTime;
            if (time >= this.timeBetweenInteractions)
            {
                break;
            }

            yield return null;
        }

        this._recentlyInteractedItems.Remove(itemInteracting);
    }

    public T GetItemComponentOfType<T>() where T : ItemComponent
    {
        foreach (var component in this.itemComponents)
        {
            T variable = component as T;
            if (variable != null)
            {
                return variable;
            }
        }
        return default(T);
    }

    public T AddItemComponent<T>(ItemComponent component) where T:ItemComponent
    {
        if (this.itemComponents.Contains(component) == false)
        {
            this.itemComponents.Add(component);
            return (T)component;
        }

        return null;
    }

    private void SetMaterial()
    {

       

        Renderer[] renderers;

        if (this.renderersToChangeMaterialFor != null && this.renderersToChangeMaterialFor.Count > 0)
        {
            renderers = new Renderer[this.renderersToChangeMaterialFor.Keys.Count];
            this.renderersToChangeMaterialFor.Keys.CopyTo(renderers, 0);
        }
        else
        {
            renderers = this.GetComponentsInChildren<Renderer>();
        }

        if (renderers != null && renderers.Length > 0)
        {
            foreach (var render in renderers)
            {
                /*TODO: If render is child of another item, (grandchild of this) remove it from this list!)
                 * 
                 */
                if (this.renderersToChangeMaterialFor.ContainsKey(render))
                {
                    renderersToChangeMaterialFor[render] = render.material;
                }
                else
                {
                    this.renderersToChangeMaterialFor.Add(render, render.material);
                }
            }
        }
    }

    internal virtual void SwitchInteractionMaterial(bool active)
    {
        ICollection<Renderer> renderers = this.renderersToChangeMaterialFor.Keys;
        foreach (var render in renderers)
        {
            if (render)
            {
                ChangeInteractionMaterial(active, render);
            }
        }
    }

    private void ChangeInteractionMaterial(bool active, Renderer render)
    {
        if (active)
        {
            render.material = ReferenceManager.Instance.interactionMaterial;
        }
        else
        {
            render.material = this.renderersToChangeMaterialFor[render];
        }
    }

    public void FindShip()
    {
        isInWater = true;
        /*BlockAttachPoint closestBlock = LookForShip();
        if (closestBlock)
        {
            attachedTo = closestBlock;
        }
        else
        {
        Float();
        }*/
    }

    public void Float()
    {
        if (FloatingScript)
        {
            FloatingScript.enabled = true;
        }
        
        /*TODO: kill this in itemspawner*/
        StartCoroutine(GetRidOfItem(this.lifeTime * 2));
    }

    public void DeletableHilighted()
    {
        foreach (Renderer renderer in renderersToChangeMaterialFor.Keys)
        {
            renderer.material = this.deleteMaterial;
        }
    }

    public void DeletableUnhighlighted()
    {
        foreach (Renderer renderer in renderersToChangeMaterialFor.Keys)
        {
            renderer.material = this.renderersToChangeMaterialFor[renderer];
        }
    }

    public void DeleteItem(Inventory playerWhoRemovedIt)
    {
        Destroy(gameObject);
        playerWhoRemovedIt.ChangeItemAmount(itemId, 1);
    }

    public void GetRidOfItem()
    {
        StartCoroutine(GetRidOfItem(this.lifeTime));
    }
    
    private IEnumerator GetRidOfItem(float time)
    {
        yield return new WaitForSeconds(time);
        Debug.Log(new System.Diagnostics.StackTrace());
        PoolManager.ReturnObjectToPoolOrDestroyIt(gameObject);
    }

}

[System.Serializable]
public class RenderMaterialDictionary : SerializableDictionaryBase<Renderer, Material>
{
}
