﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IContinuousInteractable : IInteractable
{
    void LockPlayer(PlayerController player);
}
