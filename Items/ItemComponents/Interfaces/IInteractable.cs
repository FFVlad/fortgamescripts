﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    void SingleInteract(params object[] arguments);
    void Hold(bool holding, params object[] arguments);
}
