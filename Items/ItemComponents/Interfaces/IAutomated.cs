﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAutomated
{
    void ChangeEnergyLevel(float newEnergyLevel);
    void AutomatedAction();
    float GetEnergyLevel();
    float GetEnergyThreshold();
}
