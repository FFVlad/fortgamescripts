﻿public interface IHolder
{
    float GetContents();
    void ChangeContents(float delta);

}
