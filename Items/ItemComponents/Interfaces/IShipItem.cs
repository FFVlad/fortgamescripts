﻿public interface IShipItem
{
  void AddToShip(VesselBase ship);
  void RemoveFromShip(VesselBase ship);
}
