﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public interface IContinuosComponent
{
     void IncreaseInteraction();
     void DecreaseInteraction();
     void IncreaseSecondaryInteraction();
     void DecreaseSecondaryInteraction();
}
