﻿using UnityEngine;

public interface IDamageable
{
    void TakeDamage(Damage damage);
}

public class Damage
{
    public float amount;
    public Object dealer;

    public Damage(float amount, Object dealer)
    {
        this.amount = amount;
        this.dealer = dealer;
    }
}