﻿using System;
using UnityEngine;

public class DestructibleComponent : ItemComponent, IDamageable
{
    public Action BeingDestroyed;

    public float health = -1;
    public bool destroyed;

    protected override void Interaction(params object[] arguments)
    {
        Damage takenDamage = LogicUtils.FindObjectInArguments<Damage>(arguments);
        if (takenDamage != null)
        {
            TakeDamage(takenDamage);
        }
    }

    public void TakeDamage(Damage damage)
    {
        Item damageDealer = damage.dealer as Item;
        if (damageDealer?.Allegiance() == this.itemAttachedTo.Allegiance())
        {
            return;
        }
        PoolManager.ReturnObjectToPoolOrDestroyIt(damageDealer.gameObject);
        this.health -= damage.amount;
        this.itemAttachedTo.health = this.health;
        if (this.health <= 0)
        {
            Destroy();
        }
      
    }

    private void Destroy()
    {
        this.destroyed = true;
        GameObject.Destroy(this.itemAttachedTo.gameObject);

        if (BeingDestroyed != null)
        {
            BeingDestroyed.Invoke();
        }
    }

    public DestructibleComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }
}
