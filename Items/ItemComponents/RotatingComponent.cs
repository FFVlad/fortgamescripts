﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingComponent : ItemComponent, IContinuosComponent
{
    public Action onIncreaseRotation;
    public Action onDecreaseRotation;
    
    public float anglesPerFrame = 15;
    
    public Vector3 rotationAxis = Vector3.forward;
    public Vector3 secondaryRotationAxis = Vector3.right;
    
    public bool allowVerticalRotation = false;
    
    public float _horizontalRotationLimit;
    public float verticalRotationLimit;

    public Quaternion _originalRotation;
    private float RotationSpeed
    {
        get
        {
            float rotationSpeed = this.anglesPerFrame * Time.deltaTime * 5f;
            return rotationSpeed;
        }
    }

    
    public RotatingComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }

    
    public void IncreaseInteraction()
    {
        IncreaseRotation();
    }

    public void DecreaseInteraction()
    {
        DecreaseRotation();
    }
    
    private void IncreaseRotation()
    {
        bool rotationClamped = RotateObject(-this.rotationAxis, this._horizontalRotationLimit);
        if (rotationClamped == false && this.onIncreaseRotation != null)
        {
            this.onIncreaseRotation();
        }
    }


    private void DecreaseRotation()
    {
        bool rotationClamped = RotateObject(this.rotationAxis, this._horizontalRotationLimit);
        if (rotationClamped == false  && this.onDecreaseRotation != null)
        {
            this.onDecreaseRotation();
        }
    }

   
    public void IncreaseSecondaryInteraction()
    {
        if (allowVerticalRotation)
        {
            IncreaseSecondaryRotation();
        }
    }

    public void DecreaseSecondaryInteraction()
    {
        if (allowVerticalRotation)
        {
            DecreaseSecondaryRotation();
        }
    }

    private void IncreaseSecondaryRotation()
    {
        RotateObject(this.secondaryRotationAxis, this.verticalRotationLimit);
    }

    private void DecreaseSecondaryRotation()
    {
        RotateObject(-this.secondaryRotationAxis, this.verticalRotationLimit);
    }
    
    private bool RotateObject(Vector3 rotationAxis, float rotationLimit)
    {
        Transform rotateTransform = this.itemAttachedTo.transform;

        Quaternion rotation = rotateTransform.localRotation;
        rotation *= Quaternion.Euler(rotationAxis * RotationSpeed);
        bool rotationClamped = PositionUtils.ClampRotation(ref rotation, rotationAxis, rotationLimit, this._originalRotation);
      
        rotateTransform.localRotation = rotation;
        return rotationClamped;
    }

}
