﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioRandomPitchComponent : AudioComponent
{
    private float pitchMin = 0.95f;
    private float pitchMax = 1.05f;

    public AudioRandomPitchComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {

    }

    protected override void Interaction(params object[] arguments)
    {
        base.Interaction(arguments);

        if (audioSource)
        {
            if (audioSource.isPlaying)
            {
                audioSource.pitch = Random.Range(pitchMin, pitchMax);
            }
        }
    }

}
