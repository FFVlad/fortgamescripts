﻿using System.Collections;
using UnityEngine;

public class ShootableComponent : ItemComponent
{
    private Rigidbody _rigidbody;
    public float _damage = 5;
    protected override void Interaction(params object[] arguments)
    {
        Vector3 direction = LogicUtils.FindObjectInArguments<Vector3>(arguments);
        ForceMode forceMode = LogicUtils.FindObjectInArguments<ForceMode>(arguments);
        if (direction.magnitude > 0.1f)
        {
            Shoot(direction, forceMode);
        }

        Item interactableItem = LogicUtils.FindObjectInArguments<Item>(arguments);
        if (interactableItem)
        {
            DestructibleComponent destructibleComponent = interactableItem.GetItemComponentOfType<DestructibleComponent>();
            if (destructibleComponent != null)
            {
                Damage damage = new Damage(this._damage, this.itemAttachedTo);
                interactableItem.SingleInteract(damage);
            }
        }
    }

    private void Shoot(Vector3 direction, ForceMode forceMode)
    {
        this._rigidbody.AddForce(this._rigidbody.mass * direction, forceMode);
        this.itemAttachedTo.GetRidOfItem();
    }

    

    public ShootableComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
        this._rigidbody = base.itemAttachedTo.GetComponent<Rigidbody>();
        this._damage = itemAttachedTo.damageOnImpact;
    }
}
