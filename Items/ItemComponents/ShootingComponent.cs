﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingComponent : ItemComponent
{
    public float shootingForce = 5;
    private Vector3 shootingDirection = Vector3.zero;
     
    public Vector3 ShootingDirection
    {
        get
        {
            if (this.shootingDirection == Vector3.zero)
            {
                return this.itemAttachedTo.transform.forward;
            }
            return this.shootingDirection;
        }
        set { this.shootingDirection = value; }
    }

    public float ShootingForce
    {
        get { return this.shootingForce; }
        set
        {
            if(float.IsNaN(value))
            {
                Debug.Log("Vlad is very good at math. Please tell him about it");
            }

            this.shootingForce = value;
        }
    }

    protected override void Interaction(params object[] arguments)
    {
        Item interactableItem = LogicUtils.FindObjectInArguments<Item>(arguments);
       
        if (interactableItem)
        {
            ShootableComponent shootableComponent = interactableItem.GetItemComponentOfType<ShootableComponent>();
            if (shootableComponent != null)
            {
                Vector3 direction = ShootingDirection * this.ShootingForce;
                Cannon cannon = this.itemAttachedTo as Cannon;
                if (cannon != null)
                {
                    ForceMode forceMode = cannon.ShootingForceMode;
                    interactableItem.SingleInteract(direction, forceMode);
                }
                else
                {
                    interactableItem.SingleInteract(direction);
                }
            }
        }
    }

    public ShootingComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }
}
