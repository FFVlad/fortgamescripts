﻿using UnityEngine;

public class BlockerComponent : ItemComponent, IDamageable
{
    private float _pushback = 0.5f;
    protected override void Interaction(params object[] arguments)
    {
        Damage takenDamage = LogicUtils.FindObjectInArguments<Damage>(arguments);
        if (takenDamage != null)
        {
            TakeDamage(takenDamage);
        }
    }

    public BlockerComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }

    public void Block(Object damageDealer)
    {
        Rigidbody damagingObject = (damageDealer as Item).gameObject.GetComponent<Rigidbody>();
        damagingObject.velocity = -damagingObject.velocity * this._pushback;
        damagingObject.useGravity = true;
    }

    public void TakeDamage(Damage damage)
    {
        Block(damage.dealer);
    }
}
