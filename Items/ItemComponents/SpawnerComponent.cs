﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerComponent : ItemComponent
{
    public string projectile = "CannonBall";
    
    public Transform spawnPoint;
    
    public float attacksPerSecond;
    private float timeSinceLastAttack = 1;
    
    private float shootingDistance = 100;
    
    public bool autoShoot = true;

    private List<IHolder> _holders = new List<IHolder>();
    private ShootableComponent _shootableComponent;

    /*TODO: only works with follow component, make good*/
    private Transform target;
    public Transform Target
    {
        get
        {
            if (this.target == null)
            {
                this.target = this.itemAttachedTo.GetItemComponentOfType<FollowComponent>().Target;
            }

            return this.target;
        }
        set { this.target = value; }
    }

    public SpawnerComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }

    protected override bool CheckActivationConditions()
    {
        return autoShoot;
    }

    protected override void Activation()
    {
        if (Target == null)
        {
            return;
        }
        float distanceToTarget = Vector3.Distance(Target.position, this.itemAttachedTo.transform.position);
        if (distanceToTarget> this.shootingDistance)
        {
            return;
        }

        this.timeSinceLastAttack += Time.deltaTime;
        if (this.timeSinceLastAttack > 1 / this.attacksPerSecond)
        {
            timeSinceLastAttack = 0;
            Spawn();
        }
    }

    protected override void Interaction(params object[] arguments)
    {
        AutoCannon shouldShoot = LogicUtils.FindObjectInArguments<AutoCannon>(arguments);
        PlayerController player = LogicUtils.FindObjectInArguments<PlayerController>(arguments);
        if(player || shouldShoot)
        {
            TryShoot();
        }
        
        IHolder ammoHolder = LogicUtils.FindObjectInArguments<IHolder>(arguments);
        if (ammoHolder != null)
        {
            if (this._holders.Contains(ammoHolder) == false)
            {
                this._holders.Add(ammoHolder);
            }
        }
    }

    private void TryShoot()
    {
        if (this.itemAttachedTo is EnemyCannon)
        {
            Spawn();
            return;
        }

        if (this._holders.Count > 0)
        {
            foreach (var holder in this._holders)
            {
                if (holder.GetContents() > 0)
                {
                    holder.ChangeContents(-1);
                    Spawn();
                    break;
                }
            }
        }
    }

    public void MoveSpawnPoint(Vector3 newPosition)
    {
        this.spawnPoint.transform.position = newPosition;
    }
    
    private void Spawn()
    {
        PoolObjectSettings settings = new PoolObjectSettings();
        settings.positionToSet = this.spawnPoint.transform.position;
        settings.rotationToSet = this.spawnPoint.transform.rotation;
        PoolManager.Instance.GetPooledObject(this.projectile, settings);
    }

  
}
