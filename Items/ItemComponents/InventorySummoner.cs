﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySummoner : ItemComponent
{
    private bool inventoryOpen = false;

    public InventorySummoner(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }

    protected override void Interaction(params object[] arguments)
    {
        base.Interaction(arguments);

        inventoryOpen = !inventoryOpen;
        SetInventoryView();
    }

    public override void CancelInteraction(params object[] arguments)
    {
        base.CancelInteraction(arguments);

        inventoryOpen = false;
        SetInventoryView();
    }

    private void SetInventoryView()
    {
        if (GameManager.Instance.currentPlayMode == PlayMode.PlayMode)
        {
            Inventory inventory = ReferenceManager.playerController?.CurrentPlayerInventory;

            if (inventory != null)
            {
                SailingUI.SailingInstance.SetPlayerInventoryUI(inventory, inventoryOpen);
            }

            if (inventoryOpen)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
    }

}
