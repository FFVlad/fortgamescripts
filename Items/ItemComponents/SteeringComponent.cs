﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringComponent : ItemComponent, IContinuosComponent
{
    private float _maxSteer;
    private float _minSteer;
    private Transform _vessel;
    private float _currentSteerAngle;
    private float _steerToChangePerFrame;

    internal float steerAngle;

    public SteeringComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
        SteeringItem steeringItem = itemAttachedTo as SteeringItem;

        _maxSteer = steeringItem._steerLimit;

        _minSteer = -steeringItem._steerLimit;

        _steerToChangePerFrame = steeringItem.steerToChange;

        _vessel = itemAttachedTo.transform.root;
    }
 
    public void IncreaseSteerAngle()
    {
        if (_currentSteerAngle > _minSteer)
        {
            this.steerAngle -= this._steerToChangePerFrame * Time.deltaTime;
            this._currentSteerAngle = this.steerAngle;
        }
    }

    public void DecreaseSteerAngle()
    {
        if (_currentSteerAngle < _maxSteer)
        {
            this.steerAngle += this._steerToChangePerFrame * Time.deltaTime;
            this._currentSteerAngle = this.steerAngle;
        }
    }

    public void IncreaseInteraction()
    {
        //IncreaseSteerAngle();
    }

    public void DecreaseInteraction()
    {
       //DecreaseSteerAngle();
    }

    public void IncreaseSecondaryInteraction()
    {
    }

    public void DecreaseSecondaryInteraction()
    {
    }
}
