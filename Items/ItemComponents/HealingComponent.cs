﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HealingComponent : ItemComponent
{
    public float radius = 10;
    public float healingPerSecond = 1;
    public float healTick = 0.1f;
    public Collider[] itemsInRadius = new Collider[10];


    private float _timeSinceLastHeal;
    
    
    public HealingComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }

    protected override bool CheckActivationConditions()
    {
        this._timeSinceLastHeal += Time.deltaTime;
        if (this._timeSinceLastHeal >= healTick)
        {
            this._timeSinceLastHeal = 0;
            return true;
        }

        return false;
    }

    protected override void Activation()
    {
        int amountOfItemsInRadius = ItemUtils.AmountOfItemsInRadius(this.itemAttachedTo, this.radius, this.itemsInRadius);
        for (int i = 0; i < amountOfItemsInRadius; i++)
        {
            Item healedItem = this.itemsInRadius[i].GetComponent<Item>();
            if (healedItem != null)
            {
                DestructibleComponent destructibleComponent = healedItem.GetItemComponentOfType<DestructibleComponent>();
                if (destructibleComponent != null)
                {
                    destructibleComponent.health += this.healingPerSecond;
                }
            }
        }
    }
}
