﻿public class ItemComponent
 {
    public Item itemAttachedTo;

    protected ItemComponent(Item itemAttachedTo)
    {
        this.itemAttachedTo = itemAttachedTo;
    }

    public void Activate()
    {
        if (CheckActivationConditions())
        {
            Activation();
        }
        else
        {
            Deactivation();
        }
    }

    protected virtual bool CheckActivationConditions()
    {
        return false;
    }

    protected virtual void Activation(){}

    protected virtual void Deactivation(){}

    public void Interact(params object[] arguments)
    {
        Interaction(arguments);
    }

    protected virtual void Interaction(params object[] arguments){}

    public virtual void CancelInteraction(params object[] arguments)
    {

    }

}