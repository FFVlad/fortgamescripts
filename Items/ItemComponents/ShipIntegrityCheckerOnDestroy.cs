﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipIntegrityCheckerOnDestroy : ItemComponent
{
    private DestructibleComponent destructible;

    public ShipIntegrityCheckerOnDestroy(Item itemAttachedTo) : base(itemAttachedTo)
    {
        this.destructible = itemAttachedTo.GetItemComponentOfType<DestructibleComponent>();

        if (this.destructible != null)
        {
            this.destructible.BeingDestroyed += BeingDestroyed;
        }
    }

    private void BeingDestroyed()
    {
        BoatFrameDataContainer dataContainer = this.itemAttachedTo.gameObject.transform.root.GetComponent<BoatFrameDataContainer>();

        if (dataContainer)
        {
            // commented this out since it currently just breaks things
            //dataContainer.TimedCheckShipIntegrity();
        }
    }

}