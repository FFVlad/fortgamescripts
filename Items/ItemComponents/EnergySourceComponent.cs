﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergySourceComponent : ItemComponent
{
    private List<IAutomated> _energyConsumers = new List<IAutomated>();
    
    public float radius = 10;
    public float energyReserve = 10;
    
    private float _timeSinceLastActivation = 0;
    private bool _active = true;
    
    public EnergySourceComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }


    protected override void Interaction(params object[] arguments)
    {
        base.Interaction(arguments);
        PlayerController player = LogicUtils.FindObjectInArguments<PlayerController>(arguments);
        if (player != null)
        {
            this._active = !this._active;
            if (this._active == false)
            {
                DeactivateSource();
            }
        }
    }

    protected override bool CheckActivationConditions()
    {
        if (this._active == false)
        {
            return false;
        }
        
        this._timeSinceLastActivation += Time.deltaTime;
        if ((this._timeSinceLastActivation <= 0.5f))
        {
            return false;
        }

        this._timeSinceLastActivation = 0;
        return true;
    }

    protected override void Activation()
    {
        List<IAutomated> itemsInRadius = new List<IAutomated>(); 
        int consumersInRadius = ItemUtils.AmountOfEnergyConsumersInRadius(this.itemAttachedTo, this.radius, itemsInRadius);
        for (int i = 0; i < consumersInRadius; i++)
        {         
            IAutomated foundItem = itemsInRadius[i];
            if (foundItem != null)
            {
                AddEnergyConsumer(foundItem);
            }
        }
    }

    private void AddEnergyConsumer(IAutomated foundItem)
    {
        if (this._energyConsumers.Contains(foundItem) == false)
        {
            this._energyConsumers.Add(foundItem);
            RecalculateEnergyLevels();
        }
    }

    private void RecalculateEnergyLevels()
    {
        foreach (var consumer in this._energyConsumers)
        {
            consumer.ChangeEnergyLevel(this.energyReserve/this._energyConsumers.Count);
        }
    }
    
    private void DeactivateSource()
    {
        foreach (var consumer in this._energyConsumers)
        {
            consumer.ChangeEnergyLevel(0);
        }
    }
}
