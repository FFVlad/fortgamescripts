﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderComponent : ItemComponent
{
    public int ammoLeft = 5;
    private ShootableComponent shootableComponent;

    public HolderComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {}
    
    protected override void Interaction(params object[] arguments)
    {
        Item itemInteracting = LogicUtils.FindObjectInArguments<Item>(arguments);
        if (itemInteracting != null)
        {
            if (itemInteracting is Bullet)
            {
                this.shootableComponent = itemInteracting.GetItemComponentOfType<ShootableComponent>();
                if (this.shootableComponent != null)
                {
                    PoolManager.ReturnObjectToPoolOrDestroyIt(itemInteracting.gameObject);
                    this.ammoLeft++;
                }
            }
        }
    }
}
