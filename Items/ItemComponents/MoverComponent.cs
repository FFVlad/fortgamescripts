﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverComponent : ItemComponent
{
    internal bool _isPowered;

    public float speedAddition;

    internal float currentSpeedAddition = 0f;

    public MoverComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }

    protected override void Activation()
    {
       currentSpeedAddition = speedAddition;
    }

    protected override void Deactivation()
    {
       currentSpeedAddition = 0f;
    }

    protected override bool CheckActivationConditions()
    {
        if (_isPowered)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected override void Interaction(params object[] arguments)
    {
        PlayerController player = LogicUtils.FindObjectInArguments<PlayerController>(arguments);
        if (player)
        {
            if (_isPowered)
            {
                _isPowered = false;
            }
            else
            {
                _isPowered = true;
            }
        }
    }
}
