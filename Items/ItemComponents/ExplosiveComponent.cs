﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveComponent : ItemComponent
{
    public float radius = 15f;
    public float damage = 20;
    public Collider[] itemsInRadius = new Collider[50];
    
    public ExplosiveComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
    }

    protected override void Interaction(params object[] arguments)
    {
        Explode();
    }

    private void Explode()
    {
        bool hitSomething = false;
        int amountOfItemsInRadius = Physics.OverlapSphereNonAlloc(this.itemAttachedTo.transform.position, this.radius, this.itemsInRadius);
        for (int i = 0; i < amountOfItemsInRadius; i++)
        {
            Vector3 theirPosition = itemsInRadius[i].transform.position;

            float distance = Vector3.Distance(this.itemAttachedTo.transform.position, theirPosition);
            float distanceLerp = 1 - (distance / radius);
            int damageDealt = Mathf.RoundToInt(Mathf.Lerp(0, this.damage, distanceLerp));
            
            Damage damageToDeal = new Damage(damageDealt, this.itemAttachedTo);

            Item damagedItem = this.itemsInRadius[i].GetComponent<Item>();
            if (damagedItem != null)
            {
                DestructibleComponent destructibleComponent = damagedItem.GetItemComponentOfType<DestructibleComponent>();
                if (destructibleComponent != null)
                {
                    damagedItem.SingleInteract(damageToDeal);
                    hitSomething = true;
                }
            }
            else
            {
                PlayerEntity damagedPlayer = this.itemsInRadius[i].GetComponent<PlayerEntity>();
                if (damagedPlayer)
                {
                    damagedPlayer.ChangeHealth(-damageDealt, this);
                    hitSomething = true;
                }
            }
        }

        if (hitSomething)
        {
            GameObject.Destroy(this.itemAttachedTo.gameObject);
        }
    }
}
