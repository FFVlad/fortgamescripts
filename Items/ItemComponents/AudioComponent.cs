﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioComponent : ItemComponent
{
    protected AudioSource audioSource;

    public AudioComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
        audioSource = itemAttachedTo.GetComponent<AudioSource>();

        if (audioSource == null)
        {
            Debug.LogError("LoopingAudioComponent found on an item with no audio source!");
        }
    }

    protected override void Interaction(params object[] arguments)
    {
        base.Interaction(arguments);

        if (audioSource)
        {
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
            else
            {
                audioSource.Play();
            }
        }
    }

}
