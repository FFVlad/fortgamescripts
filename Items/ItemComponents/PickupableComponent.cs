﻿using System;
using UnityEngine;

public class PickupableComponent : ItemComponent
{
    private Vector3 _wantedPosition;

    public Action onPickUp;
    
    private Rigidbody _rigidBody;
    private Collider _collider;
    
    private Quaternion _originalRotation;
    private bool _originalRigidBodyGravity;
    

    private Transform _currentParent;
    private Transform _originalParent;
    
    private PlayerController _parentPlayer;

    private bool _isInHand;
    private LayerMask ignoreRaycastAndItems;
    //private LayerMask ignoreRaycast;

    public PickupableComponent(Item itemAttachedTo) : base(itemAttachedTo)
    {
        this._originalParent = itemAttachedTo.transform.parent;
        
        this._rigidBody = itemAttachedTo.GetComponent<Rigidbody>();
        this._collider = itemAttachedTo.GetComponent<Collider>();

        this._originalRigidBodyGravity = this._rigidBody.useGravity;
        this._originalRotation = itemAttachedTo.transform.rotation;

        ignoreRaycastAndItems = ~((1 << ReferenceManager.ItemsLayer) | (1 << ReferenceManager.IgnoreRaycastLayer) | (1 << ReferenceManager.DetachedItemsLayer) | (1 << ReferenceManager.KCCLayer));
        //ignoreRaycast = ~((1 << ReferenceManager.IgnoreRaycastLayer) | (1 << ReferenceManager.DetachedItemsLayer) | (1 << ReferenceManager.KCCLayer));
    }

    protected override void Activation()
    {
        this._rigidBody.useGravity = false;
        this._rigidBody.AddTorque(Vector3.left * 0.4f);
        this._rigidBody.maxAngularVelocity = 0.4f;
        this._rigidBody.velocity = Vector3.zero;

        this._wantedPosition = GetWantedPosition();
        float distanceToWantedPosition = Vector3.Distance(this.itemAttachedTo.transform.position, this._wantedPosition);

        if (distanceToWantedPosition > 0.05f)
        {
            Vector3 forceDirection = (this.itemAttachedTo.transform.position - this._wantedPosition).normalized;

            this._rigidBody.AddForce(-forceDirection * 1.5f, ForceMode.Impulse);
        }
    }

    protected override bool CheckActivationConditions()
    {
        return this._isInHand;
    }

    protected override void Deactivation()
    {
        this._isInHand = false;
    }

    protected override void Interaction(params object[] arguments)
    {
        PlayerController picker = LogicUtils.FindObjectInArguments<PlayerController>(arguments);

        bool holding = LogicUtils.FindObjectInArguments<bool>(arguments);

        if (holding)
        {
            if (picker == null)
            {
                return;
            }

            PickUp(picker);
        }
        else if (!holding)
        {
            PutDown();

            TryToParentThis();
        }
    }

    private void PickUp(PlayerController picker)
    {
        this._isInHand = true;
        this._parentPlayer = picker;
        this._currentParent = picker.playerCamera.transform;

        this._rigidBody.isKinematic = true;
        this._rigidBody.velocity = Vector3.zero;
        this._rigidBody.angularVelocity = Vector3.zero;

        this._collider.enabled = false;
        
        this._wantedPosition = GetWantedPosition();

        this.itemAttachedTo.transform.position = this._wantedPosition;
        this.itemAttachedTo.transform.SetParent(this._currentParent);
        
        this.itemAttachedTo.attachedTo?.UnattachItem(this.itemAttachedTo);

        IShipItem shipItem = this.itemAttachedTo as IShipItem;
        if (shipItem != null)
        {
            /*TODO:possibly differentiate between ships*/
            VesselBase shipAttachedTo = ReferenceManager.PlayerVessel;
            shipItem.RemoveFromShip(shipAttachedTo);
        }

        if (this.onPickUp != null)
        {
            this.onPickUp();
        }
           
    }
    
    private void PutDown()
    {
        this._isInHand = false;

        this._rigidBody.useGravity = this._originalRigidBodyGravity;

        this._collider.enabled = true;

        
        this.itemAttachedTo.transform.rotation = this._originalRotation;
        this.itemAttachedTo.transform.SetParent(this._originalParent);

        this._parentPlayer?.StopInteractingWith(this.itemAttachedTo);
    }

    internal void TryToParentThis()
    {
        RaycastHit hit;
        Debug.DrawRay(this.itemAttachedTo.transform.position + Vector3.up, -Vector3.up * 10f, Color.green, 11f);

        if (Physics.Raycast(this.itemAttachedTo.transform.position + Vector3.up, -Vector3.up, out hit, Mathf.Infinity, ignoreRaycastAndItems))
        {
            IItemPlaceable block = hit.collider.transform.GetComponent<IItemPlaceable>();
            if (block != null)
            {
                block.TryToPlaceItemOnThis(this.itemAttachedTo, this.itemAttachedTo.snappingStyle);
            }
            else
            {
                PlacementHelper.PlaceItem(this.itemAttachedTo, SnappingStyle.Drop, Vector3.zero);
            }
        }       

        //RaycastHit[] hits = Physics.RaycastAll(this.itemAttachedTo.transform.position, -Vector3.up, Mathf.Infinity);

        //foreach (RaycastHit hit in hits)
        //{
        //    IItemPlaceable block = hit.collider.transform.GetComponent<IItemPlaceable>();

        //    if (block != null && block.TryToPlaceItemOnThis(this.itemAttachedTo, this.itemAttachedTo.snappingStyle))
        //    {
        //        return;
        //    }
        //}

        //PlacementHelper.PlaceItem(this.itemAttachedTo, SnappingStyle.Drop, Vector3.zero);

        
    }

    private Vector3 GetWantedPosition()
    {
        float multiplier = 3;
        float colliderSizeX = this.itemAttachedTo.GetComponent<Collider>().bounds.size.x;
        if (colliderSizeX > multiplier)
        {
            multiplier = colliderSizeX;
        }

        return this._currentParent.position + this._currentParent.forward * multiplier;
    }

}