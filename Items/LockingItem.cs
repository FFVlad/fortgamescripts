﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockingItem : Item, IContinuousInteractable
{
    public override void SingleInteract(params object[] arguments)
    {
        PlayerController playerController = LogicUtils.FindObjectInArguments<PlayerController>(arguments);
        if(playerController)
        {
            PlayerInteraction(arguments, playerController);
        }
        else
        {
            base.SingleInteract(arguments);
        }
    }

    private void PlayerInteraction(object[] arguments, PlayerController playerController)
    {
        if (!Input.GetKeyDown(KeyCode.E) && playerController && playerController._didAttemptInteractLastFrame)
        {
            LockPlayer(playerController);
        }

        foreach (var component in this.itemComponents)
        {
            IContinuosComponent continuosComponent = component as IContinuosComponent;
            if (continuosComponent != null)
            {
                if (Input.GetKey(KeyCode.D))
                {
                    continuosComponent.DecreaseInteraction();
                }

                if (Input.GetKey(KeyCode.A))
                {
                    continuosComponent.IncreaseInteraction();
                }

                if (Input.GetKey(KeyCode.W))
                {
                    continuosComponent.IncreaseSecondaryInteraction();
                }

                if (Input.GetKey(KeyCode.S))
                {
                    continuosComponent.DecreaseSecondaryInteraction();
                }                
            }            
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            base.SingleInteract(arguments);
        }
    }

    public void LockPlayer(PlayerController player)
    {
        if (!player._interactingWithSomething)
        {
            player._interactingWithSomething = true;
        }
    }
}
