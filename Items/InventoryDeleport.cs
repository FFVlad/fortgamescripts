﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryDeleport : StandItem
{
    public Transform teleportPosition;
    public float DespawnTime = 2f;

    private List<DeleteItemData> thingsInsideMe = new List<DeleteItemData>();

    protected override void Update()
    {
        base.Update();

        List<DeleteItemData> taggedForDeletion = new List<DeleteItemData>();

        foreach (DeleteItemData delete in thingsInsideMe)
        {
            delete.timePassed += Time.deltaTime;

            if (delete.timePassed > DespawnTime)
            {
                taggedForDeletion.Add(delete);
            }
        }

        foreach (DeleteItemData data in taggedForDeletion)
        {
            TryToDeleteItem(data.itemToDelete);
        }
    }

    public override bool TryToPlaceItemOnThis(Item item, SnappingStyle snappingStyle)
    {
        bool success = base.TryToPlaceItemOnThis(item, snappingStyle);

        if (success)
        {
            thingsInsideMe.Add(new DeleteItemData() { itemToDelete = item });
        }

        return success;
    }

    private void TryToDeleteItem(Item toDelete)
    {
        if (_attachedItems.Contains(toDelete))
        {
            toDelete.DeleteItem(ReferenceManager.playerController.CurrentPlayerInventory);
        }

        DeleteItemFromList(toDelete);
    }

    private void DeleteItemFromList(Item toDelete)
    {
        List<DeleteItemData> deleteThesePlx = new List<DeleteItemData>();

        foreach (DeleteItemData data in thingsInsideMe)
        {
            if (data.itemToDelete == toDelete)
            {
                deleteThesePlx.Add(data);
            }
        }

        foreach (DeleteItemData data in deleteThesePlx)
        {
            thingsInsideMe.Remove(data);
        }
    }

    public override Vector3 GetPlacingPosition()
    {
        return teleportPosition.position;
    }

    protected override bool CheckAttachedItems(Item item, SnappingStyle snappingStyle, out bool tryToPlaceItemOnThis)
    {
        if (item is IHolder)
        {
            if (this._attachedItems.Count > 0)
            {
                foreach (var attachedItem in this._attachedItems)
                {
                    IItemPlaceable itemPlaceable = attachedItem as IItemPlaceable;
                    if (itemPlaceable != null && itemPlaceable.TryToPlaceItemOnThis(item, snappingStyle))
                    {
                        tryToPlaceItemOnThis = true;
                        return true;
                    }
                }
            }

            PlacementHelper.PlaceItem(item, snappingStyle, teleportPosition.position - transform.position, this);
            tryToPlaceItemOnThis = true;
            return true;
        }

        tryToPlaceItemOnThis = false;
        return false;
    }

    public class DeleteItemData
    {
        public float timePassed;
        public Item itemToDelete;
    }

}