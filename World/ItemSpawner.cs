﻿using UnityEngine;
using Random = UnityEngine.Random;

public class ItemSpawner : Spawner
{
    private VesselBase _ship;

    void Start()
    {
        this._onSpawn += FloatItem;
    }

    private void FloatItem(GameObject item)
    {
        item.GetComponent<Item>()?.Float();
    }
    
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, this.minSpawnRadius);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, this.maxSpawnRadius);


    }
}


