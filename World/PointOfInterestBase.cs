﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterestBase : MonoBehaviour
{


    public string POItype = "BasicPOI";
    public float forcedStartDistance;
    public bool isPermanent;
    public float maxDistance;

    [SerializeField]
    private float _currentDistance;
    private Transform _originalParent;
    private VesselBase _playerVessel;
    private GameObject _sea;
    private Vector3 _originalScale;
    private float _minSpawnDistance = 1500f;
    private float _maxSpawnDistance = 4500f;
    private float _speedMultiplier = 0.0005f;

    void Start()
    {
        _sea = ReferenceManager.Sea;
    }
    // Update is called once per frame
    void Update()
    {
        UpdateDistance();
        UpdateScale();
    }

    private void UpdateScale()
    {
        if (_currentDistance >= 1000f)
        {
            //HandleFarAwayPOI();
        }
        else if (_currentDistance < 1000f)
        {
            //HandleNearPOI();
        }
        else if (_currentDistance > _maxSpawnDistance + _minSpawnDistance && !isPermanent)
        {
            DisablePOI();
        }
    }

    private void HandleFarAwayPOI()
    {
        if (this.transform.parent != _originalParent)
        {
            this.transform.SetParent(_originalParent);
        }

        this.transform.localScale = _originalScale / (_currentDistance * 0.001f);
    }

    private void HandleNearPOI()
    {
        if (this.transform.parent != _sea.transform)
        {
            this.transform.SetParent(_sea.transform);
        }
    }

    private void UpdateDistance()
    {
        Vector3 targetDir = transform.position - ReferenceManager.PlayerVessel.transform.position;
        float angle = Vector3.Angle(targetDir, ReferenceManager.PlayerVessel.transform.forward);

        float angleMultiplier = 90 - angle;

        _currentDistance -= (angleMultiplier * _speedMultiplier) * ReferenceManager.PlayerVessel.shipSpeed;

        if (maxDistance != 0 && _currentDistance > maxDistance)
        {
            _currentDistance = maxDistance;
        }

    }

    void DisablePOI()
    {
        this.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        _originalScale = this.transform.localScale;
        _originalParent = this.transform.parent;
        StartCoroutine(SetDistance());
    }

    IEnumerator SetDistance()
    {
        yield return new WaitForEndOfFrame();
        if (forcedStartDistance == 0)
        {
            _currentDistance = Vector3.Distance(this.transform.position, ReferenceManager.PlayerVessel.transform.position) * 10f;
        }
        else
        {
            _currentDistance = forcedStartDistance;
        }

    }

    void OnDisable()
    {
        if (this.gameObject.activeSelf)
        {
            this.transform.localScale = _originalScale;
            if (_originalParent.gameObject.activeSelf)
            {
                this.transform.SetParent(_originalParent);
            }
            PointOfInterestManager.Instance.RemovePOI(this);
        }
    }
}
