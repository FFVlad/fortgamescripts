﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaMover : MonoBehaviour
{
    public VesselBase vessel;
    private void Start()
    {
        vessel = FindObjectOfType<VesselBase>();
    }

    void Update()
    {
        if (vessel && vessel.isPlayerInShip)
        {
            //transform.position = Vector3.MoveTowards(transform.position, transform.position - vessel.MovementVector, vessel.shipSpeed);
        }
        else if(vessel && !vessel.isPlayerInShip)
        {
            //This line if we want vessel to move while you are not on it
            //vessel.transform.position = Vector3.MoveTowards(vessel.transform.position, vessel.transform.position + vessel.MovementVector, vessel.shipSpeed*0.5f);

            //This line if we want items to move while you are not on vessel 
            //transform.position = Vector3.MoveTowards(transform.position, transform.position - vessel.transform.forward, vessel.shipSpeed*0.2f);
        }
    }
}
