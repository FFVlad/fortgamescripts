﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : Spawner
{
    public int preSpawnItems = 15;
    public float yOffset = 5;

    private void Awake()
    {
       Invoke(nameof(Initialize), 2);
    }

    private void Initialize()
    {
        PlaceSpawner();
        this.maxSpawnRadius = Vector3.Distance(ReferenceManager.PlayerVessel.transform.position, transform.position) * 0.75f;
        for (int i = 0; i < preSpawnItems; i++)
        {
            SpawnItems();
        }
    }

    private void PlaceSpawner()
    {
       transform.position = (PointOfInterestManager.Instance.currentGoal.transform.position + ReferenceManager.PlayerVessel.transform.position) /2;
    }

    protected override Vector3 GetSpawnPosition()
    {
        Vector3 defaultPos = base.GetSpawnPosition();
        defaultPos += Vector3.up * yOffset;
        return defaultPos;
    }
}

