﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public List<WeightedItem> itemsToSpawn = new List<WeightedItem>();
    
    public float spawnedThings;
    public float maxThingsToSpawn;
    
    public float minSpawnRadius;
    public float maxSpawnRadius;

    private float _timeSinceLastSpawn;
    public float spawnCooldown;
    
    protected virtual GameObject SpawnPoint => gameObject;

    protected Action<GameObject> _onSpawn;
    protected virtual Type SpawnedItemType => typeof(GameObject);

    void Update()
    {
        if (this.itemsToSpawn.Count == 0)
        {
            return;
        }
        if (this.spawnedThings > this.maxThingsToSpawn)
        {
            return;
        }

        this._timeSinceLastSpawn += Time.deltaTime;
        if (this._timeSinceLastSpawn >= this.spawnCooldown)
        {
            this._timeSinceLastSpawn = 0;

            SpawnItems();
        }
    }

    protected void SpawnItems()
    {
        Vector3 spawnPosition = GetSpawnPosition();
        WeightedItem itemToSpawn = GetRandomItem();
        GameObject gameObjectToInstantiate = itemToSpawn.GetObject();
        if (ReferenceManager.Sea)
        {
            GameObject go = Instantiate(gameObjectToInstantiate, spawnPosition, gameObjectToInstantiate.transform.rotation, ReferenceManager.Sea.transform);

            this.spawnedThings++;
            if (this._onSpawn != null)
            {
                this._onSpawn(go);
            }
        }

    }

    protected virtual Vector3 GetSpawnPosition()
    {
        return SpawnPoint.transform.position + PositionUtils.RandomPositionInCircle(this.maxSpawnRadius, this.minSpawnRadius);
    }
    
    /*TODO: possibly generalise and tie loot to this*/
    protected virtual WeightedItem GetRandomItem()
    {
        if (this.itemsToSpawn.Count == 0)
        {
            Debug.LogError("Trying to spawn item with an empty item list!");
            return null;
        }

        if (this.itemsToSpawn.Count == 1)
        {
            return this.itemsToSpawn[0];
        }

        float totalChance = 0f;

        foreach (WeightedItem enemy in this.itemsToSpawn)
        {
            totalChance += enemy.GetWeight();
        }

        if (totalChance <= 0)
        {
            Debug.LogError("item weighted spawning got negative or 0 weight!");
            return null;
        }

        float randomNumber = Random.Range(0, totalChance);
        float currentlyAt = 0;
        int currentIndex = 0;
        int winnerIndex = -1;

        foreach (WeightedItem enemy in this.itemsToSpawn)
        {
            float maxLimitOfGettingIt = currentlyAt + enemy.GetWeight();

            if (randomNumber >= currentlyAt && randomNumber < maxLimitOfGettingIt)
            {
                winnerIndex = currentIndex;
                break;
            }

            currentIndex++;
            currentlyAt = maxLimitOfGettingIt;
        }

        if (winnerIndex < 0 || this.itemsToSpawn.Count <= winnerIndex)
        {
            Debug.LogError("Something went wrong when trying to get a random item from enemy table!");
            return null;
        }
        return this.itemsToSpawn[winnerIndex];
    }
}
[Serializable]
public class WeightedItem 
{
    public GameObject item;
    public float weight;
    public GameObject GetObject()
    {
        return item;
    }

    public float GetWeight()
    {
        return this.weight;
    }
}
